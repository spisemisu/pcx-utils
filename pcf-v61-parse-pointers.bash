#!/usr/bin/env bash

################################################################################
##
## PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

clear

mkdir -v -p pcx/pcf/v60
mkdir -v -p pcx/pcf/v61

echo "# Parse pointers"

echo "## Teams:"

echo "* ../dat/pcf0061/DBDAT/EQ022022.PKF"
./bin/pcx-parsepointers \
    pkf="../dat/pcf0061/DBDAT/EQ022022.PKF" \
    > pcx/pcf/v61/eq022022.ptr
echo "* pcx/pcf/v61/eq022022.ptr"

echo "## Images:"

echo "### Flags:"

echo "* ../dat/pcf0061/DBDAT/BANDERAS.PKF"
./bin/pcx-parsepointers \
    pkf="../dat/pcf0061/DBDAT/BANDERAS.PKF" \
    > pcx/pcf/v61/banderas.ptr
echo "* pcx/pcf/v61/banderas.ptr"

echo "* ../dat/pcf0061/DBDAT/MINIBAND.PKF"
./bin/pcx-parsepointers \
    pkf="../dat/pcf0061/DBDAT/MINIBAND.PKF" \
    > pcx/pcf/v61/miniband.ptr
echo "* pcx/pcf/v61/miniband.ptr"

echo "### Stadiums:"

echo "* ../dat/pcf0060/DBDAT/ESTADIO.PKF"
./bin/pcx-parsepointers \
    pkf="../dat/pcf0060/DBDAT/ESTADIO.PKF" \
    > pcx/pcf/v60/estadio.ptr
echo "* pcx/pcf/v60/estadio.ptr"

echo "### Logos:"

echo "* ../dat/pcf0061/DBDAT/MINIESC.PKF"
./bin/pcx-parsepointers \
    pkf="../dat/pcf0061/DBDAT/MINIESC.PKF" \
    > pcx/pcf/v61/miniesc.ptr
echo "* pcx/pcf/v61/miniesc.ptr"

echo "* ../dat/pcf0061/DBDAT/NANOESC.PKF"
./bin/pcx-parsepointers \
    pkf="../dat/pcf0061/DBDAT/NANOESC.PKF" \
    > pcx/pcf/v61/nanoesc.ptr
echo "* pcx/pcf/v61/nanoesc.ptr"

echo "* ../dat/pcf0061/DBDAT/RIDIESC.PKF"
./bin/pcx-parsepointers \
    pkf="../dat/pcf0061/DBDAT/RIDIESC.PKF" \
    > pcx/pcf/v61/ridiesc.ptr
echo "* pcx/pcf/v61/ridiesc.ptr"

echo "### Players:"

echo "* ../dat/pcf0061/DBDAT/MINIFOTO.PKF"
./bin/pcx-parsepointers \
    pkf="../dat/pcf0061/DBDAT/MINIFOTO.PKF" \
    > pcx/pcf/v61/minifoto.ptr
echo "* pcx/pcf/v61/minifoto.ptr"

echo "## Texts:"

echo "* ../dat/pcf0061/DBDAT/TEXTOS.PKF"
./bin/pcx-parsepointers \
    pkf="../dat/pcf0061/DBDAT/TEXTOS.PKF" \
    > pcx/pcf/v61/textos.ptr
echo "* pcx/pcf/v61/textos.ptr"

echo "## Other:"

echo "* ../dat/pcf0060/DAT.PKF"
./bin/pcx-parsepointers \
    pkf="../dat/pcf0060/DAT.PKF" \
    > pcx/pcf/v60/dat.ptr
echo "* pcx/pcf/v60/dat.ptr"

echo "* ../dat/pcf0060/MUSICAS.PKF"
./bin/pcx-parsepointers \
    pkf="../dat/pcf0060/MUSICAS.PKF" \
    > pcx/pcf/v60/musicas.ptr
echo "* pcx/pcf/v60/musicas.ptr"

#echo "* ../dat/pcf0061/RECURSOS.PKF"
#./bin/pcx-parsepointers \
#    pkf="../dat/pcf0061/RECURSOS.PKF" \
#    > pcx/pcf/v61/recursos.ptr
#echo "* pcx/pcf/v61/recursos.ptr"

#echo "* ../dat/pcf0061/IMG.PKF"
#./bin/pcx-parsepointers \
#    pkf="../dat/pcf0061/IMG.PKF" \
#    > pcx/pcf/v61/img.ptr
#echo "* pcx/pcf/v61/img.ptr"
