#!/usr/bin/env bash

################################################################################
##
## PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

clear

mkdir -v -p pcx/pcf/v50

echo "# Parse pointers:"

echo "## Teams:"

echo "* ../dat/pcf0050/DBDAT/EQUIPOS.PKF"
./bin/pcx-parsepointers \
    pkf="../dat/pcf0050/DBDAT/EQUIPOS.PKF" \
    > pcx/pcf/v50/equipos.ptr
echo "* pcx/pcf/v50/equipos.ptr"
