#!/bin/sh

################################################################################
##
## PCx-Utils, (c) 2023 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

clear

mkdir -v -p eaf/temp/

# UI ratings:
# - https://www.ea.com/games/ea-sports-fc/ratings
#
# API ratings:
# - https://drop-api.ea.com/rating/fc-24
# - https://drop-api.ea.com/rating/fc-24?locale=en&limit=100&offset=0
#
# > REMARK: 17670 / 100 = 176.7 -> 176. Use 200 as upperbound

o=100
for i in {0..200}; do
    j=$(($o*$i))
    echo "# https://drop-api.ea.com/rating/fc-24?locale=en&limit=$o&offset=$j"
    curl "https://drop-api.ea.com/rating/fc-24?locale=en&limit=$o&offset=$j" \
         > "eaf/temp/ratings-offset-$j.json"
    echo
done
