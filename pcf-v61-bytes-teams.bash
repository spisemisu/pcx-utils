#!/usr/bin/env bash

################################################################################
##
## PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

clear

mkdir -v -p pcx/pcf/v61

echo "# Bytes teams:"

mkdir -v -p pcx/pcf/v61/eq022022
echo "* pcx/pcf/v61/eq022022.tms"
./bin/pcf-v6-bytesteams \
    dbc="pcx/pcf/v61/eq022022/" \
    tms="pcx/pcf/v61/eq022022.tms"
echo "* pcx/pcf/v61/eq022022/"
