#!/usr/bin/env bash

################################################################################
##
## PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

clear

mkdir -v -p pcx/pcf/v61

echo "# Rotate bytes:"

echo "## Teams:"

echo "* ../dat/pcf0061/DBDAT/EQ022022.PKF"
./bin/pcx-bytesrotation \
    pkf="../dat/pcf0061/DBDAT/EQ022022.PKF" \
    > pcx/pcf/v61/eq022022.rot
echo "* pcx/pcf/v61/eq022022.rot"
