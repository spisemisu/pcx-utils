--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCx.Vx.PKF.Types.Pointer
  ( Pointer
    ( index
    , offset
    , uid
    , start
    , size
    )
  , pointer
  )
where

--------------------------------------------------------------------------------

import           Data.PCx.Byte.Common
  ( Bytes
  , Index
  )

--------------------------------------------------------------------------------

data Pointer = T
  { index  :: Index
  , offset :: Word
  , uid    :: Bytes
  , start  :: Word
  , size   :: Word
  }
  deriving
    ( Eq
    , Ord
    , Read
    , Show
    )

--------------------------------------------------------------------------------

pointer
  :: Index
  -> Word
  -> Bytes
  -> Word
  -> Word
  -> Pointer
pointer =
  T
