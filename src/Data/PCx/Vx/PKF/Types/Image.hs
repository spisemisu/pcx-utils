--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCx.Vx.PKF.Types.Image
  ( Image
    ( uid
    , bmp
    )
  , image
  )
where

--------------------------------------------------------------------------------

import           Data.Word
  ( Word16
  )

import           Data.PCx.Byte.Common
  ( ByteStream (bytes)
  )

import qualified Data.PCx.Vx.BMP.Types.File as BMP

--------------------------------------------------------------------------------

data Image = T
  { uid :: Word16
  , bmp :: BMP.File
  }
  deriving
    ( Eq
    , Ord
    , Read
    , Show
    )

--------------------------------------------------------------------------------

instance ByteStream Image where
  bytes i =
    bytes f
    where
      f = bmp i

--------------------------------------------------------------------------------

image
  :: Word16
  -> BMP.File
  -> Image
image =
  T
