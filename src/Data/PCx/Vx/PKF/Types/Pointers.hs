--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCx.Vx.PKF.Types.Pointers
  ( Pointers
    ( Pointers
    )
  , pointers
  )
where

--------------------------------------------------------------------------------

import qualified Data.PCx.Vx.PKF.Types.Pointer as PKF

--------------------------------------------------------------------------------

data Pointers =
  Pointers [PKF.Pointer]
  deriving
    ( Eq
    , Ord
    , Read
    , Show
    )

--------------------------------------------------------------------------------

pointers
  :: [PKF.Pointer]
  -> Pointers
pointers =
  Pointers
