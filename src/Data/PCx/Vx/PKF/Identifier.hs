--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCx.Vx.PKF.Identifier
  ( uid
  )
where

--------------------------------------------------------------------------------

import           Data.Word
  ( Word16
  )

import           Data.PCx.Byte.Common
  ( Byte
  , Bytes
  )

--------------------------------------------------------------------------------

uid
  :: Bytes
  -> Word16
uid (m:c:d:u:[]) =
  ( 1000 * mil m +
    0100 * cen c +
    0010 * dec d +
    0001 * uni u
  )
uid invalidbytes =
  error $ "Data.PCx.Vx.PKF.Identifier > uid: " ++ show invalidbytes

--------------------------------------------------------------------------------

-- HELPERS

mil
  :: Byte
  -> Word16
mil =
  ((-) 10) . fromIntegral . (flip (-) 85)

cen
  :: Byte
  -> Word16
cen 0x60 = 8
cen 0x61 = 9
cen 0x68 = 0
cen 0x69 = 1
cen 0x6A = 2
cen 0x6B = 3
cen 0x6C = 4
cen 0x6D = 5
cen 0x6E = 6
cen 0x6F = 7
cen rest = error $ "Data.PCx.Vx.PKF.Identifier > cen: " ++ show rest

dec
  :: Byte
  -> Word16
dec 0x70 = 3
dec 0x71 = 2
dec 0x72 = 1
dec 0x73 = 0
dec 0x74 = 7
dec 0x75 = 6
dec 0x76 = 5
dec 0x77 = 4
dec 0x7A = 9
dec 0x7B = 8
dec rest = error $ "Data.PCx.Vx.PKF.Identifier > dec: " ++ show rest

uni
  :: Byte
  -> Word16
uni =
  fromIntegral
