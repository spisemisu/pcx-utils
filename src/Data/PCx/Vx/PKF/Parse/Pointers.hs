--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCx.Vx.PKF.Parse.Pointers
  ( pointersP
  )
where

--------------------------------------------------------------------------------

import           Data.PCx.Byte.Parser
  ( Parser
  , byteP
  , bytesP
  , chunkP
  , getP
  , many
  , quadletP
  , (<|>)
  )
import qualified Data.PCx.Vx.PKF.Parse.Pointer  as PKF
import qualified Data.PCx.Vx.PKF.Types.Pointer  as PKF
import qualified Data.PCx.Vx.PKF.Types.Pointers as PKF

--------------------------------------------------------------------------------

pointersP
  :: Parser PKF.Pointers
pointersP =
  aux
  <$> chunkP  off
  <*> jumpP 1 off
  where
    off = 232 -- 22+7
    aux _ ps =
      PKF.pointers ps

--------------------------------------------------------------------------------

-- HELPERS

jumpP
  :: Word
  -> Word
  -> Parser [PKF.Pointer]
jumpP i o =
  byteP 0x04 >>= \ _ ->
  quadletP   >>= \ w ->
  aux w
  where
    aux w32 =
      chunkP (w-o-p) >>= \ _ ->
      jumpP i w <|> manyP i w <|> termP
      where
        w = fromIntegral w32
        p = 5

manyP
  :: Word
  -> Word
  -> Parser [PKF.Pointer]
manyP i o =
  -- Every 32 parsed elements, a jump of 1221 bytes is made
  -- > Note: 38*32+1+4=1221
  aux o <|> jumpP i o
  where
    aux a =
      (:)
      <$> PKF.pointerP  i     a
      <*> manyP        (i+1) (a+38)

termP
  :: Parser [PKF.Pointer]
termP =
  aux
  <$> byteP 0x05
  <*> bytesP end
  <*> many getP
  where
    aux _ _ _ = []
    end =
      [ 0x00, 0x00, 0x00, 0x00
      ]
