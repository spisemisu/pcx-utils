--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCx.Vx.PKF.Parse.Image
  ( imageP
  )
where

--------------------------------------------------------------------------------

import           Data.Word
  ( Word16
  )

import           Data.PCx.Byte.Parser
  ( Parser
  )
import qualified Data.PCx.Vx.BMP.Parse.File  as BMP
import qualified Data.PCx.Vx.PKF.Types.Image as PKF

--------------------------------------------------------------------------------

imageP
  :: Word16
  -> Parser PKF.Image
imageP uid =
  PKF.image uid
  <$> BMP.fileP
