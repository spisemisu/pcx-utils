--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCx.Vx.PKF.Parse.Images
  ( imagesP
  )
where

--------------------------------------------------------------------------------

import           Data.Word
  ( Word16
  )

import           Data.PCx.Byte.Parser
  ( Parser
  , chunkP
  , getP
  , many
  )
import qualified Data.PCx.Vx.PKF.Identifier     as PKF.Identifier
import qualified Data.PCx.Vx.PKF.Parse.Image    as PKF
import qualified Data.PCx.Vx.PKF.Types.Image    as PKF
import qualified Data.PCx.Vx.PKF.Types.Images   as PKF
import qualified Data.PCx.Vx.PKF.Types.Pointer  as PKF.Pointer
import qualified Data.PCx.Vx.PKF.Types.Pointers as PKF

--------------------------------------------------------------------------------

imagesP
  :: PKF.Pointers
  -> Parser PKF.Images
imagesP (PKF.Pointers ps) =
  (\ xs _ -> PKF.images xs) <$> aux 0 pts <*> many getP
  where
    pts =
      map f ps
      where
        f p =
          ( PKF.Identifier.uid $ PKF.Pointer.uid   p
          ,                      PKF.Pointer.start p
          ,                      PKF.Pointer.size  p
          )
    aux _ [          ] = pure []
    aux a ((u,o,s):xs) =
      (:)
      <$> imageP i u
      <*> aux    j xs
      where
        i = o - a
        j = o + s

--------------------------------------------------------------------------------

-- HELPERS

imageP
  :: Word
  -> Word16
  -> Parser PKF.Image
imageP o u =
  aux
  <$> chunkP     o -- Skip offset
  <*> PKF.imageP u
  where
    aux _ i =
      i
