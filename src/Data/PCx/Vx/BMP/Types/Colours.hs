--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCx.Vx.BMP.Types.Colours
  ( Colours
    ( Quadruple
    , Triple
    )
  , quadruples
  , triples
  , colours
  )
where

--------------------------------------------------------------------------------

import           Data.PCx.Byte.Common
  ( ByteStream (bytes)
  )
import           Data.PCx.Vx.BMP.Types.RGB
  ( RGB
  )

--------------------------------------------------------------------------------

-- Bitmap Header Types:
--
-- an optional color table, which is either a set of RGBQUAD structures or a set
-- of RGBTRIPLE structures.
--
-- https://docs.microsoft.com/en-us/windows/win32/gdi/bitmap-header-types

data Colours
  = Quadruple [RGB]
  | Triple    [RGB]
  deriving
    ( Eq
    , Ord
    , Read
    , Show
    )

--------------------------------------------------------------------------------

instance ByteStream Colours where
  bytes (Quadruple cs) =
    concatMap aux cs
    where
      aux = (++ [0x00]) . bytes
  bytes (Triple cs) =
    concatMap bytes cs

--------------------------------------------------------------------------------

quadruples
  :: [RGB]
  -> Colours
quadruples =
  Quadruple

triples
  :: [RGB]
  -> Colours
triples =
  Triple

colours
  :: Colours
  -> [RGB]
colours (Quadruple cs) =
  cs
colours (Triple cs) =
  cs
