--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCx.Vx.BMP.Types.File
  ( File
    ( header
    )
  , file
  )
where

--------------------------------------------------------------------------------

import           Data.Word
  ( Word32
  )

import           Data.PCx.Byte.Common
  ( Byte
  , ByteStream (bytes)
  , Bytes
  )
import qualified Data.PCx.Vx.BMP.Types.Colours as BMP
import qualified Data.PCx.Vx.BMP.Types.Colours as BMP.Colours
import           Data.PCx.Vx.BMP.Types.Header
  ( Header
  )
import           Data.PCx.Vx.BMP.Types.RGB
  ( RGB
  , c2i
  , i2c
  )

--------------------------------------------------------------------------------

data File = F
  { header    :: Header
  , info      :: Bytes
  , colourmap :: BMP.Colours
  , pixels    :: Bytes
  }
  deriving
    ( Eq
    , Ord
    , Read
    , Show
    )

--------------------------------------------------------------------------------

instance ByteStream File where
  bytes f =
    bytes h ++ bytes w ++ i ++ bytes c ++ p
    where
      h = header f
      l = 4 :: Word32
      w = (+ l) $ fromIntegral $ length i
      i = info f
      c =
        case colourmap f of
          BMP.Colours.Quadruple _ ->
            BMP.quadruples aux
          BMP.Colours.Triple _ ->
            BMP.triples aux
        where
          aux = map i2c [0x00 .. 0xFF]
      d = c2d $ BMP.colours $ colourmap f
      p = p2i d $ pixels f

--------------------------------------------------------------------------------

file
  :: Header
  -> Bytes
  -> BMP.Colours
  -> Bytes
  -> File
file =
  F

--------------------------------------------------------------------------------

-- HELPERS

c2d
  :: [RGB]
  -> Map Byte Byte
c2d =
  aux nil . zip [0x00 .. 0xFF]
  where
    aux acc [        ] = acc
    aux acc ((i,c):rs) =
      case mem i acc of
        Just _ ->
          aux acc rs
        Nothing ->
          aux bcc rs
      where
        idx =
          c2i c
        bcc =
          ins i idx acc

p2i
  :: Map Byte Byte
  -> Bytes
  -> Bytes
p2i dic =
  aux
  where
    aux [    ] = []
    aux (p:ps) =
      case mem p dic of
        Just b ->
          b:aux ps
        Nothing ->
          error "Shouldn't be possible (p2i -> aux)"

-- F U N C T I O N A L  P E A R L S
-- Red-Black Trees in a Functional Setting
--
-- CHRIS OKASAKI
-- School of Computer Science, Carnegie Mellon University
-- 5000 Forbes Avenue, Pittsburgh, Pennsylvania, USA 15213
-- (e-mail: cokasaki@cs.cmu.edu)

-- Red-Black Trees
data Colour = R | B

data Tree a = L | T Colour (Tree a) a (Tree a)

type Map k a = Tree (k, a)

nil :: Map k a
nil = L

ins
  :: Ord k
  => k
  -> a
  -> Map k a
  -> Map k a
ins k v m =
  blk . aux $ m
  where
    blk (T _ a y b) = T B a y b
    blk ___________ = error "Shouldn't be possible (ins -> blk)"
    aux  L = T R L (k,v) L
    aux (T c a (k', v') b)
      | k <  k' = bal c (aux a) (k',v')      b
      | k == k' = T   c      a  (k ,v )      b
      | k >  k' = bal c      a  (k',v') (aux b)
    aux ___________ = error "Shouldn't be possible (ins -> aux)"
    bal B (T R (T R a x b) y c) z d = T R (T B a x b) y (T B c z d)
    bal B (T R a x (T R b y c)) z d = T R (T B a x b) y (T B c z d)
    bal B a x (T R (T R b y c) z d) = T R (T B a x b) y (T B c z d)
    bal B a x (T R b y (T R c z d)) = T R (T B a x b) y (T B c z d)
    bal c a x b                     = T c a x b

mem
  :: Ord k
  => k
  -> Map k a
  -> Maybe a
mem _  L = Nothing
mem k (T _ a (k', y) b)
  | k <  k' = mem k a
  | k == k' = Just y
  | k >  k' = mem k b
mem _ (T _ _ (_,  _) _) = Nothing
