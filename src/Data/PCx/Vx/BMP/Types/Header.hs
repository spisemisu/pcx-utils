--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCx.Vx.BMP.Types.Header
  ( Header
    ( len
    , off
    )
  , header
  )
where

--------------------------------------------------------------------------------

import           Data.Word
  ( Word32
  )

import           Data.PCx.Byte.Common
  ( ByteStream (bytes)
  )
import           Data.PCx.Vx.BMP.Common
  ( idf
  , res
  )

--------------------------------------------------------------------------------

data Header = T
  { len :: Word32
  , off :: Word32
  }
  deriving
    ( Eq
    , Ord
    , Read
    , Show
    )

--------------------------------------------------------------------------------

instance ByteStream Header where
  bytes h =
    idf ++ bytes l ++ res ++ res ++ bytes o
    where
      l = len h
      o = off h

--------------------------------------------------------------------------------

header
  :: Word32
  -> Word32
  -> Header
header =
  T
