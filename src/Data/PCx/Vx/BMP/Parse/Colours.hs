--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCx.Vx.BMP.Parse.Colours
  ( coloursP
  )
where

--------------------------------------------------------------------------------

import           Data.PCx.Byte.Parser
  ( Parser
  , (<|>)
  )
import qualified Data.PCx.Vx.BMP.Parse.RGB     as BMP
import qualified Data.PCx.Vx.BMP.Types.Colours as BMP

--------------------------------------------------------------------------------

coloursP
  :: Parser BMP.Colours
coloursP =
  quadruplesP <|> triplesP

--------------------------------------------------------------------------------

-- HELPERS

quadruplesP
  :: Parser BMP.Colours
quadruplesP =
  BMP.quadruples <$> aux len
  where
    aux 0 = pure []
    aux n =
      (:)
      <$> BMP.quadrupleP
      <*> aux (n - 1)
    len = 256 :: Word

triplesP
  :: Parser BMP.Colours
triplesP =
  BMP.triples <$> aux len
  where
    aux 0 = pure []
    aux n =
      (:)
      <$> BMP.tripleP
      <*> aux (n - 1)
    len = 256 :: Word
