--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCx.Vx.BMP.Parse.RGB
  ( quadrupleP
  , tripleP
  )
where

--------------------------------------------------------------------------------

import           Data.PCx.Byte.Parser
  ( Parser
  , byteP
  , octetP
  )
import qualified Data.PCx.Vx.BMP.Types.RGB as BMP

--------------------------------------------------------------------------------

quadrupleP
  :: Parser BMP.RGB
quadrupleP =
  aux
  <$> octetP
  <*> octetP
  <*> octetP
  <*> byteP res
  where
    aux b g r _ =
      BMP.rgb b g r
    res =
      0x00

tripleP
  :: Parser BMP.RGB
tripleP =
  BMP.rgb
  <$> octetP
  <*> octetP
  <*> octetP
