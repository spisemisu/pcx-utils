--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCx.Vx.BMP.Parse.File
  ( fileP
  )
where

--------------------------------------------------------------------------------

import           Data.Word
  ( Word32
  )

import           Data.PCx.Byte.Common
  ( ByteStream (bytes)
  , Bytes
  )
import           Data.PCx.Byte.Parser
  ( Parser
  , chunkP
  , quadletP
  )
import qualified Data.PCx.Vx.BMP.Parse.Colours as BMP
import qualified Data.PCx.Vx.BMP.Parse.Header  as BMP
import qualified Data.PCx.Vx.BMP.Types.Colours as BMP
import qualified Data.PCx.Vx.BMP.Types.File    as BMP
import qualified Data.PCx.Vx.BMP.Types.Header  as BMP
import           Data.PCx.Vx.BMP.Types.RGB     as BMP

--------------------------------------------------------------------------------

fileP
  :: Parser BMP.File
fileP =
  BMP.headerP >>= aux
  where
    aux h =
      if o < 768 -- Colortable at least: 3 bytes * 256 colours = 768 bytes
      then
        ncm h
        <$> infoP
        <*> pixelsP n
      else
        BMP.file h
        <$> infoP
        <*> BMP.coloursP
        <*> pixelsP n
      where
        l = BMP.len h
        o = BMP.off h
        n = l - o
    ncm h i ps =
      BMP.file nh i cm ps
      where
        cm = BMP.triples  $ map BMP.i2c [0x00 .. 0xFF]
        bs = fromIntegral $ length $ bytes cm
        nh = h
          { BMP.len = BMP.len h + bs
          , BMP.off = BMP.off h + bs
          }

--------------------------------------------------------------------------------

-- HELPERS

infoP
  :: Parser Bytes
infoP =
  quadletP >>= aux
  where
    aux n =
      chunkP m
      where
        m = n - 4

pixelsP
  :: Word32
  -> Parser Bytes
pixelsP =
  chunkP

--------------------------------------------------------------------------------

{-

* Bitmap Storage

The following table shows the data bytes associated with the structures in a
bitmap file (*):

- BITMAPFILEHEADER.: 0x0000 - 0x000D
- BITMAPINFOHEADER.: 0x000E - 0x0035
- RGBQUAD array....: 0x0036 - 0x0075
- Color-index array: 0x0076 - 0x0275

0x0000: 42 4d•76 02 00 00•00 00•00 00•76 00 00 00
                                                  28 00
0x0010: 00 00•20 00 00 00•20 00 00 00•01 00•04 00•00 00
0x0020: 00 00•00 00 00 00•00 00 00 00•00 00 00 00•00 00
0x0030: 00 00•00 00 00 00
                          00 00 00 00•00 00 80 00•00 80
0x0040: 00 00•00 80 80 00•80 00 00 00•80 00 80 00•80 80
0x0050: 00 00•80 80 80 00•c0 c0 c0 00•00 00 ff 00•00 ff
0x0060: 00 00•00 ff ff 00•ff 00 00 00•ff 00 ff 00•ff ff
0x0070: 00 00•ff ff ff 00
                          00 00 00 00 00 00 00 00 00 00
0x0080: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 09 00
0x0090: 00 00 00 00 00 00 11 11 01 19 11 01 10 10 09 09
0x00a0: 01 09 11 11 01 90 11 01 19 09 09 91 11 10 09 11
0x00b0: 09 11 19 10 90 11 19 01 19 19 10 10 11 10 09 01
0x00c0: 91 10 91 09 10 10 90 99 11 11 11 11 19 00 09 01
0x00d0: 91 01 01 19 00 99 11 10 11 91 99 11 09 90 09 91
0x00e0: 01 11 11 11 91 10 09 19 01 00 11 90 91 10 09 01
0x00f0: 11 99 10 01 11 11 91 11 11 19 10 11 99 10 09 10
0x0100: 01 11 11 11 19 10 11 09 09 10 19 10 10 10 09 01
0x0110: 11 19 00 01 10 19 10 11 11 01 99 01 11 90 09 19
0x0120: 11 91 11 91 01 11 19 10 99 00 01 19 09 10 09 19
0x0130: 10 91 11 01 11 11 91 01 91 19 11 00 99 90 09 01
0x0140: 01 99 19 01 91 10 19 91 91 09 11 99 11 10 09 91
0x0150: 11 10 11 91 99 10 90 11 01 11 11 19 11 90 09 11
0x0160: 00 19 10 11 01 11 99 99 99 99 99 99 99 99 09 99
0x0170: 99 99 99 99 99 99 00 00 00 00 00 00 00 00 00 00
0x0180: 00 00 00 00 00 00 90 00 00 00 00 00 00 00 00 00
0x0190: 00 00 00 00 00 00 99 11 11 11 19 10 19 19 11 09
0x01a0: 10 90 91 90 91 00 91 19 19 09 01 10 09 01 11 11
0x01b0: 91 11 11 11 10 00 91 11 01 19 10 11 10 01 01 11
0x01c0: 90 11 11 11 91 00 99 09 19 10 11 90 09 90 91 01
0x01d0: 19 09 91 11 01 00 90 10 19 11 00 11 11 00 10 11
0x01e0: 01 10 11 19 11 00 90 19 10 91 01 90 19 99 00 11
0x01f0: 91 01 11 01 91 00 99 09 09 01 10 11 91 01 10 91
0x0200: 99 11 10 90 91 00 91 11 00 10 11 01 10 19 19 09
0x0210: 10 00 99 01 01 00 91 01 19 91 19 91 11 09 10 11
0x0220: 00 91 00 10 90 00 99 01 11 10 09 10 10 19 09 01
0x0230: 91 90 11 09 11 00 90 99 11 11 11 90 19 01 19 01
0x0240: 91 01 01 19 09 00 91 10 11 91 99 09 09 90 11 91
0x0250: 01 19 11 11 91 00 91 19 01 00 11 00 91 10 11 01
0x0260: 11 11 10 01 11 00 99 99 99 99 99 99 99 99 99 99
0x0270: 99 99 99 99 99 90

Source: https://docs.microsoft.com/en-us/windows/win32/gdi/bitmap-storage

(*) The Redbrick.bmp, is a 32x32 bitmap with 16 colors; its color-index array is
32 * 32 * 4 = 4096 bits because four bits index 16 colors.

The following illustration shows the developer's perspective of the bitmap found
in the file Redbrick.bmp. It shows a palette array, a 32-by-32 pixel rectangle,
and the index array that maps colors from the palette to pixels in the
rectangle.

Source: https://docs.microsoft.com/en-us/windows/win32/gdi/about-bitmaps

-}
