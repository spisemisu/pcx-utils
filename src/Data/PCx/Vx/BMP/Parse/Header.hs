--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCx.Vx.BMP.Parse.Header
  ( headerP
  )
where

--------------------------------------------------------------------------------

import           Data.PCx.Byte.Parser
  ( Parser
  , bytesP
  , quadletP
  )
import           Data.PCx.Vx.BMP.Common
  ( idf
  , res
  )
import qualified Data.PCx.Vx.BMP.Types.Header as BMP

--------------------------------------------------------------------------------

headerP
  :: Parser BMP.Header
headerP =
  aux
  <$> bytesP idf
  <*> quadletP
  <*> bytesP res
  <*> bytesP res
  <*> quadletP
  where
    aux _ l _ _ o =
      BMP.header l o
