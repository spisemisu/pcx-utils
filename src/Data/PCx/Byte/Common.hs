--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE Safe              #-}

--------------------------------------------------------------------------------

module Data.PCx.Byte.Common
  ( Byte, Bytes
  , Index, Error
  , Date
  , ByteStream
    ( bytes
    )
  , b2n, n2b
  , b2r, r2b
  , b2h
  , pad, zpd
  , cof
  )
where

--------------------------------------------------------------------------------

import           Data.Bits
  ( Bits
  , shiftL
  , shiftR
  , (.|.)
  )
import           Data.Char
  ( chr
  , ord
  )
import           Data.Word
  ( Word16
  , Word32
  , Word8
  )

import           Data.PCx.Byte.Rotate
  ( bijective
  )

--------------------------------------------------------------------------------

type Byte  = Word8
type Bytes = [Byte]

type Index = Word
type Error = String

type Date = (Word16, Word8, Word8)

--------------------------------------------------------------------------------

class ByteStream a where
  bytes :: a -> Bytes

--------------------------------------------------------------------------------

instance ByteStream Bool where
  bytes True  = [0x01]
  bytes False = [0x00]

instance ByteStream Word8 where
  bytes = tbs 1

instance ByteStream Word16 where
  bytes = tbs 2

instance ByteStream Word32 where
  bytes = tbs 4

instance ByteStream String where
  bytes x =
    tbs 2 n ++ map r2b x
    where
      n = length x

--------------------------------------------------------------------------------

b2n
  :: (Bits a, Integral a)
  => Bytes
  -> a
b2n =
  aux 0
  where
    aux _ [    ] = 0
    aux i (b:bs) = n .|. (aux (i+1) bs)
      where
        n = fromIntegral b .<. (i*8)

n2b
  :: (Bits a, Integral a)
  => Int
  -> (a -> a)
  -> a
  -> Bytes
n2b _ f 0 = [ fromIntegral $! f 0 ]
n2b b f n =
  aux n
  where
    aux 0 = []
    aux m =
      r : aux c
      where
        c =                   m         .>. b
        r = fromIntegral $ f (m - c * 1 .<. b)

--------------------------------------------------------------------------------

b2r
  :: Byte
  -> Char
b2r =
  chr . fromIntegral . bijective

r2b
  :: Char
  -> Byte
r2b =
  bijective . fromIntegral . ord

--------------------------------------------------------------------------------

b2h
  :: Word8
  -> [Char]
b2h =
  pad True 2 '0' .
  reverse .
  map (chr . fromIntegral) .
  n2b 4 {- 2^4 = 016 -} aux
  where
    aux n
      | n <= 0x09 = 48 + n
      | otherwise = 55 + n

--------------------------------------------------------------------------------

pad
  :: Bool
  -> Int
  -> Char
  -> String
  -> String
pad d n c x =
  if l > n
  then x
  else
    if d
    then replicate (n-l) c ++ x
    else x ++ replicate (n-l) c
  where
    l = length x

zpd
  :: Int
  -> Bytes
zpd n =
  take n $
  repeat 0x00

cof
  :: Int
  ->  [a]
  -> [[a]]
cof _ [] = []
cof n xs = take n xs : aux xs
  where
    aux = cof n . drop n

--------------------------------------------------------------------------------

-- HELPERS

(.<.)
  :: Bits a
  => a
  -> Int
  -> a
(.<.) x y = x `shiftL` y

(.>.)
  :: Bits a
  => a
  -> Int
  -> a
(.>.) x y = x `shiftR` y

tbs
  :: (Bits a, Integral a)
  => Int
  -> a
  -> Bytes
tbs n =
  take n . (++ aux) . n2b 8 {- 2^8 = 256 -} id
  where
    aux =
      cycle [0x00]
