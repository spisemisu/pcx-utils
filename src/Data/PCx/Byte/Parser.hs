--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCx.Byte.Parser
  ( Output
    ( output
    )
  , Parser
    ( parse
    )
  , empty, many, some, (<|>)
  , sepBy
  , errorP
  , peekP
  , indexP
  , failP
  , spanP
  , getP
  , byteP, bytesP, chunkP
  , boolP
  , octetP, hextetP, quadletP
  , stringP
  , run
  )
where

--------------------------------------------------------------------------------

import           Control.Applicative
  ( Alternative
  , empty
  , many
  , some
  , (<|>)
  )
import           Data.Char
  ( chr
  )
import           Data.List
  ( findIndex
  , tails
  )
import           Data.Word
  ( Word16
  , Word32
  , Word8
  )

import           Data.PCx.Byte.Common
  ( Byte
  , Bytes
  , Error
  , Index
  , b2h
  , b2n
  , b2r
  , cof
  , pad
  )
import           Data.PCx.Byte.Rotate
  ( bijective
  )

--------------------------------------------------------------------------------

newtype Output a = O
  { output :: (Either (Index, Error) (Index, a, Bytes))
  }

newtype Parser a = P
  { parse :: Index -> Bytes -> Output a
  }

--------------------------------------------------------------------------------

instance Show a => Show (Output a) where
  show o =
    case ppp "show" o of
      Left  e -> e
      Right a -> show a

--------------------------------------------------------------------------------

instance Functor Output where
  fmap _ (O (Left        e))  = O $ Left          e
  fmap f (O (Right (i,a,bs))) = O $ Right $ (i, f a, bs)

instance Applicative Output where
  pure          a           = O $ Right (0,   a, [])
  O (Left       e)  <*> ___ = O $ Left e
  O (Right (_,f,_)) <*> O r =
    case r of
      Left      e      -> O $ Left        e
      Right (i, a, bs) -> O $ Right (i, f a, bs)

instance Alternative Output where
  empty             = O $ Left (0, [])
  O (Left  _) <|> o = o
  o           <|> _ = o

--------------------------------------------------------------------------------

instance Functor Parser where
  fmap f (P p) = P $ \ n bs ->
    case p n bs of
      O (Left         es)  -> O $ Left           es
      O (Right (i, a, xs)) -> O $ Right (i, f a, xs)

instance Applicative Parser where
  pure a              = P $ \ n bs -> O $ Right (n, a, bs)
  (<*>) (P p1) (P p2) = P $ \ n bs ->
    case p1 n bs of
      O (Left         es)      -> O $ Left es
      O (Right (i, f, xs))     ->
        case p2 i xs of
          O (Left         es)  -> O $ Left           es
          O (Right (j, a, ys)) -> O $ Right (j, f a, ys)

instance Monad Parser where
  return        = pure
  (>>=) (P p) f = P $ \ n bs ->
    case p n bs of
      O (Left         es)  -> O $ Left es
      O (Right (i, a, xs)) -> parse (f a) i xs

instance Alternative Parser where
  empty               = P $ \ i __ -> O $ Left (i, [])
  (<|>) (P p1) (P p2) = P $ \ n bs -> p1 n bs <|> p2 n bs

--------------------------------------------------------------------------------

{-

How to look into bytes
======================

Take a chunk and pass it through `b2n` as single bytes:

> map b2n [] -- 4639 0001 0100 0102
> map b2n [[0x46], [0x39], [0x00], [0x01], [0x01], [0x00], [0x01], [0x02]]
> [70,57,0,1,1,0,1,2]

from the result, it's easy to see that it has to do with the tactics.

-}

sepBy
  :: (Alternative f)
  => f  a
  -> f  b
  -> f [a]
sepBy p sep =
  aux <|> pure []
  where
    aux = (:) <$> p <*> (many $ sep *> p)

--------------------------------------------------------------------------------

errorP
  :: Index
  -> String
  -> Bytes
  -> String
errorP i f bs =
  "# Parser error"                                 ++ "\n" ++
  "* Function:.....: " ++ f                        ++ "\n" ++
  "* Index.........: " ++ pad True 16 '0' (show i) ++ "\n" ++
  "* Unparsed bytes: " ++ show len                 ++ "\n" ++ out bs
  where
    len = length bs
    out xs = -- mimic `hexl-mode` in `emacs`
      (++ "\n") $
      (\ cs -> if xs == ys then cs else (cs ++ "…\n")) $
      foldl (\a (x,y) -> a ++ (rp y) ++ " " ++ x ++ "\n") [] . zip zs . cof 40 $
      foldl (\a  x    -> a ++     x  ++ " ")              []          . cof 04 $
      concat  $
      map b2h $
      ys
      where
        rp = pad False 40 ' '
        ys = take 5120                        xs
        zs = cof  0016 $ map (ec . bijective) ys
        ec =
          \ b ->
            if b < 032 || (126 < b && b < 160)
            then '.'
            else chr $ fromIntegral b

--------------------------------------------------------------------------------

peekP
  :: Int
  -> Parser Bytes
peekP n =
  P $ \ i bs -> O $ Right (i, take n bs, bs)

indexP
  :: (Bytes -> Bool)
  -> Parser (Maybe Int)
indexP p =
  P $ \ i bs -> O $ Right (i, aux bs, bs)
  where
    aux = findIndex p . tails

failP
  :: Error
  -> Parser a
failP e =
  P $ \ i bs -> O $ Left (i, errorP i msg bs)
  where
    msg = "faileP > " ++ e

spanP
  :: (Byte -> Bool)
  -> Parser Bytes
spanP f =
  P $ \ i bs -> aux i bs
  where
    aux i bs =
      case span f bs of
        ([],__) -> O $ Left  (i, errorP i "spanP" bs)
        (as,rs) -> O $ Right (i, as, rs)

getP
  :: Parser Byte
getP =
  P $ \ i bs -> aux i bs
  where
    aux i bs@[    ] = O $ Left  (i, errorP i "getP" bs)
    aux i    (x:xs) = O $ Right (i+1, x, xs)

byteP
  :: Byte
  -> Parser Byte
byteP b =
  P $ \ i bs -> aux i bs
  where
    aux i bs@[    ] = O $ Left  (i, errorP i "byteP > []" bs)
    aux i bs@(x:xs) =
      if b == x
      then            O $ Right (i+1, x, xs)
      else            O $ Left  (i, errorP i "byteP" bs)

bytesP
  :: Bytes
  -> Parser Bytes
bytesP =
  sequenceA . map byteP

chunkP
  :: Integral a
  => a
  -> Parser Bytes
chunkP n =
  P $ \ i bs ->
    case take m bs of
      xs | length xs == m -> O $ Right (i+j, take m xs, drop m bs)
      ___________________ -> O $ Left  (i, errorP i e bs)
  where
    j = fromIntegral n
    m = fromIntegral n
    e = "chunkP > take " ++ show m ++ " bytes"

boolP
  :: Parser Bool
boolP =
  aux <$> chunkP len
  where
    aux = not . ((==) [0x00])
    len = 1 :: Word8

octetP
  :: Parser Word8
octetP =
  b2n <$> chunkP len
  where
    len = 1 :: Word8

hextetP
  :: Parser Word16
hextetP =
  b2n <$> chunkP len
  where
    len = 2 :: Word8

quadletP
  :: Parser Word32
quadletP =
  b2n <$> chunkP len
  where
    len = 4 :: Word8

stringP
  :: Parser String
stringP =
  hextetP  >>= \ n  ->
  chunkP n >>= \ bs ->
  pure $ map b2r bs

--------------------------------------------------------------------------------

run
  :: Parser a
  -> Bytes
  -> Either Error a
run p =
  ppp "run" . parse p 0

--------------------------------------------------------------------------------

-- HELPERS

ppp
  :: String
  -> Output a
  -> Either Error a
ppp _ (O (Left     e   ))  = Left  $ snd e
ppp _ (O (Right (_,a,[]))) = Right a
ppp e (O (Right (i,_,bs))) = Left  $ errorP i ("ppp > " ++ e) bs
