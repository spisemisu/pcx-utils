--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCx.Byte.Rotate
  ( bijective
  )
where

--------------------------------------------------------------------------------

import           Data.Bits
  ( (.&.)
  )
import           Data.Word
  ( Word8
  )

--------------------------------------------------------------------------------

bijective
  :: Word8
  -> Word8

--------------------------------------------------------------------------------

bijective b
  | 032  >  b = if b .&. 01 == 0 then b + 97 else b + 95
  | 064  >  b = if b .&. 01 == 0 then b + 33 else b + 31
  | 096  >  b = if b .&. 01 == 0 then b - 31 else b - 33
  | 128  >  b = if b .&. 01 == 0 then b - 95 else b - 97
  | 160  >  b = if b .&. 01 == 0 then b + 97 else b + 95
  | 192  >  b = if b .&. 01 == 0 then b + 33 else b + 31
  | 224  >  b = if b .&. 01 == 0 then b - 31 else b - 33
  | otherwise = if b .&. 01 == 0 then b - 95 else b - 97
