--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCF.V6.PKF.Types.Stats
  ( Stats
    ( unknown00
    , unknown01
    , lastdecade
    , seasons
    , matches
    , win
    , draw
    , gf
    , ga
    , points
    , champion
    , runnersup
    , lastseason
    , uefa
    , cup
    , championsleague
    , cupwinnerscup
    , supercup
    , intercontinental
    , eufasupercup
    )
  , stats
  , LastDecade, LastSeason
  , Participations, Won, Competition
  )
where

--------------------------------------------------------------------------------

import           Data.Word
  ( Word16
  , Word8
  )

import           Data.PCx.Byte.Common
  ( ByteStream (bytes)
  )

--------------------------------------------------------------------------------

type LastDecade = [Word16]
type LastSeason = [Word8]

type Participations = Word8
type Won            = Word8
type Competition    = (Participations, Won)

--------------------------------------------------------------------------------

data Stats = T
  { unknown00        :: Word16
  , unknown01        :: Word8
  , lastdecade       :: LastDecade
  , seasons          :: Word8
  , matches          :: Word16
  , win              :: Word16
  , draw             :: Word16
  , gf               :: Word16
  , ga               :: Word16
  , points           :: Word16
  , champion         :: Word8
  , runnersup        :: Word8
  , lastseason       :: LastSeason
  , uefa             :: Competition
  , cup              :: Competition
  , championsleague  :: Competition
  , cupwinnerscup    :: Competition
  , supercup         :: Competition
  , intercontinental :: Competition
  , eufasupercup     :: Competition
  }
  deriving
    ( Eq
    , Ord
    , Read
    , Show
    )

--------------------------------------------------------------------------------

instance ByteStream Stats where
  bytes s =
    bytes (unknown00 s) ++
    bytes (unknown01 s) ++
    xs ++
    bytes (seasons s) ++
    bytes (matches s) ++
    bytes (win s) ++
    bytes (draw s) ++
    bytes (gf s) ++
    bytes (ga s) ++
    bytes (points s) ++
    bytes (champion s) ++
    bytes (runnersup s) ++
    ys ++
    bs (uefa s) ++
    bs (cup s) ++
    bs (championsleague s) ++
    bs (cupwinnerscup s) ++
    bs (supercup s) ++
    bs (intercontinental s) ++
    bs (eufasupercup s) ++
    zs
    where
      xs = take 020 $ (++ repeat 0x00) $ concatMap bytes $ lastdecade s
      ys = take 046 $ (++ repeat 0x00) $ concatMap bytes $ lastseason s
      zs = take 028 $     repeat 0x00
      bs (x,y) =
        bytes x ++
        bytes y

--------------------------------------------------------------------------------

stats
  :: Word16
  -> Word8
  -> LastDecade
  -> Word8
  -> Word16
  -> Word16
  -> Word16
  -> Word16
  -> Word16
  -> Word16
  -> Word8
  -> Word8
  -> LastSeason
  -> Competition
  -> Competition
  -> Competition
  -> Competition
  -> Competition
  -> Competition
  -> Competition
  -> Stats
stats =
  T
