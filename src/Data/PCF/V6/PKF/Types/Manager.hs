--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCF.V6.PKF.Types.Manager
  ( Core
    ( mid
    , name
    )
  , Database
    ( fullname
    , mdunknown00
    , tactics
    , honours
    , miscellaneous
    , lastseason
    , managercareer
    , mdunknown01
    , playercareer
    , statements
    )
  , Manager
    ( S -- Simple
    , E -- Extended
    )
  , manager
  , managerdb
  )
where

--------------------------------------------------------------------------------

import           Data.Word
  ( Word16
  , Word8
  )

import           Data.PCx.Byte.Common
  ( ByteStream (bytes)
  )

--------------------------------------------------------------------------------

data Core = C
  { mid  :: Word16
  , name :: String
  }
  deriving
    ( Eq
    , Ord
    , Read
    , Show
    )

data Database = D
  { fullname      :: String
  , mdunknown00   :: String
  , tactics       :: String
  , honours       :: String
  , miscellaneous :: String
  , lastseason    :: String
  , managercareer :: String
  , mdunknown01   :: Word8
  , playercareer  :: String
  , statements    :: String
  }
  deriving
    ( Eq
    , Ord
    , Read
    , Show
    )

data Manager
  = S Core
  | E Core Database
  deriving
    ( Eq
    , Ord
    , Read
    , Show
    )

--------------------------------------------------------------------------------

instance ByteStream Core where
  bytes c =
    bytes (mid c) ++
    bytes (name c)

instance ByteStream Database where
  bytes d =
    bytes (fullname d) ++
    bytes (mdunknown00 d) ++
    bytes (tactics d) ++
    bytes (honours d) ++
    bytes (miscellaneous d) ++
    bytes (lastseason d) ++
    bytes (managercareer d) ++
    bytes (mdunknown01 d) ++
    bytes (playercareer d) ++
    bug
    where
      bug =
        if 0x00 == mdunknown01 d
        then
          [                  ]
        else
          bytes (statements d)

instance ByteStream Manager where
  bytes (S c) =
    bytes c
  bytes (E c d) =
    bytes c ++ bytes d

--------------------------------------------------------------------------------

manager
  :: Word16
  -> String
  -> Manager
manager a b =
  S $ C a b

managerdb
  :: Word16
  -> String
  -> String
  -> String
  -> String
  -> String
  -> String
  -> String
  -> String
  -> Word8
  -> String
  -> String
  -> Manager
managerdb a b c d e f g h i j k l =
  E (C a b) (D c d e f g h i j k l)
