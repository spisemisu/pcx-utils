--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCF.V6.PKF.Types.Player
  ( Status
    ( Veteran
    , Signing
    , Academy
    , OnLeave
    )
  , Role
    ( GK
    , RB
    , LB
    , SW
    , LCB
    , RCB
    , RM
    , RCM
    , ST
    , CAM
    , LM
    , RW
    , CF
    , LW
    , CDM
    , LF
    , RF
    , LCM
    )
  , Skin
    ( Skin
    , Light
    , Dark
    , Medium
    )
  , Hair
    ( Hair
    , Blond
    , Bald
    , Black
    , Grey
    , Redhead
    , Brown
    )
  , Position
    ( KEP
    , DEF
    , MID
    , FOR
    )
  , Core
    ( pid
    , number
    , name
    , fullname
    , index
    , status
    , roles
    , citizenship
    , skin
    , hair
    , position
    , birthday
    , height
    , weight
    , pace
    , stamina
    , aggression
    , skill
    , finishing
    , dribbling
    , passing
    , shooting
    , tackling
    , goalkeeping
    )
  , Database
    ( country
    , birthplace
    , fromteam
    , nationalteam
    , pdunknown00
    , features
    , honours
    , intcaps
    , miscellaneous
    , lastseason
    , career
    )
  , Player
    ( S -- Simple
    , E -- Extended
    )
  , player
  , playerdb
  )
where

--------------------------------------------------------------------------------

import           Data.Word
  ( Word16
  , Word8
  )

import           Data.PCF.V6.PKF.Types.Country as PKF
import           Data.PCx.Byte.Common
  ( ByteStream (bytes)
  , Date
  )

--------------------------------------------------------------------------------

data Status
  = Veteran
  | Signing
  | Academy
  | OnLeave
  deriving
    ( Eq
    , Ord
    , Read
    , Show
    )

data Role
  = GK
  | RB  -- Right Back
  | LB  -- Left Back
  | SW  -- Sweeper
  | LCB -- Left Center Back
  | RCB -- Right Center Back
  | RM  -- Right Midfielder
  | RCM -- Right Centre Midfielder
  | ST  -- Striker
  | CAM -- Centre Attacking Midfielder
  | LM  -- Left Midfielder
  | RW  -- Right Wing
  | CF  -- Centre Forward
  | LW  -- Left Wing
  | CDM -- Centre Defensive Midfielder
  | LF  -- Left Forward
  | RF  -- Right Forward
  | LCM -- Left Centre Midfielder
  deriving
    ( Eq
    , Ord
    , Read
    , Show
    )

data Skin
  = Skin
  | Light
  | Dark
  | Medium
  deriving
    ( Eq
    , Ord
    , Read
    , Show
    )

data Hair
  = Hair
  | Blond
  | Bald
  | Black
  | Grey
  | Redhead
  | Brown
  deriving
    ( Eq
    , Ord
    , Read
    , Show
    )

data Position
  = KEP -- Keeper
  | DEF -- Defender
  | MID -- Midfielder
  | FOR -- Forward
  deriving
    ( Eq
    , Ord
    , Read
    , Show
    )

--------------------------------------------------------------------------------

data Core = C
  { pid         :: Word16
  , number      :: Word8
  , name        :: String
  , fullname    :: String
  , index       :: Word8
  , status      :: Status
  , roles       :: [Role]
  , citizenship :: PKF.Country
  , skin        :: Skin
  , hair        :: Hair
  , position    :: Position
  , birthday    :: Date
  , height      :: Word8
  , weight      :: Word8
  , pace        :: Word8
  , stamina     :: Word8
  , aggression  :: Word8
  , skill       :: Word8
  , finishing   :: Word8
  , dribbling   :: Word8
  , passing     :: Word8
  , shooting    :: Word8
  , tackling    :: Word8
  , goalkeeping :: Word8
  }
  deriving
    ( Eq
    , Ord
    , Read
    , Show
    )

data Database = D
  { country       :: Word8
  , birthplace    :: String
  , fromteam      :: String
  , nationalteam  :: String
  , pdunknown00   :: String
  , features      :: String
  , honours       :: String
  , intcaps       :: String
  , miscellaneous :: String
  , lastseason    :: String
  , career        :: String
  }
  deriving
    ( Eq
    , Ord
    , Read
    , Show
    )

data Player
  = S Core
  | E Core Database
  deriving
    ( Eq
    , Ord
    , Read
    , Show
    )

--------------------------------------------------------------------------------

instance ByteStream Status where
  bytes Veteran = [0x00]
  bytes Signing = [0x01]
  bytes Academy = [0x02]
  bytes OnLeave = [0x03]

instance ByteStream Role where
  bytes GK  = [0x01]
  bytes RB  = [0x02]
  bytes LB  = [0x03]
  bytes SW  = [0x04]
  bytes LCB = [0x05]
  bytes RCB = [0x06]
  bytes RM  = [0x07]
  bytes RCM = [0x08]
  bytes ST  = [0x09]
  bytes CAM = [0x0A]
  bytes LM  = [0x0B]
  bytes RW  = [0x0C]
  bytes CF  = [0x0D]
  bytes LW  = [0x0E]
  bytes CDM = [0x0F]
  bytes LF  = [0x10]
  bytes RF  = [0x11]
  bytes LCM = [0x12]

instance ByteStream Skin where
  bytes Skin   = [0x00]
  bytes Light  = [0x01]
  bytes Dark   = [0x02]
  bytes Medium = [0x03]

instance ByteStream Hair where
  bytes Hair    = [0x00]
  bytes Blond   = [0x01]
  bytes Bald    = [0x02]
  bytes Black   = [0x03]
  bytes Grey    = [0x04]
  bytes Redhead = [0x05]
  bytes Brown   = [0x06]

instance ByteStream Position where
  bytes KEP = [0x00]
  bytes DEF = [0x01]
  bytes MID = [0x02]
  bytes FOR = [0x03]

--------------------------------------------------------------------------------

instance ByteStream Player where
  bytes (S c) =
    bytes (pid c) ++
    bytes (number c) ++
    bytes (name c) ++
    bytes (fullname c) ++
    bytes (index c) ++
    bytes (status c) ++
    xs ++
    bytes (citizenship c) ++
    bytes (skin c) ++
    bytes (hair c) ++
    bytes (position c) ++
    dt (birthday c) ++
    bytes (height c) ++
    bytes (weight c) ++
    bytes (pace c) ++
    bytes (stamina c) ++
    bytes (aggression c) ++
    bytes (skill c) ++
    bytes (finishing c) ++
    bytes (dribbling c) ++
    bytes (passing c) ++
    bytes (shooting c) ++
    bytes (tackling c) ++
    bytes (goalkeeping c)
    where
      xs = take 006 $ (++ repeat 0x00) $ concatMap bytes $ roles c
      dt (yyyy,mm,dd) =
        bytes dd ++
        bytes mm ++
        bytes yyyy

  bytes (E c d) =
    bytes (pid c) ++
    bytes (number c) ++
    bytes (name c) ++
    bytes (fullname c) ++
    bytes (index c) ++
    bytes (status c) ++
    xs ++
    bytes (citizenship c) ++
    bytes (skin c) ++
    bytes (hair c) ++
    bytes (position c) ++
    dt (birthday c) ++
    bytes (height c) ++
    bytes (weight c) ++
    --
    bytes (country d) ++
    bytes (birthplace d) ++
    bytes (fromteam d) ++
    bytes (nationalteam d) ++
    bytes (pdunknown00 d) ++
    bytes (features d) ++
    bytes (honours d) ++
    bytes (intcaps d) ++
    bytes (miscellaneous d) ++
    bytes (lastseason d) ++
    bytes (career d) ++
    --
    bytes (pace c) ++
    bytes (stamina c) ++
    bytes (aggression c) ++
    bytes (skill c) ++
    bytes (finishing c) ++
    bytes (dribbling c) ++
    bytes (passing c) ++
    bytes (shooting c) ++
    bytes (tackling c) ++
    bytes (goalkeeping c)
    where
      xs = take 006 $ (++ repeat 0x00) $ concatMap bytes $ roles c
      dt (yyyy,mm,dd) =
        bytes dd ++
        bytes mm ++
        bytes yyyy

--------------------------------------------------------------------------------

player
  :: Word16
  -> Word8
  -> String
  -> String
  -> Word8
  -> Status
  -> [Role]
  -> PKF.Country
  -> Skin
  -> Hair
  -> Position
  -> Date
  -> Word8
  -> Word8
  -> Word8
  -> Word8
  -> Word8
  -> Word8
  -> Word8
  -> Word8
  -> Word8
  -> Word8
  -> Word8
  -> Word8
  -> Player
player
  ca cb cc cd ce cf cg ch ci cj ck cl cm cn
  co cp cq cr cs ct cu cv cw cx =
  S c
  where
    c =
      C
        ca cb cc cd ce cf cg ch ci cj ck cl cm cn
        co cp cq cr cs ct cu cv cw cx

playerdb
  :: Word16
  -> Word8
  -> String
  -> String
  -> Word8
  -> Status
  -> [Role]
  -> PKF.Country
  -> Skin
  -> Hair
  -> Position
  -> Date
  -> Word8
  -> Word8
  -> Word8
  -> String
  -> String
  -> String
  -> String
  -> String
  -> String
  -> String
  -> String
  -> String
  -> String
  -> Word8
  -> Word8
  -> Word8
  -> Word8
  -> Word8
  -> Word8
  -> Word8
  -> Word8
  -> Word8
  -> Word8
  -> Player
playerdb
  ca cb cc cd ce cf cg ch ci cj ck cl cm cn
  ea eb ec ed ee ef eg eh ei ej ek
  co cp cq cr cs ct cu cv cw cx =
  E c d
  where
    c =
      C
        ca cb cc cd ce cf cg ch ci cj ck cl cm cn
        co cp cq cr cs ct cu cv cw cx
    d =
      D ea eb ec ed ee ef eg eh ei ej ek

--------------------------------------------------------------------------------

-- HELPERS

-- mapM_ (putStr . (++ " ") . ("c" ++) . (:[])) $ take 24 ['a' .. 'z']
-- mapM_ (putStr . (++ " ") . ("e" ++) . (:[])) $ take 09 ['a' .. 'z']
