--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCF.V6.PKF.Types.Tactics
  ( PlayStyle
    ( Attacking, Speculative, Mixed
    )
  , Tackling
    ( Soft, Medium, Aggresive
    )
  , Coverage
    ( Zone, ManToMan
    )
  , Clearance
    ( Short, Long
    )
  , Preasure
    ( Own, Midfield, Opponent
    )
  , Tactics
    ( possession
    , counter
    , playstyle
    , tackling
    , coverage
    , clearance
    , preasure
    )
  , tactics
  )
where

--------------------------------------------------------------------------------

import           Data.Word
  ( Word8
  )

import           Data.PCx.Byte.Common
  ( ByteStream (bytes)
  )

--------------------------------------------------------------------------------

data PlayStyle
  = Attacking
  | Speculative
  | Mixed
  deriving
    ( Eq
    , Ord
    , Read
    , Show
    )

data Tackling
  = Soft
  | Medium
  | Aggresive
  deriving
    ( Eq
    , Ord
    , Read
    , Show
    )

data Coverage
  = Zone
  | ManToMan
  deriving
    ( Eq
    , Ord
    , Read
    , Show
    )

data Clearance
  = Short
  | Long
  deriving
    ( Eq
    , Ord
    , Read
    , Show
    )

data Preasure
  = Own
  | Midfield
  | Opponent
  deriving
    ( Eq
    , Ord
    , Read
    , Show
    )

--------------------------------------------------------------------------------

data Tactics = T
  -- ATTACK
  { possession :: Word8 -- Passing vs Long Ball in percentage
  , counter    :: Word8 -- in percentage
  , playstyle  :: PlayStyle
  -- DEFENCE
  , tackling   :: Tackling
  , coverage   :: Coverage
  , clearance  :: Clearance
  , preasure   :: Preasure
  }
  deriving
    ( Eq
    , Ord
    , Read
    , Show
    )

--------------------------------------------------------------------------------

instance ByteStream PlayStyle where
  bytes Attacking   = [0x00]
  bytes Speculative = [0x01]
  bytes Mixed       = [0x02]

instance ByteStream Tackling where
  bytes Soft      = [0x00]
  bytes Medium    = [0x01]
  bytes Aggresive = [0x02]

instance ByteStream Coverage where
  bytes Zone     = [0x00]
  bytes ManToMan = [0x01]

instance ByteStream Clearance where
  bytes Short = [0x00]
  bytes Long  = [0x01]

instance ByteStream Preasure where
  bytes Own      = [0x00]
  bytes Midfield = [0x01]
  bytes Opponent = [0x02]

--------------------------------------------------------------------------------

instance ByteStream Tactics where
  bytes t =
    bytes (possession t) ++
    bytes (counter t) ++
    bytes (playstyle t) ++
    bytes (tackling t) ++
    bytes (coverage t) ++
    bytes (clearance t) ++
    bytes (preasure t)

--------------------------------------------------------------------------------

tactics
  :: Word8
  -> Word8
  -> PlayStyle
  -> Tackling
  -> Coverage
  -> Clearance
  -> Preasure
  -> Tactics
tactics =
  T
