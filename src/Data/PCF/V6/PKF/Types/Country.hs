--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCF.V6.PKF.Types.Country
  ( Country
    ( XXX
    , ALB
    , DEU
    , ARG
    , AUS
    , AUT
    , AZE
    , BLR
    , BOL
    , BIH
    , BRA
    , BGR
    , BEL
    , CMR
    , CHL
    , CYP
    , COL
    , HRV
    , DNK
    , GBR_SCT
    , SVK
    , SVN
    , ESP
    , FIN
    , FRA
    , GHA
    , GRC
    , NLD
    , HND
    , HUN
    , GBR_ENG
    , IRL
    , GBR_NIR
    , ISL
    , FRO
    , ISR
    , ITA
    , LTU
    , LUX
    , MKD
    , MLT
    , MAR
    , MDA
    , NGA
    , NOR
    , GBR_WLS
    , POL
    , PRT
    , CZE
    , ROU
    , RUS
    , SRB
    , ZAF
    , SWE
    , CHE
    , TUR
    , UKR
    , URY
    , YUG
    , PER
    , CAN
    , USA
    , GEO
    , CRI
    , PRY
    , JPN
    , DZA
    , TTO
    , SEN
    , SUR
    , ZMB
    , CPV
    , VEN
    , RHO
    , SGP
    , AND
    , MOZ
    , LIE
    , LBR
    , PAN
    , ZAR
    , TJK
    , UZB
    , MEX
    , GIN
    , AGO
    , ZWE
    , SLE
    , GLP
    , ECU
    , EST
    , GNB
    , LBY
    , EGY
    , JAM
    , NCL
    , BMU
    , NZL
    , GUF
    , VCT
    , TCD
    , TGO
    , GIN_C
    , TZA
    , BFA
    , GMB
    , RWA
    , KEN
    , MRT
    , MLI
    , UGA
    , COG
    , LVA
    , CIV
    , ARM
    , NIC
    , ESP_CAT
    , NER
    , BRB
    , IRN
    , QAT
    , TUN
    , GAB
    , PYF
    , MUS
    , MDG
    , MTQ
    , VNM
    )
  )
where

--------------------------------------------------------------------------------

import           Data.PCx.Byte.Common
  ( ByteStream (bytes)
  )

--------------------------------------------------------------------------------

data Country -- ISO-3166
  = XXX
  | ALB     -- ALBANIA
  | DEU     -- GERMANY
  | ARG     -- ARGENTINA
  | AUS     -- AUSTRALIA
  | AUT     -- AUSTRIA
  | AZE     -- AZERBAIJAN
  | BLR     -- BELARUS
  | BOL     -- BOLIVIA
  | BIH     -- BOSNIA AND HERZEGOVINA
  | BRA     -- BRAZIL
  | BGR     -- BULGARIA
  | BEL     -- BELGIUM
  | CMR     -- CAMEROON
  | CHL     -- CHILE
  | CYP     -- CYPRUS
  | COL     -- COLOMBIA
  | HRV     -- CROATIA
  | DNK     -- DENMARK
  | GBR_SCT -- SCOTLAND
  | SVK     -- SLOVAKIA
  | SVN     -- SLOVENIA
  | ESP     -- SPAIN
  | FIN     -- FINLAND
  | FRA     -- FRANCE
  | GHA     -- GHANA
  | GRC     -- GREECE
  | NLD     -- NETHERLANDS
  | HND     -- HONDURAS
  | HUN     -- HUNGARY
  | GBR_ENG -- ENGLAND
  | IRL     -- IRELAND
  | GBR_NIR -- NORTH. IRELAND
  | ISL     -- ICELAND
  | FRO     -- FAROE ISLANDS
  | ISR     -- ISRAEL
  | ITA     -- ITALY
  | LTU     -- LITHUANIA
  | LUX     -- LUXEMBOURG
  | MKD     -- MACEDONIA
  | MLT     -- MALTA
  | MAR     -- MOROCCO
  | MDA     -- MOLDOVA
  | NGA     -- NIGERIA
  | NOR     -- NORWAY
  | GBR_WLS -- WALES
  | POL     -- POLAND
  | PRT     -- PORTUGAL
  | CZE     -- CZECHIA
  | ROU     -- ROMANIA
  | RUS     -- RUSSIA
  | SRB     -- SERBIA
  | ZAF     -- SOUTH AFRICA
  | SWE     -- SWEDEN
  | CHE     -- SWITZERLAND
  | TUR     -- TURKEY
  | UKR     -- UKRAINE
  | URY     -- URUGUAY
  | YUG     -- YUGOSLAVIA
  | PER     -- PERU
  | CAN     -- CANADA
  | USA     -- U.S.A.
  | GEO     -- GEORGIA
  | CRI     -- COSTA RICA
  | PRY     -- PARAGUAY
  | JPN     -- JAPAN
  | DZA     -- ALGERIA
  | TTO     -- TRINIDAD AND TOBAGO
  | SEN     -- SENEGAL
  | SUR     -- SURINAME
  | ZMB     -- ZAMBIA
  | CPV     -- CABO VERDE
  | VEN     -- VENEZUELA
  | RHO     -- RHODESIA
  | SGP     -- SINGAPORE
  | AND     -- ANDORRA
  | MOZ     -- MOZAMBIQUE
  | LIE     -- LIECHTENSTEIN
  | LBR     -- LIBERIA
  | PAN     -- PANAMA
  | ZAR     -- ZAIRE
  | TJK     -- TAJIKISTAN
  | UZB     -- UZBEKISTHAN
  | MEX     -- MEXICO
  | GIN     -- GUINEA
  | AGO     -- ANGOLA
  | ZWE     -- ZIMBABWE
  | SLE     -- SIERRA LEONE
  | GLP     -- GUADELOUPE
  | ECU     -- ECUADOR
  | EST     -- ESTONIA
  | GNB     -- GUINEA-BISSAU
  | LBY     -- LIBYA
  | EGY     -- EGYPT
  | JAM     -- JAMAICA
  | NCL     -- NEW CALEDONIA
  | BMU     -- BERMUDA
  | NZL     -- NEW ZEALAND
  | GUF     -- FRENCH GUIANA
  | VCT     -- SAINT VINCENT AND THE GRENADINES
  | TCD     -- CHAD
  | TGO     -- TOGO
  | GIN_C   -- GUINEA CONAKRY
  | TZA     -- TANZANIA
  | BFA     -- BURKINA FASO
  | GMB     -- GAMBIA
  | RWA     -- RWANDA
  | KEN     -- KENYA
  | MRT     -- MAURITANIA
  | MLI     -- MALI
  | UGA     -- UGANDA
  | COG     -- CONGO
  | LVA     -- LATVIA
  | CIV     -- CÔTE D'IVOIRE
  | ARM     -- ARMENIA
  | NIC     -- NICARAGUA
  | ESP_CAT -- CATALUÑA
  | NER     -- NIGER
  | BRB     -- BARBADOS
  | IRN     -- IRAN
  | QAT     -- QATAR
  | TUN     -- TUNISIA
  | GAB     -- GABON
  | PYF     -- FRENCH POLYNESIA
  | MUS     -- MAURITIUS
  | MDG     -- MADAGASCAR
  | MTQ     -- MARTINIQUE
  | VNM     -- VIETNAM
  deriving
    ( Eq
    , Ord
    , Read
    , Show
    )

--------------------------------------------------------------------------------

instance ByteStream Country where
  bytes XXX     = [0x00]
  bytes ALB     = [0x01]
  bytes DEU     = [0x02]
  bytes ARG     = [0x03]
  bytes AUS     = [0x04]
  bytes AUT     = [0x05]
  bytes AZE     = [0x06]
  bytes BLR     = [0x07]
  bytes BOL     = [0x08]
  bytes BIH     = [0x09]
  bytes BRA     = [0x0A]
  bytes BGR     = [0x0B]
  bytes BEL     = [0x0C]
  bytes CMR     = [0x0D]
  bytes CHL     = [0x0E]
  bytes CYP     = [0x0F]
  bytes COL     = [0x10]
  bytes HRV     = [0x11]
  bytes DNK     = [0x12]
  bytes GBR_SCT = [0x13]
  bytes SVK     = [0x14]
  bytes SVN     = [0x15]
  bytes ESP     = [0x16]
  bytes FIN     = [0x17]
  bytes FRA     = [0x18]
  bytes GHA     = [0x19]
  bytes GRC     = [0x1A]
  bytes NLD     = [0x1B]
  bytes HND     = [0x1C]
  bytes HUN     = [0x1D]
  bytes GBR_ENG = [0x1E]
  bytes IRL     = [0x1F]
  bytes GBR_NIR = [0x20]
  bytes ISL     = [0x21]
  bytes FRO     = [0x22]
  bytes ISR     = [0x23]
  bytes ITA     = [0x24]
  bytes LTU     = [0x25]
  bytes LUX     = [0x26]
  bytes MKD     = [0x27]
  bytes MLT     = [0x28]
  bytes MAR     = [0x29]
  bytes MDA     = [0x2A]
  bytes NGA     = [0x2B]
  bytes NOR     = [0x2C]
  bytes GBR_WLS = [0x2D]
  bytes POL     = [0x2E]
  bytes PRT     = [0x2F]
  bytes CZE     = [0x30]
  bytes ROU     = [0x31]
  bytes RUS     = [0x32]
  bytes SRB     = [0x33]
  bytes ZAF     = [0x34]
  bytes SWE     = [0x35]
  bytes CHE     = [0x36]
  bytes TUR     = [0x37]
  bytes UKR     = [0x38]
  bytes URY     = [0x39]
  bytes YUG     = [0x3A]
  bytes PER     = [0x3B]
  bytes CAN     = [0x3C]
  bytes USA     = [0x3D]
  bytes GEO     = [0x3E]
  bytes CRI     = [0x3F]
  bytes PRY     = [0x40]
  bytes JPN     = [0x41]
  bytes DZA     = [0x42]
  bytes TTO     = [0x43]
  bytes SEN     = [0x44]
  bytes SUR     = [0x45]
  bytes ZMB     = [0x46]
  bytes CPV     = [0x47]
  bytes VEN     = [0x48]
  bytes RHO     = [0x49]
  bytes SGP     = [0x4A]
  bytes AND     = [0x4B]
  bytes MOZ     = [0x4C]
  bytes LIE     = [0x4D]
  bytes LBR     = [0x4E]
  bytes PAN     = [0x4F]
  bytes ZAR     = [0x50]
  bytes TJK     = [0x51]
  bytes UZB     = [0x52]
  bytes MEX     = [0x53]
  bytes GIN     = [0x54]
  bytes AGO     = [0x55]
  bytes ZWE     = [0x56]
  bytes SLE     = [0x57]
  bytes GLP     = [0x58]
  bytes ECU     = [0x59]
  bytes EST     = [0x5A]
  bytes GNB     = [0x5B]
  bytes LBY     = [0x5C]
  bytes EGY     = [0x5D]
  bytes JAM     = [0x5E]
  bytes NCL     = [0x5F]
  bytes BMU     = [0x60]
  bytes NZL     = [0x61]
  bytes GUF     = [0x62]
  bytes VCT     = [0x63]
  bytes TCD     = [0x64]
  bytes TGO     = [0x65]
  bytes GIN_C   = [0x66]
  bytes TZA     = [0x67]
  bytes BFA     = [0x68]
  bytes GMB     = [0x69]
  bytes RWA     = [0x6A]
  bytes KEN     = [0x6B]
  bytes MRT     = [0x6C]
  bytes MLI     = [0x6D]
  bytes UGA     = [0x6E]
  bytes COG     = [0x6F]
  bytes LVA     = [0x70]
  bytes CIV     = [0x71]
  bytes ARM     = [0x72]
  bytes NIC     = [0x73]
  bytes ESP_CAT = [0x74]
  bytes NER     = [0x75]
  bytes BRB     = [0x76]
  bytes IRN     = [0x77]
  bytes QAT     = [0x78]
  bytes TUN     = [0x79]
  bytes GAB     = [0x7A]
  bytes PYF     = [0x7B]
  bytes MUS     = [0x7C]
  bytes MDG     = [0x7D]
  bytes MTQ     = [0x7E]
  bytes VNM     = [0x7F]
