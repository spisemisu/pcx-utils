--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCF.V6.PKF.Types.TextLists
  ( TextLists
    ( TextLists
    )
  , textlists
  )
where

--------------------------------------------------------------------------------

import qualified Data.PCF.V6.PKF.Types.TextList as PKF

--------------------------------------------------------------------------------

data TextLists =
  TextLists [PKF.TextList]
  deriving
    ( Eq
    , Ord
    , Read
    , Show
    )

--------------------------------------------------------------------------------

textlists
  :: [PKF.TextList]
  -> TextLists
textlists =
  TextLists
