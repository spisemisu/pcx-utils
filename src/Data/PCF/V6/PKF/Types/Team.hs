--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCF.V6.PKF.Types.Team
  ( Core
    ( tid
    , name
    , stadium
    , country
    , tunknown01
    , fullname
    , capacity
    , standing
    , width
    , length
    , founded
    --
    , formation
    , tactics
    , staff
    )
  , Database
    ( built
    , members
    , president
    , budget
    , budgetpro
    , sponsor
    , supplier
    , reserve
    , stats
    )
  , Team
    ( S -- Simple
    , E -- Extended
    )
  , team
  , teamdb
  )
where

--------------------------------------------------------------------------------

import           Prelude                         hiding
  ( length
  )

import           Data.Word
  ( Word16
  , Word32
  , Word8
  )

import           Data.PCF.V6.PKF.Notice
  ( cdm
  )
import           Data.PCF.V6.PKF.Types.Country
  ( Country
  )
import           Data.PCF.V6.PKF.Types.Formation
  ( Formation
  )
import           Data.PCF.V6.PKF.Types.Staff
  ( Staff
  )
import           Data.PCF.V6.PKF.Types.Stats
  ( Stats
  )
import           Data.PCF.V6.PKF.Types.Tactics
  ( Tactics
  )
import           Data.PCx.Byte.Common
  ( ByteStream (bytes)
  )

--------------------------------------------------------------------------------

data Core = C
  { tid        :: Word16
  , tunknown00 :: Word16
  , name       :: String
  , stadium    :: String
  , country    :: Country
  , tunknown01 :: Word8
  , fullname   :: String
  , capacity   :: Word32
  , standing   :: Word32
  , width      :: Word16
  , length     :: Word16
  , founded    :: Word16
  --
  , formation  :: Formation
  , tactics    :: Tactics
  , staff      :: [Staff]
  }
  deriving
    ( Eq
    , Ord
    , Read
    , Show
    )

data Database = D
  { built     :: Word16
  , members   :: Word32
  , president :: String
  , budget    :: Word32
  , budgetpro :: Word32
  , sponsor   :: String
  , supplier  :: String
  , reserve   :: Word16
  , stats     :: Stats
  }
  deriving
    ( Eq
    , Ord
    , Read
    , Show
    )

data Team
  = S Core
  | E Core Database
  deriving
    ( Eq
    , Ord
    , Read
    , Show
    )

--------------------------------------------------------------------------------

instance ByteStream Team where
  bytes (S c) =
    cdm ++
    bytes (tunknown00 c) ++
    xs ++ ys ++ zs ++
    bytes (name c) ++
    bytes (stadium c) ++
    bytes (country c) ++
    --bytes (tunknown01 c) ++
    bytes (fullname c) ++
    bytes (capacity c) ++
    bytes (standing c) ++
    bytes (width c) ++
    bytes (length c) ++
    bytes (founded c) ++
    --
    bytes (formation c) ++
    bytes (tactics c) ++
    concatMap
    bytes (staff c) ++
    ts
    where
      xs = [0x03, 0x02]
      ys = [0x00]
      zs = [0x01]
      ts = [0x00]

  bytes (E c d) =
    cdm ++
    bytes (tunknown00 c) ++
    xs ++ ys ++ zs ++
    bytes (name c) ++
    bytes (stadium c) ++
    bytes (country c) ++
    --bytes (tunknown01 c) ++
    bytes (fullname c) ++
    bytes (capacity c) ++
    bytes (standing c) ++
    bytes (width c) ++
    bytes (length c) ++
    bytes (founded c) ++
    --
    --bytes (built d) ++
    bytes (members d) ++
    bytes (president d) ++
    bytes (budget d) ++
    bytes (budgetpro d) ++
    bytes (sponsor d) ++
    bytes (supplier d) ++
    bytes (reserve d) ++
    bytes (stats d) ++
    --
    bytes (formation c) ++
    bytes (tactics c) ++
    concatMap
    bytes (staff c) ++
    ts
    where
      xs = [0x03, 0x02]
      ys = [0x00]
      zs = [0x00]
      ts = [0x00]

--------------------------------------------------------------------------------

team
  :: Word16
  -> Word16
  -> String
  -> String
  -> Country
  -> Word8
  -> String
  -> Word32
  -> Word32
  -> Word16
  -> Word16
  -> Word16
  -> Formation
  -> Tactics
  -> [Staff]
  -> Team
team
  ca cb cc cd ce cf cg ch ci cj ck cl
  cm cn co =
  S c
  where
    c =
      C
        ca cb cc cd ce cf cg ch ci cj ck cl
        cm cn co

teamdb
  :: Word16
  -> Word16
  -> String
  -> String
  -> Country
  -> Word8
  -> String
  -> Word32
  -> Word32
  -> Word16
  -> Word16
  -> Word16
  --
  -> Word16
  -> Word32
  -> String
  -> Word32
  -> Word32
  -> String
  -> String
  -> Word16
  -> Stats
  --
  -> Formation
  -> Tactics
  -> [Staff]
  -> Team
teamdb
  ca cb cc cd ce cf cg ch ci cj ck cl
  ea eb ec ed ee ef eg eh ei
  cm cn co =
  E c d
  where
    c =
      C
        ca cb cc cd ce cf cg ch ci cj ck cl
        cm cn co
    d =
      D ea eb ec ed ee ef eg eh ei

--------------------------------------------------------------------------------

-- HELPERS

-- mapM_ (putStr . (++ " ") . ("c" ++) . (:[])) $ take 24 ['a' .. 'z']
-- mapM_ (putStr . (++ " ") . ("e" ++) . (:[])) $ take 09 ['a' .. 'z']
