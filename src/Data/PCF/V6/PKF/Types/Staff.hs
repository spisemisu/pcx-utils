--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCF.V6.PKF.Types.Staff
  ( Staff
    ( M
    , P
    )
  , manager
  , player
  )
where

--------------------------------------------------------------------------------

import           Data.PCF.V6.PKF.Types.Manager
  ( Manager
  )
import           Data.PCF.V6.PKF.Types.Player
  ( Player
  )
import           Data.PCx.Byte.Common
  ( ByteStream (bytes)
  )

--------------------------------------------------------------------------------

data Staff
  = M Manager
  | P Player
  deriving
    ( Eq
    , Ord
    , Read
    , Show
    )

--------------------------------------------------------------------------------

instance ByteStream Staff where
  bytes (M m) =
    [0x02] ++ bytes m
  bytes (P p) =
    [0x01] ++ bytes p

--------------------------------------------------------------------------------

manager
  :: Manager
  -> Staff
manager =
  M

player
  :: Player
  -> Staff
player =
  P
