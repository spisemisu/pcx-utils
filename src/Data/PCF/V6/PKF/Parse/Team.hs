--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCF.V6.PKF.Parse.Team
  ( teamP
  )
where

--------------------------------------------------------------------------------

import           Data.Word
  ( Word16
  , Word32
  , Word8
  )

import           Data.PCF.V6.PKF.Notice
  ( cdm
  )
import qualified Data.PCF.V6.PKF.Parse.Country   as PKF
import qualified Data.PCF.V6.PKF.Parse.Formation as PKF
import qualified Data.PCF.V6.PKF.Parse.Staff     as PKF
import qualified Data.PCF.V6.PKF.Parse.Stats     as PKF
import qualified Data.PCF.V6.PKF.Parse.Tactics   as PKF
import qualified Data.PCF.V6.PKF.Types.Country   as PKF
import qualified Data.PCF.V6.PKF.Types.Formation as PKF
import qualified Data.PCF.V6.PKF.Types.Staff     as PKF
import qualified Data.PCF.V6.PKF.Types.Stats     as PKF
import qualified Data.PCF.V6.PKF.Types.Tactics   as PKF
import qualified Data.PCF.V6.PKF.Types.Team      as PKF
import           Data.PCx.Byte.Parser
  ( Parser
  , byteP
  , bytesP
  , hextetP
  , many
  , octetP
  , quadletP
  , stringP
  , (<|>)
  )

--------------------------------------------------------------------------------

teamP
  :: Word16
  -> Parser PKF.Team
teamP tid =
  aux
  <$> alt
  <*> termP
  where
    aux t _ = t
    alt =
      dbcteamsP tid <|> teamindbP tid <|> teamnodbP tid

teamindbP
  :: Word16
  -> Parser PKF.Team
teamindbP tid =
  aux
  <$> bytesP cdm
  <*> tunknown00P
  <*> bytesP [0x0d, 0x02] -- Constant
  <*> byteP 0x00          -- Constant
  <*> byteP 0x00          -- Database
  <*> nameP
  <*> stadiumP
  <*> countryP
  <*> tunknown01P
  <*> fullnameP
  <*> capacityP
  <*> standingP
  <*> widthP
  <*> lengthP
  <*> foundedP
  --
  <*> builtP
  <*> membersP
  <*> presidentP
  <*> budgetP
  <*> budgetproP
  <*> sponsorP
  <*> supplierP
  <*> reserveP
  <*> statsP
  --
  <*> formationP
  <*> tacticsP
  <*> staffP False
  where
    aux _ u00 _ _ _ =
      PKF.teamdb tid u00

teamnodbP
  :: Word16
  -> Parser PKF.Team
teamnodbP tid =
  aux
  <$> bytesP cdm
  <*> tunknown00P
  <*> bytesP [0x0d, 0x02] -- Constant
  <*> byteP 0x00          -- Constant
  <*> byteP 0x01          -- No database
  <*> nameP
  <*> stadiumP
  <*> countryP
  <*> tunknown01P
  <*> fullnameP
  <*> capacityP
  <*> standingP
  <*> widthP
  <*> lengthP
  <*> foundedP
  --
  <*> formationP
  <*> tacticsP
  <*> staffP True
  where
    aux _ u00 _ _ _ =
      PKF.team tid u00

dbcteamsP
  :: Word16
  -> Parser PKF.Team
dbcteamsP tid =
  aux
  <$> bytesP cdm
  <*> tunknown00P
  <*> bytesP [0x03, 0x02] -- Constant
  <*> byteP 0x00          -- Constant
  <*> byteP 0x00          -- Database
  <*> nameP
  <*> stadiumP
  <*> countryP
  -- <*> tunknown01P -- missing
  <*> fullnameP
  <*> capacityP
  <*> standingP
  <*> widthP
  <*> lengthP
  <*> foundedP
  --
  -- <*> builtP -- missing
  <*> membersP
  <*> presidentP
  <*> budgetP
  <*> budgetproP
  <*> sponsorP
  <*> supplierP
  <*> reserveP
  <*> statsP
  --
  <*> formationP
  <*> tacticsP
  <*> staffP False
  where
    aux _ u00 _ _ _ a b c d e f g h i j k l m n o p q r s =
      PKF.teamdb
        tid u00
        a b c 0 d e f g h i 0 j k l m n o p q r s

--------------------------------------------------------------------------------

-- DEBUG

{-

Template

bytesP cdm
tunknown00P
bytesP [0x0d, 0x02] -- Constant
byteP 0x00          -- Constant
byteP 0x00          -- Database
nameP
stadiumP
countryP
tunknown01P
fullnameP
capacityP
standingP
widthP
lengthP
foundedP
| DB BEGIN
builtP
membersP
presidentP
budgetP
budgetproP
sponsorP
supplierP
reserveP
statsP
| DB END
formationP
tacticsP
staffP False -- True

-}

{-

| F.C. Barcelona (DBC working)

436f 7079 7269 6768 7420 2863 2931 3939
3620 4469 6e61 6d69 6320 4d75 6c74 696d
6564 6961 <- CDM
          cb08 <- TUNKNOWN00
               0302 <- CONSTANT (dbcteams)
                    0000 <- CONSTANT
                         0e00 274f 224f
4123 0013 0204 0d0e 0f00  <- NAME
                         0800 2200 0c11
412f 0e14 <- STADIUM
          16 <- COUNTRY (MISSING: TUNKNOWN01 one byte)
            15 0027 9b15 030e 0d41 220d
1403 4123 2033 2224 2d2e 2f20  <- FULLNAME
                              1a84 0100  <- CAPACITY
0000 0000  <- STANDING
          4600  <- WIDTH
               6900  <- LENGTH
                    6b07 <- FOUNDED (MISSING: BUILT two bytes)
                         2c04 0200 <- MEMBERS
                                   0100
4f <- PRESIDENT
  5c 4400 00 <- BUDGET
            5c 4400 00 <- BUDGETPRO
                      01 004f <- SPONSOR
                              0100 4f <- SUPPLIER
                                     1c
00 <- RESERVE
  ff ff00 0200 0500 0100 0500 0100 0a00
0700 0f00 0100 0100 0000 0000 0000 0000
0000 0000 0000 000b 0202 0202 0302 0101
0101 0101 0101 0101 0101 0201 0101 0101
0101 0303 0404 0403 0101 0101 0100 0000
0000 0000 0000 0000 0000 0000 0000 0002
0100 0000 0000 0000 0000 0000 0000 0000
0000 0000 0000 0000 0000 0000 0000 00  <- STATS
                                     00
0042 002a 0042 0000 0058 0000 0058 0000
0084 00b0 0042 002e 009e 0080 00a7 0000
0000 00d4 0058 002d 0012 007e 0007 0000
002c 0079 006e 002a 006e 0066 006f 0000
002c 0075 006e 002a 0042 0062 003f 0061
0000 00dc 0058 0061 0021 00cc 0020 0095
0058 00a8 006e 0095 0093 000f 0188 0061
006e 00c7 0058 0061 0095 00cc 0095 007a
002c 00c4 006e 0090 0057 0013 0158 0059
002c 00a5 006e 005e 0057 00c8 0058 0090
0000 00ad 006e 0090 0022 0010 0129 00  <- FORMATION
                                     54
1402 0100 0001 <- TACTICS

| F.C. Barcelona (DBC not working)

436f 7079 7269 6768 7420 2863 2931 3939
3620 4469 6e61 6d69 6320 4d75 6c74 696d
6564 6961 <- CDM
          ac07 <- TUNKNOWN00
               0d02 <- CONSTANT
                    0000 <- CONSTANT
                         0e00 274f 224f
4123 0013 0204 0d0e 0f00 <- NAME
                         0800 2200 0c11
412f 0e14 <- STADIUM
          16 <- COUNTRY
            29 <- TUNKNOWN01
               1500 279b 1503 0e0d 4122
0d14 0341 2300 1302 040d 0e0f 00 <- FULLNAME
                                8c a701
00 <- CAPACITY
  00 0000 00 <- STANDING
            48 00 <- WIDTH
                 6b 00 <- LENGTH
                      6b 07 <- FOUNDED
                           a5 07 <- BUILT
                                40 9601
00 <- MEMBERS
  1a 002b 0e12 0411 412d 0d14 8c12 412f
9b90 041b 4122 0d04 0c04 0f15 04 <- PRESIDENT
                                92 3000
00 <- BUDGET
  e8 0300 00 <- BUDGETPRO
            08 002f 2e41 3528 242f 24 <- SPONSOR
                                     05
002a 2031 3120 <- SUPPLIER
               1c00 <- RESERVE
                    ffff 0306 0002 0003
0001 0001 0001 0001 0004 0003 0002 0042
d207 4304 9e01 1a10 4c09 560a 0e13 0303
0202 0102 0101 0101 0101 0102 0203 0202
0202 0202 0202 0202 0202 0202 0202 0202
0202 0202 0202 0202 0000 0000 1404 5c17
0801 0904 0905 0100 0502 0000 0000 0000
0000 0000 0000 0000 0000 0000 0000 0000
0000 0000 0000 <- STATS
               0000 4200 2a00 4200 0000
5800 0000 5800 0000 7500 fa00 5100 2300
9300 9f00 a500 0000 0000 b500 6400 2200
2b00 7a00 3000 1000 1e00 dc00 8a00 4000
5800 aa00 5d00 0000 2900 a000 6e00 1e00
5800 7600 5d00 0000 0000 f500 4e00 5100
2b00 c100 1d00 4e00 6e00 ef00 5700 7500
9c00 0001 7c00 2200 6000 1b01 6500 5b00
8300 e200 8c00 6a00 2600 d400 6a00 8c00
5f00 2a01 5800 4800 2e00 f500 6300 6100
5d00 fd00 5800 3e00 0000 ff00 5f00 7500
1400 0101 2d00 <- FORMATION
               4639 0001 0100 01 <- TACTICS

| Sel. Europa (no bug)

436f 7079 7269
6768 7420 2863 2931 3939 3620 4469 6e61
6d69 6320 4d75 6c74 696d 6564 6961 4001
0d02
0000 0b00 3204 0d4f 4124 1413 0e11
000b 002d 0041 330e 0c00 1304 0500 3d00
1000 3204 0d04 0202 0892 0f41 2414 130e
1100 b587 0000 0000 0000 4400 6b00 8c07
0000 cc5b 0000 1500 200d 070e 0f12 0e41
320e 0d00 0f12 4132 0e0d 000f 1274 0b00
00f4 0100 0007 0031 282a 2e2d 282f 0600
2025 2825 2032 ffff ffff 020b 0005 0009
0011 0006 0009 0003 0007 000c 000e 002c
ae05 1202 7201 e807 2a08 ad05 0001 0a07
090d 1213 1212 1313 1414 1414 1213 1313
1515 1515 1212 1212 1011 0e10 1012 1012
0e10 0e0c 0b0d 0c0e 0000 0000 0701 0104
0000 0501 0100 0000 0100 0000 0000 0000
0000 0000 0000 0000 0000 0000 0000 0000
0000 0000 0000 0000 4200 2a00 4200 0000
5800 0000 5800 0000 7e00 2f01 4600 2600
9500 b200 9e00 0000 0000 3401 4500 2800
1700 b800 1800 0000 2500 b600 7c00 1d00
3d00 6800 3600 1600 1d00 d400 6a00 4300
4000 cf00 4500 0000 3700 9c00 6d00 1d00
6a00 6800 7f00 4100 0000 fa00 6500 5d00
1900 1101 3700 5b00 3800 e000 6900 6e00
6900 1101 6c00 6a00 2c00 d400 6e00 7800
4500 0f01 5100 2200 2400 cf00 6f00 4200
6800 b000 6400 3800 7600 fc00 4e00 5e00
9700 e600 9700 462d 0000 0000 00 … (staff: 02 2402 …)

| Sel. Europa (extension 1 with bug)

436f 7079 7269
6768 7420 2863 2931 3939 3620 4469 6e61
6d69 6320 4d75 6c74 696d 6564 6961 4001
0302
0000 0b00 3204 0d4f 4124 1413 0e11
000b 002d 0041 330e 0c00 1304 0500 3d   <- missing 2 zero bytes
1000 3204 0d04 0202 0892 0f41 2414 130e
1100 b587 0000 0000 0000 4400 6b00 8c07
     cc5b 0000 1500 200d 070e 0f12 0e41 <- missing 4 zero bytes
320e 0d00 0f12 4132 0e0d 000f 1274 0b00
00f4 0100 0007 0031 282a 2e2d 282f 0600
2025 2825 2032 ffff ffff 020b 0005 0009
0011 0006 0009 0003 0007 000c 000e 002c
ae05 1202 7201 e807 2a08 ad05 0001 0a07
090d 1213 1212 1313 1414 1414 1213 1313
1515 1515 1212 1212 1011 0e10 1012 1012
0e10 0e0c 0b0d 0c0e 0000 0000 0701 0104
0000 0501 0100 0000 0100 0000 0000 0000
0000 0000 0000 0000 0000 0000 0000 0000
0000 0000 0000 0000 4200 2a00 4200 0000
5800 0000 5800 0000 7e00 2f01 4600 2600
9500 b200 9e00 0000 0000 3401 4500 2800
1700 b800 1800 0000 2500 b600 7c00 1d00
3d00 6800 3600 1600 1d00 d400 6a00 4300
4000 cf00 4500 0000 3700 9c00 6d00 1d00
6a00 6800 7f00 4100 0000 fa00 6500 5d00
1900 1101 3700 5b00 3800 e000 6900 6e00
6900 1101 6c00 6a00 2c00 d400 6e00 7800
4500 0f01 5100 2200 2400 cf00 6f00 4200
6800 b000 6400 3800 7600 fc00 4e00 5e00
9700 e600 9700 462d 0000 0000 00 … (staff: 02 2402 …)

-}


{-

| S.S. de los Reyes

436f 7079 7269 6768 7420 2863 2931 3939  Copyright (c)199
3620 4469 6e61 6d69 6320 4d75 6c74 696d  6 Dinamic Multim
6564 6961|fa01 0d02 0000 1000 324f 324f  edia
0504 410d 0e12 4133 0418 0412 0c00 2504
0904 1200 4123 0e18 000d 1628 1800 324f
3204 0300 1215 0880 0f41 0504 410d 0e12
4133 0418 0412 1027 0000 0000 0000 4600
6900 b307 0000 1027 0000 0d00 200f 0604
0d41 3708 0d0d 040f 0000 0000 0000 0000
000c 0031 0015 130e 0208 0f00 050e 1309
0031 130e 1704 0405 0e13 e78c e78c 0000
0000 0000 0000 0000 0000 0000 0011 0003
0002 0018 d402 f700 a500 d303 b604 9302
0002 0a05 0104 0806 0405 0303 0502 0201
0101 0101 0101 0101 0101 0101 0101 0101
0101 0101 0101 0102 0000 0000 0000 0000
0100 0000 0000 0000 0000 0000 0000 0000
0000 0000 0000 0000 0000 0000 0000 0000
0000 0000 0000 0000 0000 0000 4200 2a00
4200 0000 5800 0000 5800 0000 3f00 9200
6c00 2800 7200 7200 7a00 0000 0000 1201
4f00 2c00 1800 b700 1200 0000 2b00 cd00
6d00 4c00 4400 a000 3d00 0100 1500 a700
6e00 2a00 4200 7100 3b00 2900 2f00 d500
6900 4b00 7100 9f00 7e00 4200 5100 f000
7200 6600 a500 f800 a300 4b00 0000 f100
7800 6100 0a00 f600 0b00 6a00 2c00 d400
6e00 8a00 5800 1a01 5800 6200 2c00 dc00
8400 6200 5600 eb00 5b00 0000 6d00 fc00
5800 2b00 a400 b600 a500 3232 0201 0101
01 … (staff: 02 8105 …)

* bytesP cdm
436f 7079 7269 6768 7420 2863 2931 3939
3620 4469 6e61 6d69 6320 4d75 6c74 696d
6564 6961

* tunknown00P
fa01

* bytesP [0x0d, 0x02] -- Constant
0d02

* byteP 0x00          -- Constant
00

* byteP 0x00          -- Database
00

* nameP
n:  1000 (16)
bs: 324f 324f 0504 410d 0e12 4133 0418 0412
rs: …
S.S.de los Reyes

* stadiumP
n:  0c00 (12)
bs: 2504 0904 1200 4123 0e18 000d
rs: …
Dehesa Boyal

* countryP
16

* tunknown01P
28

* fullnameP
n:  1800 (24)
bs: …
rs: …
S.Sebasti.n de los Reyes

* capacityP
1027 0000 (10.000)

* standingP
0000 0000 (0)

* widthP
4600 (070)

* lengthP
6900 (105)

* foundedP
b307 (1971)

| DB BEGIN

* builtP
0000

* membersP
1027 0000 (10.000)

* presidentP
n:  0d00 (13)
bs: …
rs: …
Angel Villena

* budgetP
00 0000 00

* budgetproP
00 0000 00

* sponsorP
n:  0c 00 (12)
bs: …
rs: …
Patrocinador

* supplierP
n:  09 00 (09)
bs: 31 130e 1704 0405 0e13
rs: 50 726f 7665 6564 6f72
Proveedor

* reserveP (really?)
e78c (36.071)

--------------------------------------------------------------------------------

* statsP

e78c 0000 0000 0000 0000 0000 0000 0000
0011 0003 0002 0018 d402 f700 a500 d303
b604 9302 0002 0a05 0104 0806 0405 0303
0502 0201 0101 0101 0101 0101 0101 0101
0101 0101 0101 0101 0101 0102 0000 0000
0000 0000 0100 0000 0000 0000 0000 0000
0000 0000 0000 0000 0000 0000 0000 0000
0000 0000 0000 0000 0000 0000 0000

--------------------------------------------------------------------------------

| DB END

* formationP

0000 4200 2a00 4200 0000 5800 0000 5800
0000 3f00 9200 6c00 2800 7200 7200 7a00
0000 0000 1201 4f00 2c00 1800 b700 1200
0000 2b00 cd00 6d00 4c00 4400 a000 3d00
0100 1500 a700 6e00 2a00 4200 7100 3b00
2900 2f00 d500 6900 4b00 7100 9f00 7e00
4200 5100 f000 7200 6600 a500 f800 a300
4b00 0000 f100 7800 6100 0a00 f600 0b00
6a00 2c00 d400 6e00 8a00 5800 1a01 5800
6200 2c00 dc00 8400 6200 5600 eb00 5b00
0000 6d00 fc00 5800 2b00 a400 b600 a500

--------------------------------------------------------------------------------

* tacticsP
3232 0201 0101 01

** possessionP
32 (50%)

** counterP
32 (50%)

** playstyleP
02 (Mixed)

** tacklingP
01 (Medium)

** coverageP
01 (ManToMan)

** clearanceP
01 (Long)

** preasureP
01 (Midfield)

--------------------------------------------------------------------------------

* staffP False
02 8105 1000 2200 130d 0e12 4122 000f …

-}

--------------------------------------------------------------------------------

-- HELPERS

tunknown00P
  :: Parser Word16
tunknown00P =
  -- ac07
  hextetP

nameP
  :: Parser String
nameP =
  -- n:  0e00
  -- bs: 274f 224f 4123 0013 0204 0d0e 0f00
  -- rs: 462e 432e 2042 6172 6365 6c6f 6e61
  stringP

stadiumP
  :: Parser String
stadiumP =
  -- n:  0800
  -- bs: 2200 0c11 412f 0e14
  -- rs: 4361 6d70 204e 6f75
  stringP

countryP
  :: Parser PKF.Country
countryP =
  -- 16
  PKF.countryP

tunknown01P
  :: Parser Word8
tunknown01P =
  -- 29
  octetP

fullnameP
  :: Parser String
fullnameP =
  -- n:  1500
  -- bs: 279b 1503 0e0d 4122 0d14 0341 2300 1302 040d 0e0f 00
  -- rs: 46fa 7462 6f6c 2043 6c75 6220 4261 7263 656c 6f6e 61
  stringP

capacityP
  :: Parser Word32
capacityP =
  -- 8ca7 0100
  quadletP

standingP
  :: Parser Word32
standingP =
  -- 0000 0000
  quadletP

widthP
  :: Parser Word16
widthP =
  -- 4800
  hextetP

lengthP
  :: Parser Word16
lengthP =
  -- 6b00
  hextetP

foundedP
  :: Parser Word16
foundedP =
  -- 6b07
  hextetP

--------------------------------------------------------------------------------

builtP
  :: Parser Word16
builtP =
  -- a507
  hextetP

membersP
  :: Parser Word32
membersP =
  -- 4096 0100
  quadletP

presidentP
  :: Parser String
presidentP =
  -- n:  1a00
  -- bs: 2b0e 1204 1141 2d0d 148c 1241 2f9b 9004 1b41 220d 040c 040f 1504
  -- rs: 4a6f 7365 7020 4c6c 75ed 7320 4efa f165 7a20 436c 656d 656e 7465
  stringP

budgetP
  :: Parser Word32
budgetP =
  -- 9230 0000
  quadletP

budgetproP
  :: Parser Word32
budgetproP =
  -- e803 0000
  quadletP

sponsorP
  :: Parser String
sponsorP =
  -- n:  0800
  -- bs: 2f2e 4135 2824 2f24
  -- rs: 4e4f 2054 4945 4e45
  stringP

supplierP
  :: Parser String
supplierP =
  -- n:  0500
  -- bs: 2a20 3131 20
  -- rs: 4b41 5050 41
  stringP

reserveP
  :: Parser Word16
reserveP =
  -- 1c00
  hextetP

statsP
  :: Parser PKF.Stats
statsP =
  PKF.statsP

--------------------------------------------------------------------------------

formationP
  :: Parser PKF.Formation
formationP =
  PKF.formationP

tacticsP
  :: Parser PKF.Tactics
tacticsP =
  PKF.tacticsP

staffP
  :: Bool
  -> Parser [PKF.Staff]
staffP cond =
  many aux
  where
    aux =
      PKF.staffP cond

--------------------------------------------------------------------------------

termP
  :: Parser Word8
termP =
  -- 00
  byteP 0x00
