--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCF.V6.PKF.Parse.Stats
  ( statsP
  )
where

--------------------------------------------------------------------------------

import           Data.Word
  ( Word16
  , Word8
  )

import qualified Data.PCF.V6.PKF.Types.Stats as PKF
import           Data.PCx.Byte.Common
  ( b2n
  , cof
  )
import           Data.PCx.Byte.Parser
  ( Parser
  , chunkP
  , hextetP
  , octetP
  )

--------------------------------------------------------------------------------

statsP
  :: Parser PKF.Stats
statsP =
  -- ffff 0306 0002 0003 0001 0001 0001 0001
  -- 0004 0003 0002 0042 d207 4304 9e01 1a10
  -- 4c09 560a 0e13 0303 0202 0102 0101 0101
  -- 0101 0102 0203 0202 0202 0202 0202 0202
  -- 0202 0202 0202 0202 0202 0202 0202 0202
  -- 0000 0000 1404 5c17 0801 0904 0905 0100
  -- 0401 0000 0000 0000 0000 0000 0000 0000
  -- 0000 0000 0000 0000 0000 0000 0000
  aux
  <$> unknown00P
  <*> unknown01P
  <*> lastdecadeP
  <*> seasonsP
  <*> matchesP
  <*> winP
  <*> drawP
  <*> gfP
  <*> gaP
  <*> pointsP
  <*> championP
  <*> runnersupP
  <*> lastseasonP
  <*> uefaP
  <*> cupP
  <*> championsleagueP
  <*> cupwinnerscupP
  <*> supercupP
  <*> intercontinentalP
  <*> eufasupercupP
  <*> zeropadP
  where
    aux a b c d e f g h i j k l m n o p q r s t _ =
      PKF.stats a b c d e f g h i j k l m n o p q r s t

--------------------------------------------------------------------------------

-- HELPERS

unknown00P
  :: Parser Word16
unknown00P =
  -- ffff
  hextetP

unknown01P
  :: Parser Word8
unknown01P =
  -- 03
  octetP

lastdecadeP
  :: Parser PKF.LastDecade
lastdecadeP =
  -- 0600 0200 0300 0100 0100 0100 0100 0400 0300 0200
  aux <$> chunkP len
  where
    aux = map b2n . cof 2
    len = 020 :: Word8

seasonsP
  :: Parser Word8
seasonsP =
  -- 42
  octetP

matchesP
  :: Parser Word16
matchesP =
  -- d207
  hextetP

winP
  :: Parser Word16
winP =
  -- 4304
  hextetP

drawP
  :: Parser Word16
drawP =
  -- 9e01
  hextetP

gfP
  :: Parser Word16
gfP =
  -- 1a10
  hextetP

gaP
  :: Parser Word16
gaP =
  -- 4c09
  hextetP

pointsP
  :: Parser Word16
pointsP =
  -- 560a
  hextetP

championP
  :: Parser Word8
championP =
  -- 0e
  octetP

runnersupP
  :: Parser Word8
runnersupP =
  -- 13
  octetP

lastseasonP
  :: Parser PKF.LastSeason
lastseasonP =
  -- 0303 0202 0102 0101 0101 0101 0102 0203
  -- 0202 0202 0202 0202 0202 0202 0202 0202
  -- 0202 0202 0202 0202 0202 0000 0000
  aux <$> chunkP len
  where
    aux = map b2n . cof 1 . filter ((<) 0)
    len = 046 :: Word8

uefaP
  :: Parser PKF.Competition
uefaP =
  -- 14 04
  (,) <$> octetP <*> octetP

cupP
  :: Parser PKF.Competition
cupP =
  -- 5c 17
  (,) <$> octetP <*> octetP

championsleagueP
  :: Parser PKF.Competition
championsleagueP =
  -- 08 01
  (,) <$> octetP <*> octetP

cupwinnerscupP
  :: Parser PKF.Competition
cupwinnerscupP =
  -- 09 04
  (,) <$> octetP <*> octetP

supercupP
  :: Parser PKF.Competition
supercupP =
  -- 09 05
  (,) <$> octetP <*> octetP

intercontinentalP
  :: Parser PKF.Competition
intercontinentalP =
  -- 01 00
  (,) <$> octetP <*> octetP

eufasupercupP
  :: Parser PKF.Competition
eufasupercupP =
  -- 04 01
  (,) <$> octetP <*> octetP

zeropadP
  :: Parser ()
zeropadP =
  -- 0000 0000 0000 0000 0000 0000 0000 0000
  -- 0000 0000 0000 0000 0000 0000
  pure () <$> chunkP len
  where
    len = 28 :: Word8
