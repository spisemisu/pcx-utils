--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCF.V6.PKF.Parse.TextLists
  ( textlistsP
  )
where

--------------------------------------------------------------------------------

import qualified Data.PCF.V6.PKF.Parse.TextList  as PKF
import qualified Data.PCF.V6.PKF.Types.TextList  as PKF
import qualified Data.PCF.V6.PKF.Types.TextLists as PKF
import           Data.PCx.Byte.Common
  ( b2h
  )
import           Data.PCx.Byte.Parser
  ( Parser
  , chunkP
  )
import qualified Data.PCx.Vx.PKF.Types.Pointer   as PKF.Pointer
import qualified Data.PCx.Vx.PKF.Types.Pointers  as PKF

--------------------------------------------------------------------------------

textlistsP
  :: PKF.Pointers
  -> Parser PKF.TextLists
textlistsP (PKF.Pointers ps) =
  PKF.textlists <$> aux 0 pts
  where
    hex =
      concatMap b2h
    pts =
      map f ps
      where
        f p =
          ( hex $ PKF.Pointer.uid   p
          ,       PKF.Pointer.start p
          ,       PKF.Pointer.size  p
          )
    aux _ [          ] = pure []
    aux a ((h,o,s):xs) =
      (:)
      <$> textlistP i h
      <*> aux       j xs
      where
        i = o - a
        j = o + s

--------------------------------------------------------------------------------

-- HELPERS

textlistP
  :: Word
  -> String
  -> Parser PKF.TextList
textlistP o h =
  aux
  <$> chunkP        o -- Skip offset
  <*> PKF.textlistP h
  where
    aux _ tl =
      tl
