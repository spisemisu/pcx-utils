--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCF.V6.PKF.Parse.Staff
  ( staffP
  )
where

--------------------------------------------------------------------------------

import qualified Data.PCF.V6.PKF.Parse.Manager as PKF
import qualified Data.PCF.V6.PKF.Parse.Player  as PKF
import qualified Data.PCF.V6.PKF.Types.Staff   as PKF
import           Data.PCx.Byte.Parser
  ( Parser
  , byteP
  , (<|>)
  )

--------------------------------------------------------------------------------

staffP
  :: Bool
  -> Parser PKF.Staff
staffP cond =
  playerP cond <|> managerP cond

playerP
  :: Bool
  -> Parser PKF.Staff
playerP cond =
  aux
  <$> byteP 0x01
  <*> PKF.playerP cond
  where
    aux _ p = PKF.player p

managerP
  :: Bool
  -> Parser PKF.Staff
managerP cond =
  aux
  <$> byteP 0x02
  <*> PKF.managerP cond
  where
    aux _ m = PKF.manager m
