--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCF.V6.PKF.Parse.Teams
  ( teamsP
  )
where

--------------------------------------------------------------------------------

import           Data.Word
  ( Word16
  )

import qualified Data.PCF.V6.PKF.Parse.Team     as PKF
import qualified Data.PCF.V6.PKF.Types.Team     as PKF
import qualified Data.PCF.V6.PKF.Types.Teams    as PKF
import           Data.PCx.Byte.Parser
  ( Parser
  , chunkP
  , getP
  , many
  )
import qualified Data.PCx.Vx.PKF.Identifier     as PKF.Identifier
import qualified Data.PCx.Vx.PKF.Types.Pointer  as PKF.Pointer
import qualified Data.PCx.Vx.PKF.Types.Pointers as PKF


--------------------------------------------------------------------------------

teamsP
  :: PKF.Pointers
  -> Parser PKF.Teams
teamsP (PKF.Pointers ps) =
  -- Teams <$> aux 0 pts
  (\ ts _ -> PKF.teams ts) <$> aux 0 pts <*> many getP
  where
    pts =
      map f ps
      where
        f p =
          ( PKF.Identifier.uid $ PKF.Pointer.uid   p
          ,                      PKF.Pointer.start p
          ,                      PKF.Pointer.size  p
          )
    aux _ [          ] = pure []
    {- DEBUG: Special case with buggy manager/teams
    aux a ((0061,o,s):xs) = skipP a o t aux xs
    aux a ((8800,o,s):xs) = skipP a o t aux xs -- ext 1
    aux a ((8801,o,s):xs) = skipP a o t aux xs -- ext 1
    -}
    aux a ((u,o,s):xs) =
      (:)
      <$> teamP i u
      <*> aux   j xs
      where
        i = o - a
        j = o + s

--------------------------------------------------------------------------------

-- HELPERS

teamP
  :: Word
  -> Word16
  -> Parser PKF.Team
teamP o u =
  aux
  <$> chunkP    o -- Skip offset
  <*> PKF.teamP u
  where
    aux _ t =
      t

--------------------------------------------------------------------------------

-- DEBUG

{- DEBUG: To be used to skip buggy manager/teams

skipP
  :: Word
  -> Word
  -> Word
  -> (Word -> [(Word16,Word,Word)] -> Parser [PKF.Team])
  -> [(Word16,Word,Word)]
  -> Parser [PKF.Team]
skipP a o t f xs =
  chunkP j >>= \ _ ->
  chunkP t >>= \ _ ->
  f b xs
  where
    j = o-a
    b = o+t

-}
