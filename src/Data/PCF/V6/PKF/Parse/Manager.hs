--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCF.V6.PKF.Parse.Manager
  ( managerP
  )
where

--------------------------------------------------------------------------------

import           Data.Word
  ( Word16
  , Word8
  )

import qualified Data.PCF.V6.PKF.Types.Manager as PKF
import           Data.PCx.Byte.Parser
  ( Parser
  , byteP
  , hextetP
  , octetP
  , stringP
  , (<|>)
  )

--------------------------------------------------------------------------------

managerP
  :: Bool
  -> Parser PKF.Manager
managerP cond =
  if cond
  then
    PKF.manager
    <$> midP
    <*> nameP
  else
    buggymanagerP <|> othermanagersP

buggymanagerP
  :: Parser PKF.Manager
buggymanagerP =
  aux
  <$> midP
  <*> nameP
  <*> fullnameP
  <*> mdunknown00P
  <*> tacticsP
  <*> honoursP
  <*> miscellaneousP
  <*> lastseasonP
  <*> managercareerP
  <*> byteP 0x00
  <*> playercareerP
  -- <*> statementsP -- missing
  where
    aux a b c d e f g h i j k =
      PKF.managerdb a b c d e f g h i j k "x"

othermanagersP
  :: Parser PKF.Manager
othermanagersP =
  PKF.managerdb
  <$> midP
  <*> nameP
  <*> fullnameP
  <*> mdunknown00P
  <*> tacticsP
  <*> honoursP
  <*> miscellaneousP
  <*> lastseasonP
  <*> managercareerP
  <*> mdunknown01P
  <*> playercareerP
  <*> statementsP

--------------------------------------------------------------------------------

-- DEBUG

{-

Template

* midP

* nameP

* fullnameP

* mdunknown00P

* tacticsP

* honoursP

* miscellaneousP

* lastseasonP

* managercareerP

* mdunknown01P

* playercareerP

* statementsP

-}

{-

Escribano

02
  35 0210 0026
0013 028c 0041 2412 0213 0803 000f 0e1d
002b 0e12 8841 200f 150e 0f08 0e41 2600
1302 0800 4124 1202 1308 0300 0f0e 0100
1901 0019 0100 1901 0019 0100 1900 0000
0100 19


* midP
35 02 (565)

* nameP
n:  10 00 (16)
bs: 26 0013 028c 0041 2412 0213 0803 000f 0e
rs: …
Garc.a Escribano

* fullnameP
n:  1d 00 (29)
bs: 2b 0e12 8841 200f 150e 0f08 0e41 2600 1302 0800 4124 1202 1308 0300 0f0e
rs: …
Jos. Antonio Garcia Escribano

* mdunknown00P
n:  0100 (29)
bs: 19
rs: 78
x

* tacticsP
n:  0100 (29)
bs: 19
rs: 78
x

* honoursP
n:  0100 (29)
bs: 19
rs: 78
x

* miscellaneousP
n:  0100 (29)
bs: 19
rs: 78
x

* lastseasonP
n:  0100 (29)
bs: 19
rs: 78
x

* managercareerP
00 00

* mdunknown01P
00

* playercareerP
n:  0100 (29)
bs: 19
rs: 78
x

* statementsP

MISSING bytes !!!

-}

--------------------------------------------------------------------------------

-- HELPERS

midP
  :: Parser Word16
midP =
  -- 2d04
  hextetP

nameP
  :: Parser String
nameP =
  -- n:  0800
  -- bs: 3700 0f41 2600 000d
  -- rs: 5661 6e20 4761 616c
  stringP

fullnameP
  :: Parser String
fullnameP =
  -- n:  1e00
  -- bs: 200d 0e18 1208 1412 4131 0014 0d14 1241 2c00 1308 0041 3700 0f41 2600 000d
  -- rs: 416c 6f79 7369 7573 2050 6175 6c75 7320 4d61 7269 6120 5661 6e20 4761 616c
  stringP

mdunknown00P
  :: Parser String
mdunknown00P =
  -- n:   0100
  -- bs:  19
  -- rs:  78 (x)
  stringP

tacticsP
  :: Parser String
tacticsP =
  -- n:  7106
  -- bs: ...
  -- rs: ...
  stringP

honoursP
  :: Parser String
honoursP =
  -- n: 4d02
  -- bs: ...
  -- rs: ...
  stringP

miscellaneousP
  :: Parser String
miscellaneousP =
  -- n: bb16
  -- bs: ...
  -- rs: ...
  stringP

lastseasonP
  :: Parser String
lastseasonP =
  -- n: 4103
  -- bs: ...
  -- rs: ...
  stringP

managercareerP
  :: Parser String
managercareerP =
  -- n: 3e01
  -- bs: ...
  -- rs: ...
  stringP

mdunknown01P
  :: Parser Word8
mdunknown01P =
  octetP

playercareerP
  :: Parser String
playercareerP =
  -- n: 7900
  -- bs: ...
  -- rs: ...
  stringP

statementsP
  :: Parser String
statementsP =
  -- n: 2a01
  -- bs: ...
  -- rs: ...
  stringP
