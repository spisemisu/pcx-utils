--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCF.V6.PKF.Parse.Player
  ( playerP
  )
where

--------------------------------------------------------------------------------

import           Data.Word
  ( Word16
  , Word8
  )

import qualified Data.PCF.V6.PKF.Parse.Country as PKF
import qualified Data.PCF.V6.PKF.Types.Country as PKF
import qualified Data.PCF.V6.PKF.Types.Player  as PKF
import           Data.PCx.Byte.Common
  ( b2n
  )
import           Data.PCx.Byte.Parser
  ( Parser
  , chunkP
  , failP
  , hextetP
  , octetP
  , stringP
  )

--------------------------------------------------------------------------------

playerP
  :: Bool
  -> Parser PKF.Player
playerP cond =
  if cond
  then
    PKF.player
    <$> pidP
    <*> numberP
    <*> nameP
    <*> fullnameP
    <*> indexP
    <*> statusP
    <*> rolesP
    <*> citizenshipP
    <*> skinP
    <*> hairP
    <*> positionP
    <*> birthdayP
    <*> heightP
    <*> weightP
    <*> paceP
    <*> staminaP
    <*> aggressionP
    <*> skillP
    <*> finishingP
    <*> dribblingP
    <*> passingP
    <*> shootingP
    <*> tacklingP
    <*> goalkeepingP
  else
    PKF.playerdb
    <$> pidP
    <*> numberP
    <*> nameP
    <*> fullnameP
    <*> indexP
    <*> statusP
    <*> rolesP
    <*> citizenshipP
    <*> skinP
    <*> hairP
    <*> positionP
    <*> birthdayP
    <*> heightP
    <*> weightP
    -- DB begin
    <*> countryP
    <*> birthplaceP
    <*> fromteamP
    <*> nationalteamP
    <*> pdunknown00P
    <*> featuresP
    <*> honoursP
    <*> intcapsP
    <*> miscellaneousP
    <*> lastseasonP
    <*> careerP
    -- DB end
    <*> paceP
    <*> staminaP
    <*> aggressionP
    <*> skillP
    <*> finishingP
    <*> dribblingP
    <*> passingP
    <*> shootingP
    <*> tacklingP
    <*> goalkeepingP

--------------------------------------------------------------------------------

-- DEBUG

{-

Template

* pidP
* numberP
* nameP
* fullnameP
* indexP
* statusP
* rolesP
* citizenshipP
* skinP
* hairP
* positionP
* birthdayP
* heightP
* weightP
| DB begin
* countryP
* birthplaceP
* fromteamP
* nationalteamP
* pdunknown00P
* featuresP
* honoursP
* intcapsP
* miscellaneousP
* lastseasonP
* careerP
| DB end
* paceP
* staminaP
* aggressionP
* skillP
* finishingP
* dribblingP
* passingP
* shootingP
* tacklingP
* goalkeepingP

-}

{-

CACHITO (S.S. de los Reyes)

* pidP

98 50

* numberP
01

* nameP
0700
2200 0209 0815 0e
Cachito

* fullnameP
CACHITO
07 00
22 2022 2928 352e

* indexP
09

* statusP
00

* rolesP
0900 0000 0000

* citizenshipP
16

* skinP
01

* hairP
03

* positionP
03

* birthdayP
00 00 6c07 (00-00-1900)

* heightP
8c (140 cm)

* weightP
01 (001 kg)

| DB begin

* countryP
16

* birthplaceP
n:  01 00
bs: 4c
rs: "-"

* fromteamP
n:  01 00
bs: 4c
rs: "-"

* nationalteamP
n:  0100
bs: 51
rs: "0"

* pdunknown00P
n:  0100
bs: 19
rs: "x"

* featuresP
n:  0100
bs: 19
rs: "x"

* honoursP
n:  0100
bs: 19
rs: "x"

* intcapsP
n:  0100
bs: 19
rs: "x"

* miscellaneousP
n:  0100
bs: 19
rs: "x"

* lastseasonP
n:  0100
bs: 19
rs: "x"

* careerP
n:  1000 (16)
bs: 2f25 4d2f 254d 2f25 4d2f 254d 2f25 6c6b
rs: ND,ND,ND,ND,ND..

| DB end

* paceP
2b (43)

* staminaP
3b (59)

* aggressionP
35 (53)

* skillP
33 (51)

* finishingP
3a (58)

* dribblingP
24 (36)

* passingP
2c (44)

* shootingP
23 (35)

* tacklingP
1b (27)

* goalkeepingP
05 (05)

-}


{-

CASQUERO (Toledo)

- pidP
2f 02

- numberP
16

- nameP
08 00
Casquero

- fullnameP
21 00 (33)
Francisco Javier CASQUERO Paredes

- indexP
06

- statusP
01

- rolesP
0d 0000 0000 00

- citizenshipP
16 (22)

- skinP
01

- hairP
03

- positionP
02 (MED)

- birthdayP
13 08 b807 (19-08-1976)

- heightP
ae (174 cm)

- weightP
45 (069 kg)

| DB BEGIN

- countryP
16 (22)

- birthplaceP
1d00 (29)
Talavera de la Reina (Toledo)

- fromteamP
1500 (21)
Cultural Leonesa (97)

- nationalteamP
n:  0100
bs: 51
rs: "0"

* pdunknown00P
n:  0100
bs: 4b
rs: "*"

- featuresP
n:  3100 (49)
bs: …
rs: …
* Media punta r.pido y con buena visi.n de juego.

- honoursP
n:  0100
bs: …
rs: …
x

- intcapsP
n:  0100
bs: …
rs: …
x

- miscellaneousP
n:  3703 (823)
bs: …
rs: …
* Formado en la cantera del Real Madrid, Casquero se incorpor. al Toledo en 1993.                                             ..* Debut. en Segunda Divisi.n el 5-2-95 en Ipur.a frente al Eibar (2-2). Recibi. una tarjeta amarilla.                                                   ..* Marc. su primer gol en la categor.a de plata el 17-6-95 en el Salto del Caballo frente al Legan.s (3-2).                                              ..* En la 95/96 intervino en 19 partidos, ocho como titular. Una lesi.n en la jornada 28. le apart. del equipo hasta la conclusi.n del campeonato. Recibi. tres tarjetas amarillas.                                           ..* El Valencia ten.a una opci.n de compra por .l.                            ..* La pasada temporada fue cedido a la Cultural Leonesa antes de la 18. jornada de Liga.

- lastseasonP
n:  3301 (307)
bs: …
rs: …
* Comenz. la Liga en el Toledo, equipo con el que intervino en ocho partidos de Liga, haci.ndolo como titular en una sola ocasi.n. Despu.s se march. cedido a la Cultural Leonesa, conjunto con el que concluy. la Liga en la octava posici.n del Grupo II de Segunda B. Particip. en 17 encuentros y marc. un gol.

- careerP
n:  8f00 (143)
bs: …
rs: …
92/93,R.Madrid (c.inf),-,-,-..93/94,Toledo,2,0,0..94/95,Toledo,2,11,1..95/96,Toledo,2,19,0..96/97,Toledo,2,8,0..96/97,Cultural Leonesa,B,17,1..

| DB END

- paceP
40 (64)

- staminaP
37 (55)

- aggressionP
34 (52)

- skillP
3d (61)

- finishingP
36 (54)

- dribblingP
35 (53)

- passingP
34 (52)

- shootingP
33 (51)

- tacklingP
36 (54)

- goalkeepingP
05 (05)

-}

--------------------------------------------------------------------------------

-- HELPERS

pidP
  :: Parser Word16
pidP =
  -- 0b0e
  hextetP

numberP
  :: Parser Word8
numberP =
  -- 0d
  octetP

nameP
  :: Parser String
nameP =
  -- n:  04 00
  -- bs: 2904 1211
  -- rs:
  stringP

fullnameP
  :: Parser String
fullnameP =
  -- n:  0900
  -- bs: 33 1414 0541 2924 3231
  -- rs:
  stringP

indexP
  :: Parser Word8
indexP =
  -- 01
  octetP

statusP
  :: Parser PKF.Status
statusP =
  -- 01
  octetP >>= aux
  where
    aux 0x00 = pure  $ PKF.Veteran
    aux 0x01 = pure  $ PKF.Signing
    aux 0x02 = pure  $ PKF.Academy
    aux 0x03 = pure  $ PKF.OnLeave
    aux rest = failP $ "Parser PKF.Status | Invalid value: " ++ show rest

rolesP
  :: Parser [PKF.Role]
rolesP =
  -- 0100 0000 0000
  aux <$> chunkP len
  where
    aux = map (rol . b2n . (:[])) . filter ((<) 0)
    len = 006 :: Word8
    rol :: Word8 -> PKF.Role
    rol 0x01 = PKF.GK
    rol 0x02 = PKF.RB  -- Right Back
    rol 0x03 = PKF.LB  -- Left Back
    rol 0x04 = PKF.SW  -- Sweeper
    rol 0x05 = PKF.LCB -- Left Center Back
    rol 0x06 = PKF.RCB -- Right Center Back
    rol 0x07 = PKF.RM  -- Right Midfielder
    rol 0x08 = PKF.RCM -- Right Centre Midfielder
    rol 0x09 = PKF.ST  -- Striker
    rol 0x0A = PKF.CAM -- Centre Attacking Midfielder
    rol 0x0B = PKF.LM  -- Left Midfielder
    rol 0x0C = PKF.RW  -- Right Wing
    rol 0x0D = PKF.CF  -- Centre Forward
    rol 0x0E = PKF.LW  -- Left Wing
    rol 0x0F = PKF.CDM -- Centre Defensive Midfielder
    rol 0x10 = PKF.LF  -- Left Forward
    rol 0x11 = PKF.RF  -- Right Forward
    rol 0x12 = PKF.LCM -- Left Centre Midfielder
    rol rest = error $ "Parser [PKF.Role] | Invalid value: " ++ show rest

citizenshipP
  :: Parser PKF.Country
citizenshipP =
  -- 1b
  PKF.countryP

skinP
  :: Parser PKF.Skin
skinP =
  -- 01
  octetP >>= aux
  where
    aux 0x01 = pure  $ PKF.Light
    aux 0x02 = pure  $ PKF.Dark
    aux 0x03 = pure  $ PKF.Medium
    aux rest = failP $ "Parser PKF.Skin | Invalid value: " ++ show rest

hairP
  :: Parser PKF.Hair
hairP =
  -- 01
  octetP >>= aux
  where
    aux 0x01 = pure  $ PKF.Blond
    aux 0x02 = pure  $ PKF.Bald
    aux 0x03 = pure  $ PKF.Black
    aux 0x04 = pure  $ PKF.Grey
    aux 0x05 = pure  $ PKF.Redhead
    aux 0x06 = pure  $ PKF.Brown
    aux rest = failP $ "Parser PKF.Hair | Invalid value: " ++ show rest

positionP
  :: Parser PKF.Position
positionP =
  -- 01
  octetP >>= aux
  where
    aux 0x00 = pure  $ PKF.KEP
    aux 0x01 = pure  $ PKF.DEF
    aux 0x02 = pure  $ PKF.MID
    aux 0x03 = pure  $ PKF.FOR
    aux rest = failP $ "Parser PKF.Position | Invalid value: " ++ show rest

birthdayP
  :: Parser (Word16, Word8, Word8)
birthdayP =
  -- 1f0a ad07
  aux <$> octetP <*> octetP <*> hextetP
  where
    aux d m y =
      (y,m,d)

heightP
  :: Parser Word8
heightP =
  -- c2
  octetP

weightP
  :: Parser Word8
weightP =
  -- 5d
  octetP

--------------------------------------------------------------------------------

-- DATABASE

countryP
  :: Parser Word8
countryP =
  -- 1b
  octetP

birthplaceP
  :: Parser String
birthplaceP =
  -- n:  1000
  -- bs: 2314 1212 140c 4149 290e 0d00 0f05 0048
  -- rs:
  stringP

fromteamP :: Parser String
fromteamP =
  -- n:  0e00
  -- bs: 330e 0500 4d41 292e 2d41 4958 5648
  -- rs:
  stringP

nationalteamP :: Parser String
nationalteamP =
  -- n:   0100
  -- bs:  51
  -- rs:  0
  stringP

pdunknown00P :: Parser String
pdunknown00P =
  -- n:   0100
  -- bs:  19
  -- rs:  78 (x)
  stringP

featuresP :: Parser String
featuresP =
  -- n:  0f05
  -- bs: ...
  stringP

honoursP
  :: Parser String
honoursP =
  -- n:  8200
  -- bs: ...
  stringP

intcapsP :: Parser String
intcapsP =
  -- n:  5b00
  -- bs: ...
  stringP

miscellaneousP
  :: Parser String
miscellaneousP =
  -- n:  4404
  -- bs: ...
  stringP

lastseasonP
  :: Parser String
lastseasonP =
  -- n:  0701
  -- bs: ...
  stringP

careerP
  :: Parser String
careerP =
  -- n:  4a01
  -- bs: ...
  stringP

--------------------------------------------------------------------------------

paceP
  :: Parser Word8
paceP =
  -- 4e
  octetP

staminaP
  :: Parser Word8
staminaP =
  -- 4f
  octetP

aggressionP
  :: Parser Word8
aggressionP =
  --  4d
  octetP

skillP
  :: Parser Word8
skillP =
  -- 4e
  octetP

finishingP
  :: Parser Word8
finishingP =
  -- 13
  octetP

dribblingP
  :: Parser Word8
dribblingP =
  -- 16
  octetP

passingP
  :: Parser Word8
passingP =
  -- 12
  octetP

shootingP
  :: Parser Word8
shootingP =
  -- 17
  octetP

tacklingP
  :: Parser Word8
tacklingP =
  -- 16
  octetP

goalkeepingP
  :: Parser Word8
goalkeepingP =
  -- 55
  octetP
