--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCF.V6.PKF.Parse.Formation
  ( formationP
  )
where

--------------------------------------------------------------------------------

import           Data.Word
  ( Word8
  )

import qualified Data.PCF.V6.PKF.Types.Formation as PKF
import           Data.PCx.Byte.Parser
  ( Parser
  , bytesP
  , hextetP
  , (<|>)
  )

--------------------------------------------------------------------------------

formationP
  :: Parser PKF.Formation
formationP =
  -- 0000 4200 2a00 4200 0000 5800 0000 5800
  -- 0000 7500 fa00 5100 2300 9300 9f00 a500
  -- 0000 0000 b500 6400 2200 2b00 7a00 3000
  -- 1000 1e00 dc00 8a00 4000 5800 aa00 5d00
  -- 0000 2900 a000 6e00 1e00 5800 7600 5d00
  -- 0000 0000 f500 4e00 5100 2b00 c100 1d00
  -- 4e00 6e00 ef00 5700 7500 9c00 0001 7c00
  -- 2200 6000 1b01 6500 5b00 8300 e200 8c00
  -- 6a00 2600 d400 6a00 8c00 5f00 2a01 5800
  -- 4800 2e00 f500 6300 6100 5d00 fd00 5800
  -- 3e00 0000 ff00 5f00 7500 1400 0101 2d00
  f343P <|> f352P <|> f433P <|> f442P <|> f532P  <|>
  f541P <|> f424P <|> f523P <|> f451P <|> f3331P <|>
  customP

--------------------------------------------------------------------------------

-- HELPERS

f343P
  :: Parser PKF.Formation
f343P =
  aux <$> bytesP PKF.f343
  where
    aux _ = PKF.F343

f352P
  :: Parser PKF.Formation
f352P =
  aux <$> bytesP PKF.f352
  where
    aux _ = PKF.F352

f433P
  :: Parser PKF.Formation
f433P =
  aux <$> bytesP PKF.f433
  where
    aux _ = PKF.F433

f442P
  :: Parser PKF.Formation
f442P =
  aux <$> bytesP PKF.f442
  where
    aux _ = PKF.F442

f532P
  :: Parser PKF.Formation
f532P =
  aux <$> bytesP PKF.f532
  where
    aux _ = PKF.F532

f541P
  :: Parser PKF.Formation
f541P =
  aux <$> bytesP PKF.f541
  where
    aux _ = PKF.F541

f424P
  :: Parser PKF.Formation
f424P =
  aux <$> bytesP PKF.f424
  where
    aux _ = PKF.F424

f523P
  :: Parser PKF.Formation
f523P =
  aux <$> bytesP PKF.f523
  where
    aux _ = PKF.F523

f451P
  :: Parser PKF.Formation
f451P =
  aux <$> bytesP PKF.f451
  where
    aux _ = PKF.F451

f3331P
  :: Parser PKF.Formation
f3331P =
  aux <$> bytesP PKF.f3331
  where
    aux _ = PKF.F3331

--------------------------------------------------------------------------------

customP
  :: Parser PKF.Formation
customP =
  PKF.custom . zip [1..]
  <$> positionsP

--------------------------------------------------------------------------------

positionsP
  :: Parser [PKF.Position]
positionsP =
  aux len
  where
    aux 0 = pure []
    aux n = (:) <$> positionP <*> aux (n-1)
    len = 11 :: Word8

positionP
  :: Parser PKF.Position
positionP =
  -- 0000 4200 2a00 4200 0000 5800 0000 5800
  aux
  <$> hextetP
  <*> hextetP
  <*> hextetP
  <*> hextetP
  <*> hextetP
  <*> hextetP
  <*> hextetP
  <*> hextetP
  where
    aux a1x a1y a2x a2y dx dy ox oy =
      PKF.position
        ( (a1x, a1y)
        , (a2x, a2y)
        )
        (dx, dy)
        (ox, oy)
