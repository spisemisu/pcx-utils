--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCF.V6.PKF.Parse.Tactics
  ( tacticsP
  )
where

--------------------------------------------------------------------------------

import           Data.Word
  ( Word8
  )

import qualified Data.PCF.V6.PKF.Types.Tactics as PKF
import           Data.PCx.Byte.Parser
  ( Parser
  , boolP
  , failP
  , octetP
  )

--------------------------------------------------------------------------------

tacticsP
  :: Parser PKF.Tactics
tacticsP =
  -- 4639 0001 0100 01
  PKF.tactics
  <$> possessionP
  <*> counterP
  <*> playstyleP
  <*> tacklingP
  <*> coverageP
  <*> clearanceP
  <*> preasureP

--------------------------------------------------------------------------------

-- HELPERS

possessionP
  :: Parser Word8
possessionP =
  -- 46
  octetP

counterP
  :: Parser Word8
counterP =
  -- 39
  octetP

playstyleP
  :: Parser PKF.PlayStyle
playstyleP =
  -- 00
  octetP >>= aux
  where
    aux 0x00 = pure  $ PKF.Attacking
    aux 0x01 = pure  $ PKF.Speculative
    aux 0x02 = pure  $ PKF.Mixed
    aux rest = failP $ "Parser PKF.PlayStyle | Invalid value: " ++ show rest

tacklingP
  :: Parser PKF.Tackling
tacklingP =
  -- 01
  octetP >>= aux
  where
    aux 0x00 = pure  $ PKF.Soft
    aux 0x01 = pure  $ PKF.Medium
    aux 0x02 = pure  $ PKF.Aggresive
    aux rest = failP $ "Parser PKF.Tackling | Invalid value: " ++ show rest

coverageP
  :: Parser PKF.Coverage
coverageP =
  -- 01
  aux <$> boolP
  where
    aux False = PKF.Zone
    aux True  = PKF.ManToMan

clearanceP
  :: Parser PKF.Clearance
clearanceP =
  -- 00
  aux <$> boolP
  where
    aux False = PKF.Short
    aux True  = PKF.Long

preasureP
  :: Parser PKF.Preasure
preasureP =
  -- 01
  octetP >>= aux
  where
    aux 0x00 = pure  $ PKF.Own
    aux 0x01 = pure  $ PKF.Midfield
    aux 0x02 = pure  $ PKF.Opponent
    aux rest = failP $ "Parser PKF.Preasure | Invalid value: " ++ show rest
