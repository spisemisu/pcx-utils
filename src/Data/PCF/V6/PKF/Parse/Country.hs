--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.PCF.V6.PKF.Parse.Country
  ( countryP
  )
where

--------------------------------------------------------------------------------

import qualified Data.PCF.V6.PKF.Types.Country as PKF
import           Data.PCx.Byte.Parser
  ( Parser
  , failP
  , octetP
  )

--------------------------------------------------------------------------------

countryP
  :: Parser PKF.Country
countryP =
  -- 1b
  octetP >>= aux
  where
    aux 0x01 = pure PKF.ALB
    aux 0x02 = pure PKF.DEU
    aux 0x03 = pure PKF.ARG
    aux 0x04 = pure PKF.AUS
    aux 0x05 = pure PKF.AUT
    aux 0x06 = pure PKF.AZE
    aux 0x07 = pure PKF.BLR
    aux 0x08 = pure PKF.BOL
    aux 0x09 = pure PKF.BIH
    aux 0x0A = pure PKF.BRA
    aux 0x0B = pure PKF.BGR
    aux 0x0C = pure PKF.BEL
    aux 0x0D = pure PKF.CMR
    aux 0x0E = pure PKF.CHL
    aux 0x0F = pure PKF.CYP
    aux 0x10 = pure PKF.COL
    aux 0x11 = pure PKF.HRV
    aux 0x12 = pure PKF.DNK
    aux 0x13 = pure PKF.GBR_SCT
    aux 0x14 = pure PKF.SVK
    aux 0x15 = pure PKF.SVN
    aux 0x16 = pure PKF.ESP
    aux 0x17 = pure PKF.FIN
    aux 0x18 = pure PKF.FRA
    aux 0x19 = pure PKF.GHA
    aux 0x1A = pure PKF.GRC
    aux 0x1B = pure PKF.NLD
    aux 0x1C = pure PKF.HND
    aux 0x1D = pure PKF.HUN
    aux 0x1E = pure PKF.GBR_ENG
    aux 0x1F = pure PKF.IRL
    aux 0x20 = pure PKF.GBR_NIR
    aux 0x21 = pure PKF.ISL
    aux 0x22 = pure PKF.FRO
    aux 0x23 = pure PKF.ISR
    aux 0x24 = pure PKF.ITA
    aux 0x25 = pure PKF.LTU
    aux 0x26 = pure PKF.LUX
    aux 0x27 = pure PKF.MKD
    aux 0x28 = pure PKF.MLT
    aux 0x29 = pure PKF.MAR
    aux 0x2A = pure PKF.MDA
    aux 0x2B = pure PKF.NGA
    aux 0x2C = pure PKF.NOR
    aux 0x2D = pure PKF.GBR_WLS
    aux 0x2E = pure PKF.POL
    aux 0x2F = pure PKF.PRT
    aux 0x30 = pure PKF.CZE
    aux 0x31 = pure PKF.ROU
    aux 0x32 = pure PKF.RUS
    aux 0x33 = pure PKF.SRB
    aux 0x34 = pure PKF.ZAF
    aux 0x35 = pure PKF.SWE
    aux 0x36 = pure PKF.CHE
    aux 0x37 = pure PKF.TUR
    aux 0x38 = pure PKF.UKR
    aux 0x39 = pure PKF.URY
    aux 0x3A = pure PKF.YUG
    aux 0x3B = pure PKF.PER
    aux 0x3C = pure PKF.CAN
    aux 0x3D = pure PKF.USA
    aux 0x3E = pure PKF.GEO
    aux 0x3F = pure PKF.CRI
    aux 0x40 = pure PKF.PRY
    aux 0x41 = pure PKF.JPN
    aux 0x42 = pure PKF.DZA
    aux 0x43 = pure PKF.TTO
    aux 0x44 = pure PKF.SEN
    aux 0x45 = pure PKF.SUR
    aux 0x46 = pure PKF.ZMB
    aux 0x47 = pure PKF.CPV
    aux 0x48 = pure PKF.VEN
    aux 0x49 = pure PKF.RHO
    aux 0x4A = pure PKF.SGP
    aux 0x4B = pure PKF.AND
    aux 0x4C = pure PKF.MOZ
    aux 0x4D = pure PKF.LIE
    aux 0x4E = pure PKF.LBR
    aux 0x4F = pure PKF.PAN
    aux 0x50 = pure PKF.ZAR
    aux 0x51 = pure PKF.TJK
    aux 0x52 = pure PKF.UZB
    aux 0x53 = pure PKF.MEX
    aux 0x54 = pure PKF.GIN
    aux 0x55 = pure PKF.AGO
    aux 0x56 = pure PKF.ZWE
    aux 0x57 = pure PKF.SLE
    aux 0x58 = pure PKF.GLP
    aux 0x59 = pure PKF.ECU
    aux 0x5A = pure PKF.EST
    aux 0x5B = pure PKF.GNB
    aux 0x5C = pure PKF.LBY
    aux 0x5D = pure PKF.EGY
    aux 0x5E = pure PKF.JAM
    aux 0x5F = pure PKF.NCL
    aux 0x60 = pure PKF.BMU
    aux 0x61 = pure PKF.NZL
    aux 0x62 = pure PKF.GUF
    aux 0x63 = pure PKF.VCT
    aux 0x64 = pure PKF.TCD
    aux 0x65 = pure PKF.TGO
    aux 0x66 = pure PKF.GIN_C
    aux 0x67 = pure PKF.TZA
    aux 0x68 = pure PKF.BFA
    aux 0x69 = pure PKF.GMB
    aux 0x6A = pure PKF.RWA
    aux 0x6B = pure PKF.KEN
    aux 0x6C = pure PKF.MRT
    aux 0x6D = pure PKF.MLI
    aux 0x6E = pure PKF.UGA
    aux 0x6F = pure PKF.COG
    aux 0x70 = pure PKF.LVA
    aux 0x71 = pure PKF.CIV
    aux 0x72 = pure PKF.ARM
    aux 0x73 = pure PKF.NIC
    aux 0x74 = pure PKF.ESP_CAT
    aux 0x75 = pure PKF.NER
    aux 0x76 = pure PKF.BRB
    aux 0x77 = pure PKF.IRN
    aux 0x78 = pure PKF.QAT
    aux 0x79 = pure PKF.TUN
    aux 0x7A = pure PKF.GAB
    aux 0x7B = pure PKF.PYF
    aux 0x7C = pure PKF.MUS
    aux 0x7D = pure PKF.MDG
    aux 0x7E = pure PKF.MTQ
    aux 0x7F = pure PKF.VNM
    {-
    aux ____ = pure PKF.XXX
    -}
    aux rest = failP $ "Parser PKF.Country | Invalid value: " ++ show rest
