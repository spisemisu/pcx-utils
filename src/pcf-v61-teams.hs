#!/usr/bin/env stack
{- stack
   --resolver lts-20.24
   --install-ghc
   script
   --
   --ghc-options -Werror
   --ghc-options -Wall
-}

--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import qualified Data.PCF.V6.PKF.Types.Team  as PKF.Team
import qualified Data.PCF.V6.PKF.Types.Teams as PKF
import           Data.PCx.Byte.Common
  ( pad
  )

--------------------------------------------------------------------------------

main
  :: IO ()

--------------------------------------------------------------------------------

main =
  readFile tms >>= pprint . teams
  where
    tms = "../pcx/pcf/v61/eq022022.tms"

--------------------------------------------------------------------------------

-- HELPERS

teams
  :: String
  -> PKF.Teams
teams =
  read

pprint
  :: PKF.Teams
  -> IO ()
pprint (PKF.Teams ts) =
  mapM_ aux $ map cor ts
  where
    cor (PKF.Team.S c)   = c
    cor (PKF.Team.E c _) = c
    aux t =
      (putStr   $ "|" ++ (pad True 4 '0' $ show $ tid)) >>
      (putStrLn $ "|" ++                          tsn)
      where
        tsn = PKF.Team.name t
        tid = PKF.Team.tid t
