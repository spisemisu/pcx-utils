#!/usr/bin/env stack
{- stack
   --resolver lts-20.24
   --install-ghc
   script
   --package bytestring
   --package pretty-show
   --
   --ghc-options -Werror
   --ghc-options -Wall
-}

--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Main
  ( main
  , euverbose
  )
where

--------------------------------------------------------------------------------

import qualified Data.ByteString.Lazy as LBS
import           Data.List
  ( sort
  )
import           Data.Word
  ( Word8
  )
import           Text.Show.Pretty
  ( ppShow
  )

import           Data.PCx.Byte.Common
  ( b2h
  )

--------------------------------------------------------------------------------

main
  :: IO ()

--------------------------------------------------------------------------------

main =
  LBS.readFile exe >>=
  ppp .
  sort .
  filter con .
  map len .
  aux ini [] idx .
  LBS.unpack
  where
    exe = "../../dat/pcf0061/MANAGER.EXE"
    aux i os ps bs
      |   bs ==        [] = []
      |   ps ==        [] = rev os : aux  i         []   idx      bs
      | f ps == take c bs =          aux (i+c) (i-c:os) (g ps) (h bs)
      | otherwise         =          aux (i+1)      os     ps  (g bs)
      where
        f = take 1
        g = drop 1
        h = drop c
        c = 1
    ini = 0001
    idx = euindexes
    len xs =
      (last xs - head xs, dis (head xs) xs, xs)
      where
        dis _ [    ] =           []
        dis p (y:ys) = y-p:dis y ys
    con = \(n, _, _) -> n < 2000
    ppp = mapM_ (putStrLn . ppShow)
    rev = reverse

--------------------------------------------------------------------------------

-- HELPERS

countries
  :: [String]
countries =
  -- Souce: ../pcx/pcf/v61/textos.tls
  [ "XXX"
  , "ALBANIA"
  , "GERMANY"
  , "ARGENTINA"
  , "AUSTRALIA"
  , "AUSTRIA"
  , "AZERBAYAN"
  , "BIELORUSSIA"
  , "BOLIVIA"
  , "BOSNIA"
  , "BRAZIL"
  , "BULGARIA"
  , "BELGIUM"
  , "CAMEROON"
  , "CHILE"
  , "CYPRUS"
  , "COLOMBIA"
  , "CROATIA"
  , "DENMARK"
  , "SCOTLAND"
  , "SLOVAKIA"
  , "SLOVENIA"
  , "SPAIN"
  , "FINLAND"
  , "FRANCE"
  , "GHANA"
  , "GREECE"
  , "HOLLAND"
  , "HONDURAS"
  , "HUNGARY"
  , "ENGLAND"
  , "REP. OF IRELAND"
  , "NORTH. IRELAND"
  , "ICELAND"
  , "FAROE ISLANDS"
  , "ISRAEL"
  , "ITALY"
  , "LITHUANIA"
  , "LUXEMBOURG"
  , "MACEDONIA"
  , "MALTA"
  , "MOROCCO"
  , "MOLDOVA"
  , "NIGERIA"
  , "NORWAY"
  , "WALES"
  , "POLAND"
  , "PORTUGAL"
  , "CZECH REPUBLIC"
  , "ROMANIA"
  , "RUSSIA"
  , "SERBIA"
  , "SOUTH AFRICA"
  , "SWEDEN"
  , "SWITZERLAND"
  , "TURKEY"
  , "UKRAINE"
  , "URUGUAY"
  , "YUGOSLAVIA"
  , "PERU"
  , "CANADA"
  , "U.S.A."
  , "GEORGIA"
  , "COSTA RICA"
  , "PARAGUAY"
  , "JAPAN"
  , "ALGERIA"
  , "TRINIDAD T."
  , "SENEGAL"
  , "SURINAM"
  , "ZAMBIA"
  , "CABO VERDE"
  , "VENEZUELA"
  , "RHODESIA"
  , "SINGAPORE"
  , "ANDORRA"
  , "MOZAMBIQUE"
  , "LIECHTENSTEIN"
  , "LIBERIA"
  , "PANAMA"
  , "ZAIRE"
  , "TADJIKISTAN"
  , "UZBEKISTHAN"
  , "MEXICO"
  , "GUINEA"
  , "ANGOLA"
  , "ZIMBABWE"
  , "SIERRA LEONA"
  , "GUADALUPE"
  , "ECUADOR"
  , "STONIA"
  , "GUINEA BISSAU"
  , "LIBYA"
  , "EGIPTO"
  , "JAMAICA"
  , "N. CALEDONIA"
  , "BERMUDA"
  , "NUEVA ZELANDA"
  , "GUAYANA FR."
  , "SAN VICENTE"
  , "CHAD"
  , "TOGO"
  , "GUINEA CONAKRY"
  , "TANZANIA"
  , "BURKINA FASO"
  , "GAMBIA"
  , "RUANDA"
  , "KENIA"
  , "MAURITANIA"
  , "MALI"
  , "UGANDA"
  , "CONGO"
  , "LETONIA"
  , "COSTA MARFIL"
  , "ARMENIA"
  , "NICARAGUA"
  , "CATALUÑA"
  , "NÍGER"
  , "BARBADOS"
  , "IRAN"
  , "QUATAR"
  , "TUNISIA"
  , "GABON"
  , "TAHITI"
  , "ISLAS MAURICIOS"
  , "MADAGASCAR"
  , "MARTINICA"
  , "VIETNAM"
  ]

euindexes
  :: [Word8]
euindexes =
  [ 0x02, 0x05, 0x0c, 0x12, 0x13
  , 0x16, 0x17, 0x18, 0x1a, 0x1b
  , 0x1e, 0x1f, 0x20, 0x24, 0x26
  , 0x2d, 0x2f, 0x35
  ]

euverbose
  :: [ ( String
       , String
       )
     ]
euverbose =
  map h $
  filter g $
  zip [0..] countries
  where
    f xs = any (`elem` xs)
    g    = f euindexes . (:[]) . fst
    h    = \(i,x) -> (b2h i,x)
