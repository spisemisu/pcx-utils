#!/usr/bin/env stack
{- stack
   --resolver lts-20.24
   --install-ghc
   script
   --
   --ghc-options -Werror
   --ghc-options -Wall
-}

--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import           Data.List
  ( sortBy
  )
import           Data.Ratio
  ( (%)
  )
import           Data.Word
  ( Word16
  , Word8
  )

import qualified Data.PCF.V6.PKF.Types.Player as PKF.Player
import qualified Data.PCF.V6.PKF.Types.Staff  as PKF.Staff
import qualified Data.PCF.V6.PKF.Types.Team   as PKF.Team
import qualified Data.PCF.V6.PKF.Types.Teams  as PKF
import           Data.PCx.Byte.Common
  ( pad
  )

--------------------------------------------------------------------------------

main
  :: IO ()

--------------------------------------------------------------------------------

main =
  readFile tms >>= stats . take 50 . players . teams . parse
  where
    tms = "../pcx/pcf/v61/eq022022.tms"

--------------------------------------------------------------------------------

-- HELPERS

parse
  :: String
  -> PKF.Teams
parse =
  read

teams
  :: PKF.Teams
  -> [PKF.Team.Team]
teams (PKF.Teams ts) =
  ts

players
  :: [PKF.Team.Team]
  -> [ ( Word16
       , PKF.Player.Core
       )
     ]
players =
  sortBy sb . concat . map
  (
    (
      \ t ->
        zip (ci t) $ map pc $ map sp $ filter fp $ PKF.Team.staff t
    ) . tc
  )
  where
    tc (PKF.Team.S c  ) = c
    tc (PKF.Team.E c _) = c
    fp (PKF.Staff.M _) = False
    fp (PKF.Staff.P _) = True
    sp (PKF.Staff.M _) = undefined
    sp (PKF.Staff.P p) = p
    pc (PKF.Player.S c  ) = c
    pc (PKF.Player.E c _) = c
    ci t = cycle [ PKF.Team.tid t ]
    sb (_,p1) (_,p2) =
      compare y x
      where
        x = average p1
        y = average p2

stats
  :: [ ( Word16
       , PKF.Player.Core
       )
     ]
  -> IO ()
stats =
  mapM_ aux
  where
    aux (_, p) =
      (putStr   $ "|" ++ (pad True  5 '0' $ show $ pid)) >>
      (putStr   $ "|" ++ (pad False 3 ' ' $ show $ pos)) >>
      (putStr   $ "|" ++ (pad True  2 '0' $ show $ avg)) >>
      (putStr   $ "|" ++ (pad True  2 '0' $ show $ pac)) >>
      (putStr   $ " " ++ (pad True  2 '0' $ show $ sta)) >>
      (putStr   $ " " ++ (pad True  2 '0' $ show $ agg)) >>
      (putStr   $ " " ++ (pad True  2 '0' $ show $ ski)) >>
      (putStr   $ "|" ++ (pad True  2 '0' $ show $ fin)) >>
      (putStr   $ " " ++ (pad True  2 '0' $ show $ dri)) >>
      (putStr   $ " " ++ (pad True  2 '0' $ show $ pas)) >>
      (putStr   $ " " ++ (pad True  2 '0' $ show $ sho)) >>
      (putStr   $ " " ++ (pad True  2 '0' $ show $ tac)) >>
      (putStr   $ " " ++ (pad True  2 '0' $ show $ goa)) >>
      (putStrLn $ "|" ++ pfn)
      where
        pos = PKF.Player.position p
        pid = PKF.Player.pid p
        avg = average p
        pac = PKF.Player.pace p
        sta = PKF.Player.stamina p
        agg = PKF.Player.aggression p
        ski = PKF.Player.skill p
        fin = PKF.Player.finishing p
        dri = PKF.Player.dribbling p
        pas = PKF.Player.passing p
        sho = PKF.Player.shooting p
        tac = PKF.Player.tackling p
        goa = PKF.Player.goalkeeping p
        pfn = PKF.Player.fullname p

average
  :: PKF.Player.Core
  -> Word8
average p =
  round $ aux
  where
    aux :: Double
    aux =
      fromRational $
      (% 4) $
      sum $
      map fromIntegral [pac, sta, agg, ski]
    pac = PKF.Player.pace p
    sta = PKF.Player.stamina p
    agg = PKF.Player.aggression p
    ski = PKF.Player.skill p
