#!/bin/sh

################################################################################
##
## PCx-Utils, (c) 2023 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

clear

mkdir -v -p eaf/json/
mkdir -v -p eaf/temp/

echo "# teams: eaf/temp/ratings-offset-*.json > eaf/json/teams.json"
jq -s '
   .[] |
   .items |
   map(select(.gender.id == 0)) |
   map(.team)
   ' eaf/temp/ratings-offset-*.json \
       > eaf/temp/teams.json
jq -s '
   [ .[] ] |
   flatten |
   unique
   ' eaf/temp/teams.json \
       > eaf/json/teams.json
