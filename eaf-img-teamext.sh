#!/bin/sh

################################################################################
##
## PCx-Utils, (c) 2023 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

cat eaf/json/teams.json | \
    jq -c --arg x $1 \
       '.[] | 
       select (
       	 .id == ($x | split(",")[] | tonumber)
       )' | \
    jq -c -s 'sort_by(.id)[]' | \
    jq -c -r \
    --argjson lidacid "$(
      jq -c --arg x $1 \
      '[.[] | select ( .id == ($x | split(",")[] | tonumber))]
      | [sort_by(.id)[]]
      | map (.id) 
      | to_entries
      | map( { (.value | tostring): .key } ) 
      | add' \
      eaf/json/teams.json
    )" \
    --argjson pcfteam "$(cat $2)" \
    ' .
    | .cid = ($pcfteam[($lidacid[(.id|tostring)])])
    | "
convert eaf/imgs/teams/\(.id).png \\
	-background black \\
	-alpha off \\
	-type palette \\
	-compress none \\
	-remap eaf/meta/palette.bmp \\
	BMP2:eaf/temp/\(.cid).bmp
convert eaf/temp/\(.cid).bmp \\
	-resize 48x48 \\
	-gravity center \\
	-crop 48x64+0+0 \\
	BMP2:eaf/temp/\(.cid)_mini.bmp
convert eaf/temp/\(.cid)_mini.bmp \\
	-background black \\
	-alpha off \\
	-type palette \\
	-compress none \\
	-remap eaf/meta/palette.bmp \\
	BMP2:eaf/temp/\(.cid)_mini_.bmp
./bin/pcx-colourpalette \\
	bmp=eaf/temp/\(.cid)_mini_.bmp \\
	> eaf/data/miniesc/eq96$(printf %04d \(.cid)).bmp
convert eaf/temp/\(.cid).bmp \\
	-resize 25x25 \\
	-gravity center \\
	-crop 25x32+0+0 \\
	BMP2:eaf/temp/\(.cid)_nano.bmp
convert eaf/temp/\(.cid)_nano.bmp \\
	-background black \\
	-alpha off \\
	-type palette \\
	-compress none \\
	-remap eaf/meta/palette.bmp \\
	BMP2:eaf/temp/\(.cid)_nano_.bmp
./bin/pcx-colourpalette \\
	bmp=eaf/temp/\(.cid)_nano_.bmp \\
	> eaf/data/nanoesc/eq96$(printf %04d \(.cid)).bmp
cp eaf/meta/teamkit.bmp eaf/data/ridiesc/eq96$(printf %04d \(.cid)).bmp
cp eaf/meta/stadium.bmp eaf/data/estadio/eq96$(printf %04d \(.cid)).bmp"
    '

# ImageMagick > Special crop and resize
#
# https://www.imagemagick.org/discourse-server/viewtopic.php?t=13793
