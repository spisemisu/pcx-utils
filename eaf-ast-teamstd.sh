#!/bin/sh

################################################################################
##
## PCx-Utils, (c) 2023 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

cat eaf/json/teams.json | \
    jq -c --arg x $1 \
       '.[] | 
       select (
       	 .id == ($x | split(",")[] | tonumber)
       )' | \
    jq -c -s 'sort_by(.id)[]' | \
    jq -c -r \
    --arg     country $2 \
    --argjson lidacid "$(
      jq -c --arg x $1 \
      '[.[] | select ( .id == ($x | split(",")[] | tonumber))]
      | [sort_by(.id)[]]
      | map (.id) 
      | to_entries
      | map( { (.value | tostring): .key } ) 
      | add' \
      eaf/json/teams.json
    )" \
    --argjson pcfteam "$(cat $3)" \
    ' .
    | .cid = ($pcfteam[($lidacid[(.id | tostring)])])
    | .mid = (.cid + 50000)
    | .ctl = .label
    | "S C { tid = \(.cid), tunknown00 = \(.mid), name = \"\(.ctl)\", stadium = \"\(.ctl) Stadium\", country = \($country), tunknown01 = 0, fullname = \"\(.ctl)\", capacity = 50000, standing = 0, width = 70, length = 100, founded = 0, formation = F442, tactics = T { possession = 50, counter = 50, playstyle = Mixed, tackling = Medium, coverage = Zone, clearance = Short, preasure = Opponent}, staff = [M (S C { mid = \(.mid) , name = [] })] }"
    ' 
