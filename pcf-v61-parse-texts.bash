#!/usr/bin/env bash

################################################################################
##
## PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

clear

mkdir -v -p pcx/pcf/v61

echo "# Extract texts:"

echo "* ../dat/pcf0061/DBDAT/TEXTOS.PKF"
./bin/pcf-v6-parsetexts \
    pkf="../dat/pcf0061/DBDAT/TEXTOS.PKF" \
    ptr="pcx/pcf/v61/textos.ptr" \
    > pcx/pcf/v61/textos.tls
echo "* pcx/pcf/v61/textos.tls"
