#!/bin/sh

################################################################################
##
## PCx-Utils, (c) 2023 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

clear

mkdir -v -p eaf/json/
mkdir -v -p eaf/temp/

echo "# countries: eaf/temp/ratings-offset-*.json > eaf/json/countries.json"
jq -s '
   .[] |
   .items |
   map(select(.gender.id == 0)) |
   map(.nationality)
   ' eaf/temp/ratings-offset-*.json \
       > eaf/temp/countries.json
jq -s '
   [ .[] ] |
   flatten |
   unique
   ' eaf/temp/countries.json \
       > eaf/json/countries.json
