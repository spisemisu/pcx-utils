#!/bin/sh

################################################################################
##
## PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

echo "# Generate default stadium picture"

convert \
    -size 320x240 \
    xc:black \
    -alpha off \
    -type palette \
    -compress none \
    -remap fut/meta/palette.bmp \
    BMP2:fut/temp/stadium.bmp

./bin/pcx-colourpalette \
    bmp=fut/temp/stadium.bmp \
    > fut/meta/stadium.bmp

echo "# Remove temporary files"
rm -fr fut/temp/*

# ImageMagick > how to apply a specific colourmap?
#
# http://www.imagemagick.org/discourse-server/viewtopic.php?t=16505
