#!/bin/sh

################################################################################
##
## PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

cat fut/json/futbest-leagues-and-clubs.json | \
    jq -c --arg x $1 \
       '.[] | 
       select (
         (.uniqueId == .baseId) and 
       	 (.lid == ($x|tonumber))
       )' | \
    jq -c -s 'sort_by(.cid)[]' | \
    jq -c -r \
    --arg     country $2 \
    --argjson lidacid "$(
      jq -c --arg x $1 \
      '[.[] 
      | select ((.lid == ($x|tonumber)))]
      | [sort_by(.cid)[]] 
      | map (.cid) 
      | to_entries
      | map( { (.value|tostring): .key } ) 
      | add' \
      fut/json/futbest-leagues-and-clubs.json
    )" \
    --argjson pcfteam "$(cat $3)" \
    ' .
    | .cid = ($pcfteam[($lidacid[(.cid|tostring)])])
    | .mid = (.cid + 50000)
    | "S C { tid = \(.cid), tunknown00 = \(.mid), name = \"\(.ctl)\", stadium = \"\(.ctl) Stadium\", country = \($country), tunknown01 = 0, fullname = \"\(.ctl)\", capacity = 50000, standing = 0, width = 70, length = 100, founded = 0, formation = F442, tactics = T { possession = 50, counter = 50, playstyle = Mixed, tackling = Medium, coverage = Zone, clearance = Short, preasure = Opponent}, staff = [M (S C { mid = \(.mid) , name = [] })] }"
    ' 
