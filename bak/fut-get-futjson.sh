#!/bin/sh

################################################################################
##
## PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

clear

mkdir -v -p fut/json/
mkdir -v -p fut/temp/

# https://fut.best/
#
# https://fut.best/api/players?page=1&limit=1000

echo "# https://fut.best/api/clubs"
curl "https://fut.best/api/clubs" | \
    jq '.data.clubs' > fut/json/futbest-clubs.json
echo

echo "# https://fut.best/api/countries"
curl "https://fut.best/api/countries" | \
    jq '.data.countries' > fut/json/futbest-countries.json
echo

echo "# https://fut.best/api/leagues"
curl "https://fut.best/api/leagues" | \
    jq '.data.leagues' > fut/json/futbest-leagues.json
echo

for i in {01..20}; do
    echo "# https://fut.best/api/players?page=$i&limit=1000"
    curl "https://fut.best/api/players?page=$i&limit=1000" | \
	jq '.data.players' > fut/temp/futbest-players-page-$i.json
    echo
done
echo "# merge: fut/temp/futbest-players-*.json > fut/json/futbest-players.json"
jq -s '[.[][]]' fut/temp/futbest-players-*.json > fut/json/futbest-players.json
echo "# remove: fut/temp/futbest-players-*.json"
rm fut/temp/futbest-players-*.json

echo "# combine League ids (lid) and Club ids (cid)"
jq \
    'map(
      { "lid": .League.id
      , "ltl": .League.name 
      , "cid": .Club.id 
      , "ctl": .Club.name 
      }) 
    | unique' \
    fut/json/futbest-players.json \
    > fut/json/futbest-leagues-and-clubs.json
echo
