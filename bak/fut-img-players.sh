#!/bin/sh

################################################################################
##
## PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

cat fut/json/futbest-players.json | \
    jq -c --arg x $1 \
       '.[] | 
       select (
         (.uniqueId == .baseId) and 
       	 (.League.id == ($x|tonumber))
       )' | \
    jq -c -s 'sort_by(.Club.id)[]' | \
    jq -c -r \
    --argjson lidacid "$(
      jq -c --arg x $1 \
      '[.[] 
      | select ((.lid == ($x|tonumber)))]
      | [sort_by(.cid)[]] 
      | map (.cid) 
      | to_entries
      | map( { (.value|tostring): .key } ) 
      | add' \
      fut/json/futbest-leagues-and-clubs.json
    )" \
    ' . 
      | "
convert fut/imgs/players/\(.uniqueId).png \\
	-alpha remove \\
	BMP2:fut/temp/\(.id).bmp
convert fut/temp/\(.id).bmp \\
	-resize 32x32 \\
	BMP2:fut/temp/\(.id)_mini.bmp
convert fut/temp/\(.id)_mini.bmp \\
	-shave 1x1 \\
	-bordercolor Black \\
	-border 1x1 \\
	-type palette \\
	-compress none \\
	-remap fut/meta/palette.bmp \\
	BMP2:fut/temp/\(.id)_mini_.bmp
./bin/pcx-colourpalette \\
	bmp=fut/temp/\(.id)_mini_.bmp \\
	> fut/data/minifoto/j96$(printf %05d \(.id)).bmp"
    ' 

# ImageMagick > Special crop and resize
#
# https://www.imagemagick.org/discourse-server/viewtopic.php?t=13793

