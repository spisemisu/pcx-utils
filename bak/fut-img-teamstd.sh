#!/bin/sh

################################################################################
##
## PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

cat fut/json/futbest-leagues-and-clubs.json | \
    jq -c --arg x $1 \
       '.[] | 
       select (
         (.uniqueId == .baseId) and 
       	 (.lid == ($x|tonumber))
       )' | \
    jq -c -s 'sort_by(.cid)[]' | \
    jq -c -r \
    --argjson lidacid "$(
      jq -c --arg x $1 \
      '[.[] 
      | select ((.lid == ($x|tonumber)))]
      | [sort_by(.cid)[]] 
      | map (.cid) 
      | to_entries
      | map( { (.value|tostring): .key } ) 
      | add' \
      fut/json/futbest-leagues-and-clubs.json
    )" \
    --argjson pcfteam "$(cat $2)" \
    ' .
    | .img = ($pcfteam[($lidacid[(.cid|tostring)])])
    | "
convert fut/imgs/clubs/\(.cid).png \\
	-background black \\
	-alpha off \\
	-type palette \\
	-compress none \\
	-remap fut/meta/palette.bmp \\
	BMP2:fut/temp/\(.cid).bmp
convert fut/temp/\(.cid).bmp \\
	-resize 48x48 \\
	-gravity center \\
	-crop 48x64+0+0 \\
	BMP2:fut/temp/\(.cid)_mini.bmp
convert fut/temp/\(.cid)_mini.bmp \\
	-background black \\
	-alpha off \\
	-type palette \\
	-compress none \\
	-remap fut/meta/palette.bmp \\
	BMP2:fut/temp/\(.cid)_mini_.bmp
./bin/pcx-colourpalette \\
	bmp=fut/temp/\(.cid)_mini_.bmp \\
	> fut/data/miniesc/eq96$(printf %04d \(.img)).bmp
convert fut/temp/\(.cid).bmp \\
	-resize 25x25 \\
	-gravity center \\
	-crop 25x32+0+0 \\
	BMP2:fut/temp/\(.cid)_nano.bmp
convert fut/temp/\(.cid)_nano.bmp \\
	-background black \\
	-alpha off \\
	-type palette \\
	-compress none \\
	-remap fut/meta/palette.bmp \\
	BMP2:fut/temp/\(.cid)_nano_.bmp
./bin/pcx-colourpalette \\
	bmp=fut/temp/\(.cid)_nano_.bmp \\
	> fut/data/nanoesc/eq96$(printf %04d \(.img)).bmp
cp fut/meta/teamkit.bmp fut/data/ridiesc/eq96$(printf %04d \(.img)).bmp"
    '

# ImageMagick > Special crop and resize
#
# https://www.imagemagick.org/discourse-server/viewtopic.php?t=13793
