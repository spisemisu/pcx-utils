#!/bin/sh

################################################################################
##
## PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

clear

mkdir -v -p fut/imgs/clubs
mkdir -v -p fut/imgs/countries
mkdir -v -p fut/imgs/leagues
mkdir -v -p fut/imgs/players

echo "# Clubs"
cat fut/json/futbest-clubs.json | \
    jq -r '.[] | "wget -O fut/imgs/clubs/\(.id).png \(.Image.url)"' \
       > fut/temp/futbest-clubs.urls
echo
sh fut/temp/futbest-clubs.urls

echo "# Countries"
cat fut/json/futbest-countries.json | \
    jq -r '.[] | "wget -O fut/imgs/countries/\(.id).png \(.Flag.url)"' \
       > fut/temp/futbest-countries.urls
echo
sh fut/temp/futbest-countries.urls

echo "# Leagues"
cat fut/json/futbest-leagues.json | \
    jq -r '.[] | "wget -O fut/imgs/leagues/\(.id).png \(.Image.url)"' \
       > fut/temp/futbest-leagues.urls
echo
sh fut/temp/futbest-leagues.urls

echo "# Players"
cat fut/json/futbest-players.json | \
    jq '[.[] | select (.uniqueId == .baseId)]' | \
    jq -r '.[] | "wget -O fut/imgs/players/\(.uniqueId).png \(.Avatar.url)"' \
       > fut/temp/futbest-players.urls 
sh fut/temp/futbest-players.urls

echo "# Remove all URL files"
rm fut/temp/*.urls
