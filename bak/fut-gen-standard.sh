#!/bin/sh

################################################################################
##
## PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

lid=$1
iso=$2
nid=$3

echo "### Extracting from JSON"

echo "#### Teams"
./fut-ast-teamstd.sh \
    $lid $iso fut/meta/$lid-$nid.json \
    > fut/temp/$nid.fbt
echo "* fut/temp/$nid.fbt"

echo "#### Players"
./fut-ast-playstd.sh \
    $lid      fut/meta/$lid-$nid.json \
    > fut/temp/$nid.fbp
echo "* fut/temp/$nid.fbp"

echo "#### Merge"

./bin/fut-v6-parseteams \
    aby="2003" \
    fbt="fut/temp/$nid.fbt" \
    fbp="fut/temp/$nid.fbp" \
    > fut/data/$nid.tms
echo "* fut/data/$nid.tms"

echo "#### Convert to DBCs"

mkdir -v -p fut/data/eq022022

./bin/pcf-v6-bytesteams \
    tms="fut/data/$nid.tms" \
    dbc="fut/data/eq022022/"
echo "* fut/data/eq022022/"

echo "#### Remove temporary files"
rm -fr fut/temp/*

echo "### Transforming images"

mkdir -v -p fut/temp/

echo "#### Remove temporary files"
rm -fr fut/temp/*

echo "#### Teams images"
mkdir -v -p fut/data/estadio
mkdir -v -p fut/data/miniesc
mkdir -v -p fut/data/nanoesc
mkdir -v -p fut/data/ridiesc

./fut-img-teamstd.sh \
    $lid      fut/meta/$lid-$nid.json \
    > fut/temp/$nid.ftb
echo "* fut/temp/$nid.ftb"
sh fut/temp/$nid.ftb

echo "#### Remove temporary files"
rm -fr fut/temp/*

echo "#### Players images"
mkdir -v -p fut/data/minifoto

./fut-img-players.sh \
    $lid \
    > fut/temp/$nid.fpb
echo "* fut/temp/$nid.fpb"
sh fut/temp/$nid.fpb

echo "#### Remove temporary files"
rm -fr fut/temp/* 
