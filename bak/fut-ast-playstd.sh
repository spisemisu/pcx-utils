#!/bin/sh

################################################################################
##
## PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

cat fut/json/futbest-players.json | \
    jq -c --arg x $1 \
       '.[] | 
       select (
         (.uniqueId == .baseId) and 
       	 (.League.id == ($x|tonumber))
       )' | \
    jq -c -s 'sort_by(.Club.id)[]' | \
    jq -c -r \
    --argjson iso3166 "$(jq -s 'add' fut/meta/iso3166.json fut/meta/add3166.json)" \
    --argjson pcf3166 "$(cat fut/meta/pcf3166.json)" \
    --argjson pcfposi "$(cat fut/meta/pcfposi.json)" \
    --argjson pcfrole "$(cat fut/meta/pcfrole.json)" \
    --argjson lidacid "$(
      jq -c --arg x $1 \
      '[.[] 
      | select ((.lid == ($x|tonumber)))]
      | [sort_by(.cid)[]] 
      | map (.cid) 
      | to_entries
      | map( { (.value|tostring): .key } ) 
      | add' \
      fut/json/futbest-leagues-and-clubs.json
    )" \
    --argjson pcfteam "$(cat $2)" \
    ' . 
      | .cid = ($pcfteam[($lidacid[(.Club.id|tostring)])])
      | .hgt = (if .height                == null then 140 else .height                end)
      | .wgt = (if .weight                == null then 001 else .weight                end)
      | .pac = (if .pace                  == null then 050 else .pace                  end)
      | .sta = (if .physicalityStamina    == null then 050 else .physicalityStamina    end)
      | .agg = (if .physicalityAggression == null then 050 else .physicalityAggression end)
      | .fin = (if .shootingFinishing     == null then 050 else .shootingFinishing     end)
      | .dri = (if .dribbling             == null then 050 else .dribbling             end)
      | .pas = (if .passing               == null then 050 else .passing               end)
      | .sho = (if .shooting              == null then 050 else .shooting              end)
      | .tac = (if .defending             == null then 050 else .defending             end)
      | .a3c = 
               ( if   $iso3166[(.Country.name)] 
                 then 
               	      if   $pcf3166[$iso3166[(.Country.name)]]
                      then $iso3166[(.Country.name)] 
                      else "XXX"
                      end 
                 else "XXX" 
                 end
               ) 
      | .pos  = $pcfposi[(.Position.name)]
      | .rol  = $pcfrole[(.Position.name)]
      | "(\(.cid), S C { pid = \(.id), number = 0, name = \"\(.nameOnCard)\", fullname = \"\(.fullName)\", index = 0, status = Veteran, roles = \(.rol), citizenship = \(.a3c), skin = Skin, hair = Hair, position = \(.pos), birthday = ( 0000 , 0 , 0 ), height = \(.hgt), weight = \(.wgt), pace = \(.pac), stamina = \(.sta), aggression = \(.agg), skill = \(.rating), finishing = \(.fin), dribbling = \(.dri), passing = \(.pas), shooting = \(.sho), tackling = \(.tac), goalkeeping = \(.rating) })"
    '
