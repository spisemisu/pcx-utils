#!/bin/sh

################################################################################
##
## PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

echo "# Extended"
echo

echo "## FUT/FIFA Icons"
./fut-gen-extended.sh 001 ESP esp-icons
echo

echo "## Primera (Spain)"
./fut-gen-extended.sh 002 ESP esp-primera
echo

echo "## Segunda (Spain)"
./fut-gen-extended.sh 022 ESP esp-segunda
echo

echo "# Standard"
echo

echo "## Scudetto (Italy)"
./fut-gen-standard.sh 003 ITA ita-scudetto
echo

echo "## Ligue One (France)"
./fut-gen-standard.sh 004 FRA fra-ligueone
echo

echo "## Premier (England/Wales)"
./fut-gen-standard.sh 005 GBR_ENG eng-premier
echo

echo "## Bundesliga (Germany)"
./fut-gen-standard.sh 006 DEU deu-budesliga
echo

echo "## Primeira (Portugal)"
./fut-gen-standard.sh 007 PRT prt-primeira
echo

echo "## Eredivisie (The Netherlands)"
./fut-gen-standard.sh 008 NLD nld-eredivisie
echo

echo "## Süper Lig (Turkey)"
./fut-gen-standard.sh 010 TUR tur-superlig
echo

echo "## First Division A (Belgium)"
./fut-gen-standard.sh 012 BEL bel-proleague
echo

echo "## Primera (Argentina)"
./fut-gen-standard.sh 014 ARG arg-primera
echo

echo "## Primera A (Colombia)"
./fut-gen-standard.sh 024 COL col-primera
echo

echo "## Superliga (Denmark)"
./fut-gen-standard.sh 026 DNK dnk-superliga
echo

echo "## Super League (Switzerland)"
./fut-gen-standard.sh 027 CHE che-superleague
echo

echo "## Allsvenskan (Sweden)"
./fut-gen-standard.sh 028 SWE swe-allsvenskan
echo

echo "## Premiership (Scotland)"
./fut-gen-standard.sh 030 GBR_SCT sct-premiership
echo

echo "## Bundesliga (Austria)"
./fut-gen-standard.sh 031 AUT aut-budesliga
echo

echo "## Eliteserien (Norway)"
./fut-gen-standard.sh 033 NOR nor-eliteserien
echo

echo "## Ekstraklasa (Poland)"
./fut-gen-standard.sh 035 POL pol-ekstraklasa
echo

echo "## Primera División (Chile)"
./fut-gen-standard.sh 036 CHL chl-primera
echo

echo "## Liga One (Romania)"
./fut-gen-standard.sh 039 ROU rou-ligaone
echo

echo "## Premier Division (Ireland)"
./fut-gen-standard.sh 042 IRL irl-premier
echo
