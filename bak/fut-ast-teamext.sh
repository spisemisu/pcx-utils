#!/bin/sh

################################################################################
##
## PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

cat fut/json/futbest-leagues-and-clubs.json | \
    jq -c --arg x $1 \
       '.[] | 
       select (
         (.uniqueId == .baseId) and 
       	 (.lid == ($x|tonumber))
       )' | \
    jq -c -s 'sort_by(.cid)[]' | \
    jq -c -r \
    --arg     country $2 \
    --argjson lidacid "$(
      jq -c --arg x $1 \
      '[.[] 
      | select ((.lid == ($x|tonumber)))]
      | [sort_by(.cid)[]] 
      | map (.cid) 
      | to_entries
      | map( { (.value|tostring): .key } ) 
      | add' \
      fut/json/futbest-leagues-and-clubs.json
    )" \
    --argjson pcfteam "$(cat $3)" \
    ' .
    | .cid = ($pcfteam[($lidacid[(.cid|tostring)])])
    | .mid = (.cid + 50000)
    | "E C { tid = \(.cid), tunknown00 = \(.mid), name = \"\(.ctl)\", stadium = \"\(.ctl) Stadium\", country = \($country), tunknown01 = 0, fullname = \"\(.ctl)\", capacity = 50000, standing = 0, width = 70, length = 100, founded = 0, formation = F442, tactics = T { possession = 50, counter = 50, playstyle = Mixed, tackling = Medium, coverage = Zone, clearance = Short, preasure = Opponent}, staff = [M (E (C { mid = \(.mid), name = [] }) (D {fullname = [], mdunknown00 = [], tactics = [], honours = [], miscellaneous = [], lastseason = [], managercareer = [], mdunknown01 = 3, playercareer = [], statements = []}))]} D { built = 0, members = 0, president = [], budget = 5000, budgetpro = 5000, sponsor = [], supplier = [], reserve = 65535, stats = T { unknown00 = 65535, unknown01 = 3, lastdecade = [ ], seasons = 0, matches = 0, win = 0, draw = 0, gf = 0, ga = 0, points = 0, champion = 0, runnersup = 0, lastseason = [ ], uefa = ( 0 , 0 ), cup = ( 0 , 0 ), championsleague = ( 0 , 0 ), cupwinnerscup = ( 0 , 0 ), supercup = ( 0 , 0 ), intercontinental = ( 0 , 0 ), eufasupercup = ( 0 , 0 )}}"
    '
