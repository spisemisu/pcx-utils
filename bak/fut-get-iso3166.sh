#!/bin/sh

################################################################################
##
## PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

curl https://restcountries.eu/rest/v2/all | \
    jq -c 'map( { (.name|tostring): (.alpha3Code) } ) | add' \
       > fut/meta/iso3166.json

# References
#
# - https://www.iso.org/iso-3166-country-codes.html
#
# - https://www.iso.org/obp/ui/#search/code/
#
# - https://restcountries.eu/
