#!/bin/sh

################################################################################
##
## PCx-Utils, (c) 2023 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

echo "# Extended"
echo

echo "## Primera (Spain)"
./eaf-gen-extended.sh ESP esp-primera "240,241,243,448,449,450,453,457,461,463,472,479,480,481,483,1860,1861,1968,110062,110832"
echo

echo "## Segunda (Spain)"
./eaf-gen-extended.sh ESP esp-segunda "244,260,452,456,459,462,467,468,1853,1854,10846,100831,100851,100888,110069,110242,110827,110839,110902,113356,114554,131110"
echo

echo "# Standard"
echo

echo "## Scudetto (Italy)"
./eaf-gen-standard.sh ITA ita-scudetto "44,45,47,54,55,189,206,347,1746,1842,110373,110374,110556,111657,111811,111974,114912,115841,115845,116365"
echo

echo "## Ligue One (France)"
./eaf-gen-standard.sh FRA fra-ligueone "64,65,66,68,69,70,71,72,73,74,76,217,219,378,379,1738,1809,1815"
echo

echo "## Premier (England/Wales)"
./eaf-gen-standard.sh GBR_ENG eng-premier "1,2,5,7,9,10,11,13,14,18,19,110,144,1794,1796,1799,1808,1923,1925,1943"
echo

echo "## Bundesliga (Germany)"
./eaf-gen-standard.sh DEU deu-budesliga "21,22,23,25,31,32,36,38,160,169,175,1824,1831,10029,100409,110502,111235,112172"
echo

echo "## Primeira (Portugal)"
./eaf-gen-standard.sh PRT prt-primeira "234,236,237,489,518,718,744,1887,1888,1896,1898,1900,10020,10031,111539,112513,112809,114510"
echo

echo "## Eredivisie (The Netherlands)"
./eaf-gen-standard.sh NLD nld-eredivisie "245,246,247,634,645,1903,1905,1906,1908,1909,1910,1913,1914,1971,100632,100634,100646,111380"
echo

echo "## Süper Lig (Turkey)"
./eaf-gen-standard.sh TUR tur-superlig "325,326,327,436,741,746,748,101007,101014,101016,101020,101028,101033,101037,101041,110776,111117,111339,113142,131389"
echo

echo "## First Division A (Belgium)"
./eaf-gen-standard.sh BEL bel-proleague "229,230,231,232,670,673,674,680,681,1750,2013,2014,8001,100081,100087,110724"
echo

echo "## Primera (Argentina)"
./eaf-gen-standard.sh ARG arg-primera "101088,110093,110394,110395,110404,110406,110580,110953,111022,111706,111708,111716,112670,112689,112713,112965,113044,111019,1877,111710,101083,101084,111711,110396,101085,1876,1013,110953"
echo

#echo "## Primera A (Colombia)"
#./eaf-gen-standard.sh COL col-primera ""
#echo

echo "## Superliga (Denmark)"
./eaf-gen-standard.sh DNK dnk-superliga "269,270,271,272,819,822,1443,1516,1786,1788,2002,15001"
echo

echo "## Super League (Switzerland)"
./eaf-gen-standard.sh CHE che-superleague "322,324,894,896,897,898,900,1704,1713,1862,10032,115201"
echo

echo "## Allsvenskan (Sweden)"
./eaf-gen-standard.sh SWE swe-allsvenskan "319,320,321,433,700,702,708,710,711,1439,111705,112072,112126,113458,113743,113892"
echo

echo "## Premiership (Scotland)"
./eaf-gen-standard.sh GBR_SCT sct-premiership "77,78,80,81,82,83,86,180,621,631,100804,100805"
echo

echo "## Bundesliga (Austria)"
./eaf-gen-standard.sh AUT aut-budesliga "191,209,252,254,256,781,1787,2017,15009,15040,110720,111822"
echo

#echo "## Eliteserien (Norway)"
#./eaf-gen-standard.sh NOR nor-eliteserien ""
#echo

echo "## Ekstraklasa (Poland)"
./eaf-gen-standard.sh POL pol-ekstraklasa "301,420,873,874,1871,110745,110746,110747,110749,111083,111085,111086,111088,111092,112511,114004,114326,114393"
echo

#echo "## Primera División (Chile)"
#./eaf-gen-standard.sh CHL chl-primera ""
#echo

#echo "## Liga One (Romania)"
#./eaf-gen-standard.sh ROU rou-ligaone ""
#echo

echo "## Premier Division (Ireland)"
./eaf-gen-standard.sh IRL irl-premier "305,306,422,423,445,563,834,837,1572,111132"
echo

echo "## MLS (United States of America)"
./eaf-gen-standard.sh USA usa-mls "687,688,689,691,693,694,695,696,697,698,101112,111065,111138,111139,111140,111144,111651,111928,112134,112606,112828,112885,112893,112996,113018,113149,114161,114162,114640"
echo

echo "## Super League (China)"
./eaf-gen-standard.sh XXX chn-superleague "110955,111724,111768,111769,111773,111774,111779,112163,112378,112540,112979,112985,114628,116360,116361,131173"
echo

echo "## Pro League (Saudi Arabia)"
./eaf-gen-standard.sh XXX sau-proleague "605,607,111674,112096,112139,112387,112390,112392,112393,112408,112572,112883,113037,113057,113058,113060,113217,113222"
echo
