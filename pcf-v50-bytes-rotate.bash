#!/usr/bin/env bash

################################################################################
##
## PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

clear

mkdir -v -p pcx/pcf/v50

echo "# Rotate bytes:"

echo "## Teams:"

echo "* ../dat/pcf0050/DBDAT/EQUIPOS.PKF"
./bin/pcx-bytesrotation \
    pkf="../dat/pcf0050/DBDAT/EQUIPOS.PKF" \
    > pcx/pcf/v50/equipos.rot
echo "* pcx/pcf/v50/equipos.rot"
