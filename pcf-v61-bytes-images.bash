#!/usr/bin/env bash

################################################################################
##
## PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

clear

mkdir -v -p pcx/pcf/v60
mkdir -v -p pcx/pcf/v61

echo "# Extract images"

echo "## Flags:"

mkdir -v -p pcx/pcf/v61/banderas
echo "* ../dat/pcf0061/DBDAT/BANDERAS.PKF"
./bin/pcx-extractimages \
    pfx="ba96" \
    pad="0x04" \
    pkf="../dat/pcf0061/DBDAT/BANDERAS.PKF" \
    ptr="pcx/pcf/v61/banderas.ptr" \
    bmp="pcx/pcf/v61/banderas/"
echo "* pcx/pcf/v61/banderas/"

mkdir -v -p pcx/pcf/v61/miniband
echo "* ../dat/pcf0061/DBDAT/MINIBAND.PKF"
./bin/pcx-extractimages \
    pfx="ba96" \
    pad="0x04" \
    pkf="../dat/pcf0061/DBDAT/MINIBAND.PKF" \
    ptr="pcx/pcf/v61/miniband.ptr" \
    bmp="pcx/pcf/v61/miniband/"
echo "* pcx/pcf/v61/miniband/"

echo "## Teams:"

mkdir -v -p pcx/pcf/v60/estadio
echo "* ../dat/pcf0060/DBDAT/ESTADIO.PKF"
./bin/pcx-extractimages \
    pfx="eq96" \
    pad="0x04" \
    pkf="../dat/pcf0060/DBDAT/ESTADIO.PKF" \
    ptr="pcx/pcf/v60/estadio.ptr" \
    bmp="pcx/pcf/v60/estadio/"
echo "* pcx/pcf/v60/estadio/"

mkdir -v -p pcx/pcf/v61/miniesc
echo "* ../dat/pcf0061/DBDAT/MINIESC.PKF"
./bin/pcx-extractimages \
    pfx="eq96" \
    pad="0x04" \
    pkf="../dat/pcf0061/DBDAT/MINIESC.PKF" \
    ptr="pcx/pcf/v61/miniesc.ptr" \
    bmp="pcx/pcf/v61/miniesc/"
echo "* pcx/pcf/v61/miniesc/"

mkdir -v -p pcx/pcf/v61/nanoesc
echo "* ../dat/pcf0061/DBDAT/NANOESC.PKF"
./bin/pcx-extractimages \
    pfx="eq96" \
    pad="0x04" \
    pkf="../dat/pcf0061/DBDAT/NANOESC.PKF" \
    ptr="pcx/pcf/v61/nanoesc.ptr" \
    bmp="pcx/pcf/v61/nanoesc/"
echo "* pcx/pcf/v61/nanoesc/"

mkdir -v -p pcx/pcf/v61/ridiesc
echo "* ../dat/pcf0061/DBDAT/RIDIESC.PKF"
./bin/pcx-extractimages \
    pfx="eq96" \
    pad="0x04" \
    pkf="../dat/pcf0061/DBDAT/RIDIESC.PKF" \
    ptr="pcx/pcf/v61/ridiesc.ptr" \
    bmp="pcx/pcf/v61/ridiesc/"
echo "* pcx/pcf/v61/ridiesc/"

echo "## Players:"

mkdir -v -p pcx/pcf/v61/minifoto
echo "* ../dat/pcf0061/DBDAT/MINIFOTO.PKF"
./bin/pcx-extractimages \
    pfx="j96" \
    pad="0x05" \
    pkf="../dat/pcf0061/DBDAT/MINIFOTO.PKF" \
    ptr="pcx/pcf/v61/minifoto.ptr" \
    bmp="pcx/pcf/v61/minifoto/"
echo "* pcx/pcf/v61/minifoto/"
