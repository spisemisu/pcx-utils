--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import qualified Data.ByteString.Lazy as LBS

--------------------------------------------------------------------------------

main
  :: IO ()

--------------------------------------------------------------------------------

main =
  LBS.getContents >>=
  LBS.putStr .
  LBS.pack .
  aux .
  LBS.unpack
  where
    aux [                                                              ] =
      [           ]
    aux (0x6c:0x07:0x00:0x00:0x76:0x08:0x81:0xfd:0xd0:0x07:0x00:0x00:bs) =
      rep ++ aux bs
    aux (                                                       byte:bs) =
      byte : aux bs
    rep =
      {- INITIAL
      -- Replace 1900 and 2000 with 00001 and 65535
      0x01:0x00:0x00:0x00:0x76:0x08:0x81:0xfd:0xff:0xff:0x00:0x00:[]
      -- > Note: Use 0000-00-00 for random birthday
      -}
      -- Replace 1900 and 2000 with 1900 and 65535
      0x6c:0x07:0x00:0x00:0x76:0x08:0x81:0xfd:0xff:0xff:0x00:0x00:[]
      -- > Note: Use 1900-00-00 for random birthday

-- Emacs > Regexp I-search: 6c.*?07.*?d0.*?07 (MANAGER.EXE)
--
-- 000b2470: fd6c 0700 0076 0881 fdd0 0700 0072 1f6a  .l...v.......r.j
