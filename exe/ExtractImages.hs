--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import qualified Data.ByteString.Lazy         as LBS
import           Data.List
  ( isPrefixOf
  )
import           Data.Maybe
  ( listToMaybe
  )
import           Data.Word
  ( Word16
  )
import           System.Environment
  ( getArgs
  )

import           Data.PCx.Byte.Common
  ( ByteStream (bytes)
  , Bytes
  )
import qualified Data.PCx.Byte.Common         as PKF
import qualified Data.PCx.Byte.Parser         as Parser
import qualified Data.PCx.Vx.PKF.Parse.Images as PKF
import qualified Data.PCx.Vx.PKF.Types.Image  as PKF.Image
import qualified Data.PCx.Vx.PKF.Types.Images as PKF

--------------------------------------------------------------------------------

main
  :: IO ()

--------------------------------------------------------------------------------

main =
  pfxcons >>= pfx
  where
    pfx          mv =
      case mv of
        Nothing -> putStrLn msg
        Just  p ->
            padcons >>= pad p
      where
        msg = "No file prefix provided. Example of usage: … pfx=\"eq96\" …"
    pad p        mv =
      case mv of
        Nothing -> putStrLn msg
        Just  z ->
            bmppath >>= dir p (read z)
      where
        msg = "No file zero-pad provided. Example of usage: … pad=\"0x04\" …"
    dir p z      mp =
      case mp of
        Nothing -> putStrLn msg
        Just  d ->
            ptrpath >>= ptr p z d
      where
        msg = "No BMP folder provided. Example of usage: … bmp=\"nanoesc/\" …"
    ptr p z d    mp =
      case mp of
        Nothing -> putStrLn msg
        Just  f ->
          readFile f >>= \ cs -> pkfpath >>= pkf p z d (read cs)
      where
        msg = "No PTR file provided. Example of usage: … ptr=\"nanoesc.ptr\" …"
    pkf p z d ps mp =
      case mp of
        Nothing -> putStrLn msg
        Just  f ->
          LBS.readFile f >>= aux p z d ps . LBS.unpack
      where
        msg = "No PKF file provided. Example of usage: … pkf=\"NANOESC.PKF\" …"
    aux p z d ps bs =
      case Parser.run (PKF.imagesP ps) bs of
        Right ast -> write p z d $ images $ ast
        Left  err -> putStrLn err

--------------------------------------------------------------------------------

-- HELPERS

pfxcons
  :: IO (Maybe String)
pfxcons =
  listToMaybe . map (drop len) . filter (isPrefixOf pat) <$> getArgs
  where
    pat = "pfx="
    len = length pat

padcons
  :: IO (Maybe String)
padcons =
  listToMaybe . map (drop len) . filter (isPrefixOf pat) <$> getArgs
  where
    pat = "pad="
    len = length pat

bmppath
  :: IO (Maybe String)
bmppath =
  listToMaybe . map (drop len) . filter (isPrefixOf pat) <$> getArgs
  where
    pat = "bmp="
    len = length pat

ptrpath
  :: IO (Maybe String)
ptrpath =
  listToMaybe . map (drop len) . filter (isPrefixOf pat) <$> getArgs
  where
    pat = "ptr="
    len = length pat

pkfpath
  :: IO (Maybe String)
pkfpath =
  listToMaybe . map (drop len) . filter (isPrefixOf pat) <$> getArgs
  where
    pat = "pkf="
    len = length pat

images
  :: PKF.Images
  -> [ ( Word16
       , Bytes
       )
     ]
images (PKF.Images xs) =
  map aux xs
  where
    aux i =
      ( PKF.Image.uid i
      , bytes i
      )

write
  :: String
  -> Int
  -> String
  -> [ ( Word16
       , Bytes
       )
     ]
  -> IO ()
write p z d =
  mapM_ aux
  where
    aux (uid, bs) =
      LBS.writeFile n $ LBS.pack bs
      where
        i = PKF.pad True z '0' $ show uid
        n = d ++ "/" ++ p ++ i ++ ".bmp"
