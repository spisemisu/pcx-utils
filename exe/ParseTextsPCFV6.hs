--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import qualified Data.ByteString.Lazy            as LBS
import           Data.Char
  ( chr
  )
import           Data.List
  ( isPrefixOf
  )
import           Data.Maybe
  ( listToMaybe
  )
import           System.Environment
  ( getArgs
  )
import           Text.Show.Pretty
  ( ppShow
  )

import qualified Data.PCF.V6.PKF.Parse.TextLists as PKF
import qualified Data.PCx.Byte.Parser            as Parser

--------------------------------------------------------------------------------

main
  :: IO ()

--------------------------------------------------------------------------------

main =
  ptrpath >>= ptr
  where
    ptr    mp =
      case mp of
        Nothing -> putStrLn msg
        Just  p ->
          readFile p >>= \ cs -> pkfpath >>= pkf (read cs)
      where
        msg = "No PTR file provided. Example of usage: … ptr=\"textos.ptr\" …"
    pkf ps mp =
      case mp of
        Nothing -> putStrLn msg
        Just  p ->
          LBS.readFile p >>= putStrLn . aux ps . LBS.unpack
      where
        msg = "No PKF file provided. Example of usage: … pkf=\"TEXTOS.PKF\" …"
    aux ps bs =
      case Parser.run (PKF.textlistsP ps) bs of
        Right ast -> esc False $ ppShow ast
        Left  err -> err
    -- Unescape Extended ASCII
    esc _ [             ]        = [        ]
    esc b ('\\':'"'  :xs)        = q:esc n xs
      where
        q = if  b then '»' else '«'
        n = not b
    esc b ('\\':x:y:z:xs)
      | '\048' < x && x < '\051' && -- 0 <  x <  3 -> ['\1xx' - '\2xx']
        '\047' < y && y < '\058' && -- 0 <= y <= 9
        '\047' < z && z < '\058' =  -- 0 <= z <= 9
        c:esc b xs
      where
        c =
          chr
            ( read [x] * 100
            + read [y] * 010
            + read [z]
            )
    esc b (         x:xs)        = x:esc b xs

--------------------------------------------------------------------------------

-- HELPERS

ptrpath
  :: IO (Maybe String)
ptrpath =
  listToMaybe . map (drop len) . filter (isPrefixOf pat) <$> getArgs
  where
    pat = "ptr="
    len = length pat

pkfpath
  :: IO (Maybe String)
pkfpath =
  listToMaybe . map (drop len) . filter (isPrefixOf pat) <$> getArgs
  where
    pat = "pkf="
    len = length pat
