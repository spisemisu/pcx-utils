--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import qualified Data.ByteString.Lazy as LBS
import           Data.List
  ( isPrefixOf
  )
import           Data.Maybe
  ( listToMaybe
  )
import           System.Environment
  ( getArgs
  )

import           Data.PCx.Byte.Common
  ( n2b
  )

--------------------------------------------------------------------------------

main
  :: IO ()

--------------------------------------------------------------------------------

main =
  ytdcons >>= ytd
  where
    ytd mv =
      case mv of
        Nothing -> putStrLn msg
        Just  y ->
          LBS.getContents >>=
          LBS.putStr .
          LBS.pack .
          aux (year y) (1 + year y) .
          LBS.unpack
      where
        msg = "No Year-to-date provided. Example of usage: … ytd=\"2019\" …"
    aux _ _ [            ] =                  []
    aux f t (0xcd:0x07:bs) = ybs f ++ aux f t bs
    aux f t (0xce:0x07:bs) = ybs t ++ aux f t bs
    aux f t (     byte:bs) = byte  :  aux f t bs
    ybs =
      take 2 . (++ repeat 0x00) . n2b 8 id

--------------------------------------------------------------------------------

-- HELPERS

ytdcons
  :: IO (Maybe String)
ytdcons =
  listToMaybe . map (drop len) . filter (isPrefixOf pat) <$> getArgs
  where
    pat = "ytd="
    len = length pat

year
  :: String
  -> Word
year =
  read
