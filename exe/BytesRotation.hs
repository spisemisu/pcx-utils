--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import qualified Data.ByteString.Lazy as LBS
import           Data.List
  ( isPrefixOf
  )
import           Data.Maybe
  ( listToMaybe
  )
import           System.Environment
  ( getArgs
  )

import qualified Data.PCx.Byte.Rotate as ByteRotate

--------------------------------------------------------------------------------

main
  :: IO ()

--------------------------------------------------------------------------------

main =
  pkfpath >>= pkf
  where
    pkf mp =
      case mp of
        Nothing -> putStrLn msg
        Just  p ->
          LBS.readFile p >>= LBS.putStr . LBS.map ByteRotate.bijective
      where
        msg = "No PKF file provided. Example of usage: … pkf=\"EQ022022.PKF\" …"

--------------------------------------------------------------------------------

-- HELPERS

pkfpath
  :: IO (Maybe String)
pkfpath =
  listToMaybe . map (drop len) . filter (isPrefixOf pat) <$> getArgs
  where
    pat = "pkf="
    len = length pat
