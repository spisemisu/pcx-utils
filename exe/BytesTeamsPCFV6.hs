--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import qualified Data.ByteString.Lazy          as LBS
import           Data.List
  ( isPrefixOf
  )
import           Data.Maybe
  ( listToMaybe
  )
import           Data.Word
  ( Word16
  )
import           System.Environment
  ( getArgs
  )

import qualified Data.PCF.V6.PKF.Types.Manager as PKF.Manager
import qualified Data.PCF.V6.PKF.Types.Player  as PKF.Player
import qualified Data.PCF.V6.PKF.Types.Staff   as PKF.Staff
import qualified Data.PCF.V6.PKF.Types.Team    as PKF.Team
import qualified Data.PCF.V6.PKF.Types.Teams   as PKF
import           Data.PCx.Byte.Common
  ( ByteStream (bytes)
  , Bytes
  , pad
  )

--------------------------------------------------------------------------------

main
  :: IO ()

--------------------------------------------------------------------------------

main =
  dbcpath >>= dir
  where
    dir mp =
      case mp of
        Nothing -> putStrLn msg
        Just  d ->
            tmspath >>= tms d
      where
        msg = "No DBC folder provided. Example of usage: … dbc=\"eq022022/\" …"
    tms d mp =
      case mp of
        Nothing -> putStrLn msg
        Just  p ->
          readFile p >>= write d . teams . parse
      where
        msg = "No TMS file provided. Example of usage: … tms=\"eq022022.tms\" …"

--------------------------------------------------------------------------------

-- HELPERS

dbcpath
  :: IO (Maybe String)
dbcpath =
  listToMaybe . map (drop len) . filter (isPrefixOf pat) <$> getArgs
  where
    pat = "dbc="
    len = length pat

tmspath
  :: IO (Maybe String)
tmspath =
  listToMaybe . map (drop len) . filter (isPrefixOf pat) <$> getArgs
  where
    pat = "tms="
    len = length pat

parse
  :: String
  -> PKF.Teams
parse =
  read

teams
  :: PKF.Teams
  -> [ ( Word16
       , Bytes
       )
     ]
teams (PKF.Teams ts) =
  map (aux . notxt) ts
  where
    aux t =
      ( uid t
      , bytes t
      )
    uid (PKF.Team.S c  ) = PKF.Team.tid c
    uid (PKF.Team.E c _) = PKF.Team.tid c

notxt
  :: PKF.Team.Team
  -> PKF.Team.Team
notxt =
  aux
  where
    aux (PKF.Team.S c  ) = PKF.Team.S (cor c)
    aux (PKF.Team.E c d) = PKF.Team.E (cor c) (tdb d)
    cor t =
      t { PKF.Team.staff = map f $ PKF.Team.staff t }
      where
        f   (PKF.Staff.M m) = PKF.Staff.M $ g m
        f   (PKF.Staff.P p) = PKF.Staff.P $ h p
        g s@(PKF.Manager.S _)   = s
        g   (PKF.Manager.E c d) = PKF.Manager.E c (mdb d)
        h s@(PKF.Player.S _)   = s
        h   (PKF.Player.E c d) = PKF.Player.E c (pdb d)
    tdb d = d
      { PKF.Team.president = []
      , PKF.Team.sponsor = []
      , PKF.Team.supplier = []
      }
    mdb d = d
      { PKF.Manager.fullname = []
      , PKF.Manager.mdunknown00 = []
      , PKF.Manager.tactics = []
      , PKF.Manager.honours = []
      , PKF.Manager.miscellaneous = []
      , PKF.Manager.lastseason = []
      , PKF.Manager.managercareer = []
      , PKF.Manager.playercareer = []
      , PKF.Manager.statements = []
      }
    pdb d = d
      { PKF.Player.country = 0x00
      , PKF.Player.birthplace = []
      , PKF.Player.fromteam = []
      , PKF.Player.nationalteam = []
      , PKF.Player.pdunknown00 = []
      , PKF.Player.features = []
      , PKF.Player.honours = []
      , PKF.Player.intcaps = []
      , PKF.Player.miscellaneous = []
      , PKF.Player.lastseason = []
      , PKF.Player.career = []
      }

write
  :: String
  -> [ ( Word16
       , Bytes
       )
     ]
  -> IO ()
write d =
  mapM_ aux
  where
    aux (uid, bs) =
      LBS.writeFile n $ LBS.pack bs
      where
        i = pad True 4 '0' $ show uid
        n = d ++ "/eq97" ++ i ++ ".dbc"
