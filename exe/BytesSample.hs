--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import           Data.Int
  ( Int64
  )

import qualified Data.ByteString.Lazy as LBS
import           Data.List
  ( isPrefixOf
  )
import           Data.Maybe
  ( listToMaybe
  )
import           System.Environment
  ( getArgs
  )

--------------------------------------------------------------------------------

main
  :: IO ()

--------------------------------------------------------------------------------

main =
  offcons >>= off
  where
    off mv =
      case mv of
        Nothing -> putStrLn msg
        Just  o ->
          lencons >>= len o
      where
        msg = "No offset provided. Example of usage: … off=\"1024\" …"
    len o mv =
      case mv of
        Nothing -> putStrLn msg
        Just  l ->
          LBS.getContents >>=
          LBS.putStr .
          LBS.take (int l) .
          LBS.drop (int o)
      where
        msg = "No length provided. Example of usage: … len=\"8192\" …"

--------------------------------------------------------------------------------

-- HELPERS

offcons
  :: IO (Maybe String)
offcons =
  listToMaybe . map (drop len) . filter (isPrefixOf pat) <$> getArgs
  where
    pat = "off="
    len = length pat

lencons
  :: IO (Maybe String)
lencons =
  listToMaybe . map (drop len) . filter (isPrefixOf pat) <$> getArgs
  where
    pat = "len="
    len = length pat

int
  :: String
  -> Int64
int =
  read
