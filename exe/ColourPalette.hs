--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import qualified Data.ByteString.Lazy       as LBS
import qualified Data.ByteString.Lazy.Char8 as C8
import           Data.List
  ( isPrefixOf
  )
import           Data.Maybe
  ( listToMaybe
  )
import           System.Environment
  ( getArgs
  )

import           Data.PCx.Byte.Common
  ( ByteStream (bytes)
  )
import qualified Data.PCx.Byte.Parser       as Parser
import qualified Data.PCx.Vx.BMP.Parse.File as BMP

--------------------------------------------------------------------------------

main
  :: IO ()

--------------------------------------------------------------------------------

main =
  bmppath >>= bmp
  where
    bmp mp =
      case mp of
        Nothing -> putStrLn msg
        Just  p ->
          LBS.readFile p >>= LBS.putStr . LBS.pack . aux . LBS.unpack
      where
        msg = "No BMP file provided. Example of usage: … bmp=\"eq960001.bmp\" …"
    aux bs =
      case Parser.run BMP.fileP bs of
        Right ast -> bytes ast
        Left  err -> c2b err
      where
        c2b = LBS.unpack . C8.pack

--------------------------------------------------------------------------------

-- HELPERS

bmppath
  :: IO (Maybe String)
bmppath =
  listToMaybe . map (drop len) . filter (isPrefixOf pat) <$> getArgs
  where
    pat = "bmp="
    len = length pat
