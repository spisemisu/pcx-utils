--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import qualified Data.ByteString.Lazy           as LBS
import           Data.List
  ( isPrefixOf
  )
import           Data.Maybe
  ( listToMaybe
  )
import           System.Environment
  ( getArgs
  )
import           Text.Show.Pretty
  ( ppShow
  )

import qualified Data.PCx.Byte.Parser           as Parser
import qualified Data.PCx.Vx.PKF.Parse.Pointers as PKF

--------------------------------------------------------------------------------

main
  :: IO ()

--------------------------------------------------------------------------------

main =
  pkfpath >>= pkf
  where
    pkf mp =
      case mp of
        Nothing -> putStrLn msg
        Just  p ->
          LBS.readFile p >>= putStrLn . aux . LBS.unpack
      where
        msg = "No PKF file provided. Example of usage: … pkf=\"EQ022022.PKF\" …"
    aux bs =
      case Parser.run PKF.pointersP bs of
        Right ast -> ppShow ast
        Left  err -> err

--------------------------------------------------------------------------------

-- HELPERS

pkfpath
  :: IO (Maybe String)
pkfpath =
  listToMaybe . map (drop len) . filter (isPrefixOf pat) <$> getArgs
  where
    pat = "pkf="
    len = length pat
