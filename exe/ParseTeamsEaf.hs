--------------------------------------------------------------------------------
--
-- PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import           Data.Char
  ( chr
  )
import           Data.List
  ( isPrefixOf
  )
import           Data.List
  ( sortBy
  )
import           Data.Maybe
  ( fromMaybe
  )
import           Data.Maybe
  ( listToMaybe
  )
import           Data.Time.Calendar
  ( toGregorian
  )
import           Data.Time.Clock
  ( UTCTime (utctDay)
  , getCurrentTime
  )
import           Data.Word
  ( Word16
  )
import           System.Environment
  ( getArgs
  )
import           Text.Show.Pretty
  ( ppShow
  )

import qualified Data.PCF.V6.PKF.Types.Player as PKF.Player
import qualified Data.PCF.V6.PKF.Types.Staff  as PKF.Staff
import qualified Data.PCF.V6.PKF.Types.Team   as PKF.Team
import qualified Data.PCF.V6.PKF.Types.Teams  as PKF.Teams

--------------------------------------------------------------------------------

main
  :: IO ()

--------------------------------------------------------------------------------

main =
  gcy     >>= \ cy ->
  abycons >>= \ by ->
  fbppath >>= fbp (fromMaybe cy (parse <$> by))
  where
    gcy =
      f . toGregorian . utctDay <$> getCurrentTime
      where
        f (y,_,_) = fromIntegral $ y - 16
    fbp y mp =
      case mp of
        Nothing -> putStrLn msg
        Just  p ->
          readFile p >>= \cs -> fbtpath >>= fbt y (players cs)
      where
        msg = "No FBP file provided. Example of usage: … fbt=\"eq022022.fbp\" …"
    fbt y ps mp =
      case mp of
        Nothing -> putStrLn msg
        Just  p ->
          readFile p >>= putStrLn . esc False . ppShow . teams y ps
      where
        msg = "No FBT file provided. Example of usage: … fbt=\"eq022022.fbt\" …"
    -- Unescape Extended ASCII
    esc _ [             ]        = [        ]
    esc b ('\\':'"'  :xs)        = q:esc n xs
      where
        q = if  b then '»' else '«'
        n = not b
    esc b ('\\':x:y:z:xs)
      | '\048' < x && x < '\051' && -- 0 <  x <  3 -> ['\1xx' - '\2xx']
        '\047' < y && y < '\058' && -- 0 <= y <= 9
        '\047' < z && z < '\058' =  -- 0 <= z <= 9
        c:esc b xs
      where
        c =
          chr
            ( read [x] * 100
            + read [y] * 010
            + read [z]
            )
    esc b (         x:xs)        = x:esc b xs


--------------------------------------------------------------------------------

-- HELPERS

abycons
  :: IO (Maybe String)
abycons =
  listToMaybe . map (drop len) . filter (isPrefixOf pat) <$> getArgs
  where
    pat = "aby="
    len = length pat

fbppath
  :: IO (Maybe String)
fbppath =
  listToMaybe . map (drop len) . filter (isPrefixOf pat) <$> getArgs
  where
    pat = "fbp="
    len = length pat

fbtpath
  :: IO (Maybe String)
fbtpath =
  listToMaybe . map (drop len) . filter (isPrefixOf pat) <$> getArgs
  where
    pat = "fbt="
    len = length pat

parse
  :: String
  -> Word16
parse =
  read

players
  :: String
  -> [ ( Word16
       , PKF.Player.Player
       )
     ]
players =
  map par . lines
  where
    par
      :: String
      -> ( Word16
         , PKF.Player.Player
         )
    par = read

teams
  :: Word16
  -> [ ( Word16
       , PKF.Player.Player
       )
     ]
  -> String
  -> PKF.Teams.Teams
teams year ps =
  PKF.Teams.teams . map (add . par) . lines
  where
    par
      :: String
      -> PKF.Team.Team
    par = read
    add (PKF.Team.S c  ) = PKF.Team.S (cor c)
    add (PKF.Team.E c d) = PKF.Team.E (cor c) d
    cor t = t
      { PKF.Team.staff = PKF.Team.staff t ++ pls
      }
      where
        avg rat pac sta agg =
          squish r $ scaleDown $ adjustRating (p, s, a, r) r
          where
            r = fromIntegral rat :: Word
            p = fromIntegral pac :: Word
            s = fromIntegral sta :: Word
            a = fromIntegral agg :: Word
        tid = PKF.Team.tid t
        pls =
          reverse $
          map PKF.Staff.P $
          upd 1 $
          lineup $
          map snd $
          filter ((== tid) . fst) ps
        upd _ [                     ] = []
        upd i ((PKF.Player.S c  ):xs) =
          PKF.Player.S x:upd (i+1) xs
          where
            g =
              if   PKF.Player.position    c == PKF.Player.KEP
              then PKF.Player.goalkeeping c
              else 0x00
            x = c
              { PKF.Player.goalkeeping = g
              , PKF.Player.skill       = fromIntegral sk
              , PKF.Player.pace        = fromIntegral pa
              , PKF.Player.stamina     = fromIntegral st
              , PKF.Player.aggression  = fromIntegral ag
              , PKF.Player.birthday    =
                if    9955 == tid -- Academy
                then (year,01,01)
                else PKF.Player.birthday c
              , PKF.Player.pid         =
                if    9955 == tid -- Academy
                then 19000 + PKF.Player.pid c
                else PKF.Player.pid c
              , PKF.Player.number      =
                if    9955 == tid -- Academy
                then 12
                else
                  if 50 < i then 00 else i
              , PKF.Player.index       =
                if    9955 == tid -- Academy
                then 12
                else
                  if 50 < i then 99 else i
              }
            (pa, st, ag, sk) =
              avg
              (PKF.Player.skill c)
              (PKF.Player.pace c)
              (PKF.Player.stamina c)
              (PKF.Player.aggression c)
        upd i ((PKF.Player.E c d):xs) =
          PKF.Player.E x d:upd (i+1) xs
          where
            g =
              if   PKF.Player.position    c == PKF.Player.KEP
              then PKF.Player.goalkeeping c
              else 0x00
            x = c
              { PKF.Player.goalkeeping = g
              , PKF.Player.skill       = fromIntegral sk
              , PKF.Player.pace        = fromIntegral pa
              , PKF.Player.stamina     = fromIntegral st
              , PKF.Player.aggression  = fromIntegral ag
              , PKF.Player.birthday    =
                if    9955 == tid -- Academy
                then (year,01,01)
                else PKF.Player.birthday c
              , PKF.Player.pid         =
                if    9955 == tid -- Academy
                then 19000 + PKF.Player.pid c
                else PKF.Player.pid c
              , PKF.Player.number      =
                if    9955 == tid -- Academy
                then 12
                else
                  if 50 < i then 00 else i
              , PKF.Player.index       =
                if    9955 == tid -- Academy
                then 12
                else
                  if 50 < i then 99 else i
              }
            (pa, st, ag, sk) =
              avg
              (PKF.Player.skill c)
              (PKF.Player.pace c)
              (PKF.Player.stamina c)
              (PKF.Player.aggression c)

lineup
  :: [PKF.Player.Player]
  -> [PKF.Player.Player]
lineup ps =
  aux 1 ps
  where
    pat xs = any (`elem` xs)
    fpc (PKF.Player.S c  ) = c
    fpc (PKF.Player.E c _) = c
    fsb p1 p2 =
      compare y x -- descending
      where
        x = PKF.Player.skill $ fpc p1
        y = PKF.Player.skill $ fpc p2
    fgk =
      pat
      [ PKF.Player.GK
      ] . PKF.Player.roles . fpc
    frb =
      pat
      [ PKF.Player.RB
      ] . PKF.Player.roles . fpc
    flb =
      pat
      [ PKF.Player.LB
      ] . PKF.Player.roles . fpc
    fcb =
      pat
      [ PKF.Player.SW
      ] . PKF.Player.roles . fpc
    flm =
      pat
      [ PKF.Player.LM
      , PKF.Player.LCM
      , PKF.Player.LW
      ] . PKF.Player.roles . fpc
    fom =
      pat
      [ PKF.Player.RCM
      , PKF.Player.CAM
      , PKF.Player.CDM
      , PKF.Player.CF
      ] . PKF.Player.roles . fpc
    frm =
      pat
      [ PKF.Player.RM
      , PKF.Player.RCM
      , PKF.Player.RW
      ] . PKF.Player.roles . fpc
    frf =
      pat
      [ PKF.Player.ST
      , PKF.Player.CF
      , PKF.Player.RF
      , PKF.Player.RW
      ] . PKF.Player.roles . fpc
    fdm =
      pat
      [ PKF.Player.LCM
      , PKF.Player.CAM
      , PKF.Player.CDM
      , PKF.Player.CF
      ] . PKF.Player.roles . fpc
    flf =
      pat
      [ PKF.Player.ST
      , PKF.Player.CF
      , PKF.Player.LF
      , PKF.Player.LW
      ] . PKF.Player.roles . fpc
    aux
      :: Word
      -> [PKF.Player.Player]
      -> [PKF.Player.Player]
    aux 01 xs =
      (take 1 ys) ++ aux 02 (drop 1 ys ++ zs)
      where
        ys = sortBy fsb $ filter        fgk  xs
        zs =              filter (not . fgk) xs
    aux 02 xs =
      (take 1 ys) ++ aux 03 (drop 1 ys ++ zs)
      where
        ys = sortBy fsb $ filter        frb  xs
        zs =              filter (not . frb) xs
    aux 03 xs =
      (take 1 ys) ++ aux 04 (drop 1 ys ++ zs)
      where
        ys = sortBy fsb $ filter        flb  xs
        zs =              filter (not . flb) xs
    aux 04 xs =
      (take 1 ys) ++ aux 05 (drop 1 ys ++ zs)
      where
        ys = sortBy fsb $ filter        fcb  xs
        zs =              filter (not . fcb) xs
    aux 05 xs =
      (take 1 ys) ++ aux 06 (drop 1 ys ++ zs)
      where
        ys = sortBy fsb $ filter        fcb  xs
        zs =              filter (not . fcb) xs
    aux 06 xs =
      (take 1 ys) ++ aux 07 (drop 1 ys ++ zs)
      where
        ys = sortBy fsb $ filter        flm  xs
        zs =              filter (not . flm) xs
    aux 07 xs =
      (take 1 ys) ++ aux 08 (drop 1 ys ++ zs)
      where
        ys = sortBy fsb $ filter        fom  xs
        zs =              filter (not . fom) xs
    aux 08 xs =
      (take 1 ys) ++ aux 09 (drop 1 ys ++ zs)
      where
        ys = sortBy fsb $ filter        frm  xs
        zs =              filter (not . frm) xs
    aux 09 xs =
      (take 1 ys) ++ aux 10 (drop 1 ys ++ zs)
      where
        ys = sortBy fsb $ filter        frf  xs
        zs =              filter (not . frf) xs
    aux 10 xs =
      (take 1 ys) ++ aux 11 (drop 1 ys ++ zs)
      where
        ys = sortBy fsb $ filter        fdm  xs
        zs =              filter (not . fdm) xs
    aux 11 xs =
      (take 1 ys) ++ aux 12 (drop 1 ys ++ zs)
      where
        ys = sortBy fsb $ filter        flf  xs
        zs =              filter (not . flf) xs
    aux 12 xs =
      (take 1 ys) ++ aux 13 (drop 1 ys ++ zs)
      where
        ys = sortBy fsb $ filter        fgk  xs
        zs =              filter (not . fgk) xs
    aux __ xs =
      sortBy fsb xs

--------------------------------------------------------------------------------

-- 2020-07-17: Scale code provided by @eckankar

type Stats =
  ( Word
  , Word
  , Word
  , Word
  )

scaleStats
  :: Word
  -> Word
  -> Stats
  -> Stats
scaleStats old new (pace, stamina, aggression, skill) =
  ( scale pace
  , scale stamina
  , scale aggression
  , scale skill
  )
  where
    scale v = (v * new) `div` old

maxSkill :: Stats -> Word
maxSkill (pace, stamina, aggression, skill) =
  maximum
  [ pace
  , stamina
  , aggression
  , skill
  ]

gameRating
  :: Stats
  -> Word
gameRating (pace, stamina, aggression, skill) =
  ( pace
  + stamina
  + aggression
  + skill
  ) `div` 4

adjustRating
  :: Stats
  -> Word
  -> Stats
adjustRating stats fifaRating =
  scaleStats actualRating fifaRating stats
    where
      actualRating =
        gameRating stats

scaleDown
  :: Stats
  -> Stats
scaleDown stats =
  if maxVal <= 99
  then stats
  else scaleStats maxVal 99 stats
  where
    maxVal =
      maxSkill stats

invertStats :: Stats -> Stats
invertStats (pace, stamina, aggression, skill) =
  ( 99 - pace
  , 99 - stamina
  , 99 - aggression
  , 99 - skill
  )

squish
  :: Word
  -> Stats
  -> Stats
squish fifaRating stats =
  invertStats squishedInvStats
  where
    invStats =
      invertStats stats
    invRating =
      gameRating invStats
    invFifaRating =
      99 - fifaRating
    squishedInvStats =
      scaleStats invRating invFifaRating invStats
