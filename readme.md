PCx-Utils
=========

`PCx-Utils` are a set of tools to help update old PCx (PCF/PCB) games data.


## A tribute to Michael Robinson

The end of April, we were all a bit sad when we heard that `Michael Robinson`
had passed away after a long battle with (skin) cancer, diagnosed with malignant
melanoma since December 2018.

![1958-07-12 – 2020-04-27. RIP][robinson-rip]

To the rest of the world, he might had been known for being a former Liverpool
striker who managed to win the European Cup (UEFA Champions League) in 1983-84
with `the reds`.

![1984 European Cup Final winner with Liverpool FC][robinson-lfc]

For Spaniards, as myself, he will always be remember for:

* **El día después**: a Spanish football TV-show, formerly shown on `Canal+`, that
  celebrated the _“culture, passion and madness of Spanish football”_.

* **Being on the cover of PC Fútbol**: By using his public figure to give
  awareness of these games, he contributed to make them the most successful
  PC-game in the history of Spain.

![El día después host 1991 — 2005][robinson-edd]

I can say that I saw (almost) all of **El día después** shows and I spent an
insane amount of hours playing the several versions of **PC Fútbol**.

![On the cover of the many PC Fútbol][robinson-pcf]

In the later years, he was mostly known (and awarded) for **Informe Robinson**,
a monthly Spanish sports magazine broadcasted on `#0 (Movistar)` and previously
on `Canal+`.

[robinson-rip]: doc/imgs/robinson-rip-1958-2020.jpg
[robinson-lfc]: doc/imgs/robinson-liverpoolfc-cl.jpg
[robinson-edd]: doc/imgs/robinson-el-dia-despues.jpg
[robinson-pcf]: doc/imgs/robinson-pc-futbol.jpg


## Requirements

In order to use the `PCx-Utils`, your system will need the following applications:

* [**stack**][stackreadme]: is a cross-platform program for build/develop
  Haskell projects.
* [**jq**][jqwebsite]: is like `sed` for JSON data - you can use it to slice and filter and
  map and transform structured data with the same ease that `sed`, `awk`, `grep`
  and friends let you play with text.
* [**imagemagick**][imagemagick]: is used to create, edit, compose, or convert
  `bitmap` images.
* [**curl**][curlwebsite]: is used in command lines or scripts to transfer data.
* [**wget**][wgetwebsite]: is a free software package for retrieving files using
  HTTP, HTTPS, FTP and FTPS the most widely-used Internet protocols.
* **xxd**: is used to create a `hex dump` of a given file or standard input.

> **Note**: If you use `nix`, you just need to type `nix-shell` in the root
> folder of the project to get access to all the tools except for `stack`, which
> needs to be installed separately, see `Tools > Binaries` for more information.

[stackreadme]: https://docs.haskellstack.org/en/stable/README/
[jqwebsite]:   https://stedolan.github.io/jq/
[imagemagick]: https://imagemagick.org/
[curlwebsite]: https://curl.haxx.se/
[wgetwebsite]: https://www.gnu.org/software/wget/

> **Note**: It might be possible to install the tools mentioned above on a
> `Windows` box, but I would highly suggest that you use the `Windows Subsystem
> for Linux (WSL)` component from `Windows 10`.


## Tools

### Binaries

To build the binaries, execute the following script:

```
[nix-shell:~/…/pcx-utils]$ ./build.bash
```

> **Note**: `stack` MUST be installed on the operating system in order to use
> the build script:

```
curl -sSL https://get.haskellstack.org/ | sh
```

or

```
wget -qO- https://get.haskellstack.org/ | sh
```

For more information, visit [The Haskell Tool Stack > How to
install][stackinstall].

[stackinstall]: https://docs.haskellstack.org/en/stable/README/#how-to-install

#### EA FC/FIFA API

* **eaf-v6-parseteams**: is a stand-alone tool that merge generated EA FC/FIFA
  API player and team AST files into a single AST team file.
  - **input**:
    - ABY cons (args): Example of usage: `… aby="2003" …`
    - FBP file (args): Example of usage: `… fbt="002-esp-primera.fbp" …`
    - FBT file (args): Example of usage: `… fbt="002-esp-primera.fbt" …`
  - **output**:
    - TMS file (pipe): Example of usage: `… > 002-esp-primera.tms`

#### PC Fútbol 6.x

* **pcf-v6-bytesteams**: is a stand-alone tool that converts an AST team file to
  DBC files.
  - **input**:
    - TMS file (args): Example of usage: `… tms="eq022022.tms" …`
  - **output**:
    - DBC files (args): Example of usage: `… dbc="eq022022/" …`

* **pcf-v6-parseteams**: is a stand-alone tool that extracts teams from PKF
  files to an AST team file.
  - **input**:
    - PTR file (args): Example of usage: `… ptr="eq022022.ptr" …`
    - PKF file (args): Example of usage: `… pkf="EQ022022.PKF" …`
  - **output**:
    - TMS file (pipe): Example of usage: `… > eq022022.tms`

* **pcf-v6-parsetexts**: is a stand-alone tool that extracts text from PKF files
  to an AST text file.
  - **input**:
    - PTR file (args): Example of usage: `… ptr="textos.ptr" …`
    - PKF file (args): Example of usage: `… pkf="TEXTOS.PKF" …`
  - **output**:
    - TMS file (pipe): Example of usage: `… > textos.tls`

* **pcf-v6-patchbirthday**: is a stand-alone tool that removes the birthday
  constraint (`1900`-`2000`) of the main application file.
  - **input**:
    - EXE file (pipe): Example of usage: `cat "MANAGER.EXE" | …`
  - **output**:
    - EXE file (pipe): Example of usage: `… > manager.exe`

* **pcf-v6-patchcalendar**: is a stand-alone tool that changes the calendar year
  of the main application file.
  - **input**:
    - YTD cons (args): Example of usage: `… ytd="2019" …`
    - EXE file (pipe): Example of usage: `cat "MANAGER.EXE" | …`
  - **output**:
    - EXE file (pipe): Example of usage: `… > manager.exe`

* **pcf-v6-patchnoneu**: is a stand-alone tool that removes signing and aligning
  non-EU players constraints of the main application file.
  - **input**:
    - EXE file (pipe): Example of usage: `cat "MANAGER.EXE" | …`
  - **output**:
    - EXE file (pipe): Example of usage: `… > manager.exe`

#### PC Basket/Fútbol 5.x/6.x

* **pcx-bytesrotation**: is a stand-alone tool that rotates all the bytes to
  match DM text encoding. Notice that using on the rotated bytes file, should
  produce the initial file.
  - **input**:
    - PKF file (args): Example of usage: `… pkf="EQ022022.PKF" …`
  - **output**:
    - ROT file (pipe): Example of usage: `… > eq022022.rot`

* **pcx-bytessample**: is a stand-alone tool that can extract a chunk of bytes
  from a given binary file.
  - **input**:
    - OFF cons (args): Example of usage: `… off="1024" …`
    - LEN cons (args): Example of usage: `… len="8192" …`
  - **output**:
    - BIN file (pipe): Example of usage: `… > sample.bin`

* **pcx-colourpalette**: is a stand-alone tool that ensures that 256
  index-colour BMP files use the DM table of colours.
  - **input**:
    - BMP file (args): Example of usage: `… bmp="eq960001.bmp" …`
  - **output**:
    - BMP file (pipe): Example of usage: `… > eq960001.bmp`

* **pcx-extractimages**: is a stand-alone tool that extracts BMP images from PKF
  files.
  - **input**:
    - PFX cons (args): Example of usage: `… pfx="eq96" …`
    - PAD cons (args): Example of usage: `… pad="0x04" …`
    - PTR file (args): Example of usage: `… ptr="nanoesc.ptr" …`
    - PKF file (args): Example of usage: `… pkf="NANOESC.PKF" …`
  - **output**:
    - BMP files (args): Example of usage: `… bmp="nanoesc/" …`

* **pcx-parsepointers**: is a stand-alone tool that extracts pointers from PKF
  files to an AST pointer file.
  - **input**:
    - PKF file (args): Example of usage: `… pkf="EQ022022.PKF" …`
  - **output**:
    - PTR file (pipe): Example of usage: `… > eq022022.ptr`

### Scripts

#### EA FC/FIFA API

* **eaf-all-leagues.sh**: is a script that transforms data and images from the
  EA FC/FIFA API to DBC and BMP files.
  * **dependencies**:
    - `eaf-gen-extended.sh` 
      - `eaf/meta/esp-primera.json`
      - `eaf/meta/esp-segunda.json`
    - `eaf-gen-standard.sh`
      - `eaf/meta/ita-scudetto.json`
      - `eaf/meta/fra-ligueone.json`
      - `eaf/meta/eng-premier.json`
      - `eaf/meta/deu-budesliga.json`
      - `eaf/meta/prt-primeira.json`
      - `eaf/meta/nld-eredivisie.json`
      - `eaf/meta/tur-superlig.json`
      - `eaf/meta/bel-proleague.json`
      - `eaf/meta/arg-primera.json`
      - `eaf/meta/col-primera.json`
      - `eaf/meta/dnk-superliga.json`
      - `eaf/meta/che-superleague.json`
      - `eaf/meta/swe-allsvenskan.json`
      - `eaf/meta/sct-premiership.json`
      - `eaf/meta/aut-budesliga.json`
      - `eaf/meta/nor-eliteserien.json`
      - `eaf/meta/pol-ekstraklasa.json`
      - `eaf/meta/chl-primera.json`
      - `eaf/meta/rou-ligaone.json`
      - `eaf/meta/irl-premier.json`
      - `eaf/meta/usa-mls.json`
      - `eaf/meta/chn-superleague.json`
      - `eaf/meta/sau-proleague.json`

* **eaf-ast-playext.sh**: is a script that transforms players data from a EA
  FC/FIFA API JSON file to an AST extended player file.
  * **dependencies**:
    - `eaf/json/players.json`
    - `eaf/json/teams.json`
    - `eaf/meta/iso3166.json`
    - `eaf/meta/add3166.json`
    - `eaf/meta/pcf3166.json`
    - `eaf/meta/pcfposi.json`
    - `eaf/meta/pcfrole.json`

* **eaf-ast-playstd.sh**: is a script that transforms players data from a EA
  FC/FIFA API JSON file to an AST standard player file.
  * **dependencies**:
    - `eaf/json/players.json`
    - `eaf/json/teams.json`
    - `eaf/meta/iso3166.json`
    - `eaf/meta/add3166.json`
    - `eaf/meta/pcf3166.json`
    - `eaf/meta/pcfposi.json`
    - `eaf/meta/pcfrole.json`

* **eaf-ast-teamext.sh**: is a script that transforms players data from a EA
  FC/FIFA API JSON file to an AST extended team file.
  * **dependencies**:
    - `eaf/json/teams.json`

* **eaf-ast-teamstd.sh**: is a script that transforms players data from a EA
  FC/FIFA API JSON file to an AST standard team file.
  * **dependencies**:
    - `eaf/json/teams.json`

* **eaf-gen-extended.sh**: is a script that merge generated EA FC/FIFA API
  player and team extended AST files into a single AST team file.
  * **dependencies**:
    - `eaf-ast-teamext.sh`
    - `eaf-ast-playext.sh`
    - `eaf-v6-parseteams`
    - `bin/pcf-v6-bytesteams`
    - `eaf-img-teamext.sh`
    - `eaf-img-players.sh`

* **eaf-gen-standard.sh**: is a script that merge generated EA FC/FIFA API
  player and team standard AST files into a single AST team file.
  * **dependencies**:
    - `eaf-ast-teamstd.sh`
    - `eaf-ast-playstd.sh`
    - `eaf-v6-parseteams`
    - `bin/pcf-v6-bytesteams`
    - `eaf-img-teamstd.sh`
    - `eaf-img-players.sh`

* **eaf-get-imgs.sh**: is a script that retrieve images from the EA FC/FIFA API
  JSON files.
  * **dependencies**:
    - `eaf/json/countries.json`
    - `eaf/json/teams.json`
    - `eaf/json/players.json`

* **eaf-get-json.sh**: is a script that retrieve JSON files from the EA FC/FIFA
  API.

* **eaf-get-iso3166.sh**: is a script that retrieve country codes from a
  ISO-3166 API.

* **eaf-img-palette.sh**: is a script that generates a BMP file containing the
  DM table of colours.
  
![DM table of colours][dm-colourmap]

* **eaf-img-players.sh**: is a script that transforms players images from EA
  FC/FIFA API PNG files to BMP files.
  * **dependencies**:
    - `eaf/json/players.json`
    - `eaf/json/teams.json`
    - `eaf/imgs/players/*.png`
    - `eaf/meta/palette.bmp`

* **eaf-img-stadium.sh**: is a script that generates a black BMP file, of
  dimensions 320x240, containing the DM table of colours to be used as a default
  stadium file.
  * **dependencies**:
    - `eaf/meta/palette.bmp`

* **eaf-img-teamext.sh**: is a script that transforms teams images from EA FC/FIFA
  API PNG files to BMP files.
  * **dependencies**:
    - `eaf/json/players.json`
    - `eaf/json/teams.json`
    - `eaf/imgs/teams/*.png`
    - `eaf/meta/palette.bmp`
    - `eaf/meta/teamkit.bmp`
    - `eaf/meta/stadium.bmp`

* **eaf-img-teamkit.sh**: is a script that generates a black BMP file, of
  dimensions 19x21, containing the DM table of colours to be used as a default
  team-kit file.
  * **dependencies**:
    - `eaf/meta/palette.bmp`

* **eaf-img-teamstd.sh**: is a script that transforms teams images from EA FC/FIFA
  API PNG files to BMP files.
  * **dependencies**:
    - `eaf/json/players.json`
    - `eaf/json/teams.json`
    - `eaf/imgs/teams/*.png`
    - `eaf/meta/palette.bmp`

[dm-colourmap]: doc/imgs/dm-colourmap.png

#### PC Basket 6.x

* **pcb-v60-bytes-rotate.bash**: see `pcx-bytesrotation` above.
  * **dependencies**:
    - `../dat/pcb0060/DBDAT/EQ022022.PKF`

* **pcb-v60-parse-pointers.bash**: see `pcx-parsepointers` above.
  * **dependencies**:
    - `../dat/pcb0060/DBDAT/EQ022022.PKF`

#### PC Fútbol 5.x

* **pcf-v50-bytes-rotate.bash**: see `pcx-bytesrotation` above.
  * **dependencies**:
    - `../dat/pcf0050/DBDAT/EQUIPOS.PKF`

* **pcf-v50-parse-pointers.bash**: see `pcx-parsepointers` above.
  * **dependencies**:
    - `../dat/pcf0050/DBDAT/EQUIPOS.PKF`

#### PC Fútbol 6.x
	
* **pcf-v61-bytes-images.bash**: see `pcx-extractimages` above.
  * **dependencies**:
    - `../dat/pcf0061/DBDAT/BANDERAS.PKF`
    - `../dat/pcf0061/DBDAT/MINIBAND.PKF`
    - `../dat/pcf0060/DBDAT/ESTADIO.PKF`
    - `../dat/pcf0061/DBDAT/MINIESC.PKF`
    - `../dat/pcf0061/DBDAT/NANOESC.PKF`
    - `../dat/pcf0061/DBDAT/RIDIESC.PKF`
    - `../dat/pcf0061/DBDAT/MINIFOTO.PKF`
    - `pcx/pcf/v61/minifoto.ptr`
    - `pcx/pcf/v61/banderas.ptr`
    - `pcx/pcf/v61/miniband.ptr`
    - `pcx/pcf/v60/estadio.ptr`
    - `pcx/pcf/v61/miniesc.ptr`
    - `pcx/pcf/v61/nanoesc.ptr`
    - `pcx/pcf/v61/ridiesc.ptr`
    - `pcx/pcf/v61/minifoto.ptr`

* **pcf-v61-bytes-rotate.bash**: see `pcx-bytesrotation` above.
  * **dependencies**:
    - `../dat/pcf0061/DBDAT/EQ022022.PKF`

* **pcf-v61-bytes-teams.bash**: see `pcf-v6-bytesteams` above.
  * **dependencies**:
    - `pcx/pcf/v61/eq022022.tms`

* **pcf-v61-parse-pointers.bash**: see `pcx-parsepointers` above.
  * **dependencies**:
    - `../dat/pcf0061/DBDAT/EQ022022.PKF`
    - `../dat/pcf0061/DBDAT/BANDERAS.PKF`
    - `../dat/pcf0061/DBDAT/MINIBAND.PKF`
    - `../dat/pcf0060/DBDAT/ESTADIO.PKF`
    - `../dat/pcf0061/DBDAT/MINIESC.PKF`
    - `../dat/pcf0061/DBDAT/NANOESC.PKF`
    - `../dat/pcf0061/DBDAT/RIDIESC.PKF`
    - `../dat/pcf0061/DBDAT/MINIFOTO.PKF`
    - `../dat/pcf0061/DBDAT/TEXTOS.PKF`
    - `../dat/pcf0060/DAT.PKF`
    - `../dat/pcf0060/MUSICAS.PKF`

* **pcf-v61-parse-teams.bash**: see `pcf-v6-parseteams` above.
  * **dependencies**:
    - `pcx/pcf/v61/eq022022.ptr`
    - `pcx/pcf/v61/eq022022.tms`

* **pcf-v61-parse-texts.bash**: see `pcf-v6-parsetexts` above.
  * **dependencies**:
    - `../dat/pcf0061/DBDAT/TEXTOS.PKF`

* **pcf-v61-patch-all.bash**: see `pcf-v6-patchbirthday`, `pcf-v6-patchcalendar`
  and `pcf-v6-patchnoneu` above.
  * **dependencies**:
    - `../dat/pcf0061/MANAGER.EXE`

* **pcf-v61-patch-birthday.bash**: see `pcf-v6-patchbirthday` above.
  * **dependencies**:
    - `../dat/pcf0061/MANAGER.EXE`

* **pcf-v61-patch-calendar.bash**: see `pcf-v6-patchcalendar` above.
  * **dependencies**:
    - `../dat/pcf0061/MANAGER.EXE`

* **pcf-v61-patch-noneu.bash**: see `pcf-v6-patchnoneu` above.
  * **dependencies**:
    - `../dat/pcf0061/MANAGER.EXE`

* **src/pcf-v61-playerstats.hs**: is a script that, from an AST team file,
  prints a list of the 50 best rated players.
  * **dependencies**:
    - `pcx/pcf/v61/eq022022.tms`

```
[nix-shell:~/…/pcx-utils]$ ./src/pcf-v61-playerstats.hs 
|09911|FOR|95|98 93 92 97|96 99 96 97 40 05|RONALDO Luiz Nazario da Lima
|03380|MID|92|88 93 99 87|79 77 79 81 82 21|Roy Maurice KEANE
|01797|MID|92|98 88 87 93|75 91 77 79 50 16|Marc OVERMARS
|00144|FOR|91|93 89 86 96|82 84 83 83 37 26|Predrag MIJATOVIC
|02691|FOR|91|93 91 85 95|90 89 80 88 46 08|George Manneh Ousman WEAH
|08476|DEF|91|93 94 96 80|62 66 60 63 79 17|Stephen (Steve) Norman HOWEY
|07223|FOR|91|93 92 88 92|79 77 72 78 49 07|Andreas ANDERSSON
|19483|FOR|91|95 94 84 90|91 83 83 79 73 05|Ibrahima BAKAYOKO
|02685|DEF|90|94 92 83 91|70 85 82 73 88 08|Paolo MALDINI
|08487|MID|90|90 92 93 87|66 62 63 77 84 17|David BATTY
|02001|MID|90|99 96 79 85|68 78 73 75 64 15|Attilio LOMBARDO
|08577|MID|89|91 85 86 94|94 78 95 91 51 13|Djalma Feitosa Dias, DJALMINHA
|00020|FOR|89|86 87 88 95|85 90 85 81 34 09|RAÚL González Blanco
|20959|FOR|89|88 85 92 90|88 89 80 88 46 08|EDMUNDO Alves de Souza Neto
|04150|FOR|89|90 89 85 92|94 78 75 95 45 08|Gabriel Omar BATISTUTA
|08472|KEP|89|92 87 88 90|19 17 26 12 22 82|Pavel SRNICEK
|08620|MID|89|92 89 80 95|78 92 90 88 40 10|DENÍLSON de Oliveira
|17128|FOR|89|94 86 86 89|01 02 03 04 02 02|Stan COLLYMORE
|04327|MID|88|89 90 78 95|76 87 88 87 42 25|RIVALDO Vítor Borba Ferreira
|04326|MID|88|87 90 78 97|62 90 81 82 50 09|GIOVANNI Silva de Oliveira
|00010|DEF|88|85 91 89 87|79 70 74 80 84 10|Fernando Ruiz HIERRO
|08561|FOR|88|91 87 80 93|84 94 81 80 75 27|SAVIO Bortolini Pimentel
|01793|MID|88|92 88 84 89|86 71 82 80 58 27|FINIDI George
|02813|DEF|88|92 88 85 87|74 87 79 80 82 10|Christian ZIEGE
|03936|MID|88|88 86 84 92|71 79 85 83 50 07|Zinedine ZIDANE
|08569|DEF|88|94 86 87 84|74 87 84 77 84 10|Marcos Evangelista de Moraes, CAFÚ
|03371|KEP|88|90 86 86 91|46 31 39 29 39 91|Peter Boleslaw SCHMEICHEL
|03381|MID|88|94 85 82 89|83 93 85 81 76 08|Ryan Joseph GIGGS
|03385|MID|88|90 85 85 90|86 95 90 88 72 11|David Robert BECKHAM
|08450|MID|88|90 87 84 92|79 86 83 89 67 10|Patrick BERGER
|08479|DEF|88|94 87 86 85|67 65 67 71 80 12|Warren Dean BARTON
|08492|FOR|88|91 85 83 95|99 74 81 95 72 12|Alan SHEARER
|07953|FOR|88|95 75 88 93|85 91 83 85 68 09|Ian Edward WRIGHT
|02003|FOR|88|88 84 96 82|78 74 76 77 70 11|Gianluca VIALLI
|08502|MID|88|89 87 83 91|75 90 88 86 61 13|Mehmet SCHOLL
|20948|MID|88|90 86 88 88|72 95 84 86 50 09|RODRIGO Fabri
|08837|KEP|88|75 89 97 90|19 20 15 23 25 90|José Luis Félix CHILAVERT González
|17476|KEP|88|85 88 85 93|20 19 16 18 15 90|Oscar Eduardo CORDOBA Arce
|00059|MID|87|86 84 81 96|83 71 80 82 42 27|Francisco Javier González Pérez, FRAN
|03149|DEF|87|96 91 80 80|76 77 78 92 83 09|ROBERTO CARLOS da Silva
|00012|MID|87|80 88 85 96|65 79 82 79 70 29|Fernando Carlos REDONDO Neri
|00124|FOR|87|89 84 80 96|80 84 82 82 35 09|Davor SUKER
|07278|MID|87|86 89 85 88|72 86 85 72 70 08|LEONARDO Nascimento de Araujo
|01995|KEP|87|86 85 89 88|27 40 46 34 37 86|Angelo PERUZZI
|03203|FOR|87|91 84 77 95|88 86 80 86 60 15|Giuseppe SIGNORI
|03203|FOR|87|91 84 77 95|88 86 80 86 60 15|Giuseppe SIGNORI
|02516|MID|87|87 86 82 93|78 83 84 79 40 08|Youri DJORKAEFF
|13053|MID|87|87 85 83 92|70 76 75 69 64 20|Jon Dahl TOMASSON
|07947|MID|87|88 84 86 90|83 77 84 82 72 11|David Andrew PLATT
|08504|FOR|87|90 87 82 90|81 88 83 88 81 22|Zickler
[nix-shell:~/…/pcx-utils]$ 
```
	
* **src/pcf-v61-teams.hs**: is a script that, from an AST team file,
  prints a list of each team with their respective unique identifiers and names.
  * **dependencies**:
    - `pcx/pcf/v61/eq022022.tms`

```
[nix-shell:~/…/pcx-utils]$ ./src/pcf-v61-teams.hs 
|0001|F.C. Barcelona
|0002|R.C. Deportivo
|0003|R. Zaragoza
|0004|Real Madrid C.F.
|0005|Athletic Club
|0006|Sevilla F.C.
|0007|Valencia C.F.
|0008|R. Racing
|0009|R. Oviedo
|0010|C.D. Tenerife
|0011|Real Sociedad
|0012|C. At. Madrid
|0013|Albacete B.
|0014|R. Sporting
|0015|R.C. Celta
|0016|C.D. Logroñés
|0017|R. Valladolid
|0018|R.C.D. Espanyol
|0019|R. Betis B.
|0020|S.D. Compostela
|0021|Rayo Vallecano
|0022|U.E. Lleida
|0023|C. At. Osasuna
|0024|C.D. Toledo
|0025|R.C.D. Mallorca
|0026|Real Madrid B
|0027|Hércules C.F.
|0028|Barcelona B
|0029|Mérida C.P.
|0030|S.D. Eibar
|0031|C.D. Badajoz
|0033|Palamós
|0034|Athletic B
|0035|C.D. Leganés
|0036|Villarreal C.F.
|0037|Getafe
|0038|U.D. Salamanca
|0039|C.D. Ourense
|0040|C.F. Extremadura
|0041|Ferrol
|0042|Mensajero
|0043|U.D. Las Palmas
|0044|Pontevedra
|0045|Langreo
|0046|Moscardó
|0047|S.S.de los Reyes
|0048|D. Alavés
|0050|Almería
|0051|Ecija
|0052|C.D. Numancia
|0053|Avilés
|0054|Beasain
|0055|Levante U.D.
|0056|Lugo
|0057|Real Unión
|0058|Izarra
|0059|Lemona
|0060|Palencia
|0061|Gramenet
|0062|Castellón
|0063|Manlleu
|0064|Elche C.F.
|0065|Andorra
|0067|Córdoba
|0068|R. Jaén C.F.
|0069|Talavera
|0070|Yeclano
|0071|Xerez C.D.
|0072|Cádiz
|0075|Amurrio
|0076|Barakaldo
|0077|Bermeo
|0079|Figueres
|0080|Sabadell
|0081|Hospitalet
|0082|Terrassa
|0084|Melilla
|0085|Granada
|0086|Recreativo
|0091|Fuenlabrada
|0092|G.Tarragona
|0093|Pol. Almería
|0095|Manchego
|0096|Cacereño
|0099|Deportivo B
|0100|Mallorca B
|0103|E. As Pontes
|0104|Oviedo B
|0105|Carabanchel
|0106|Sporting B
|0108|Osasuna B
|0109|Cultural L.
|0111|Aurrerá
|0113|Zaragoza B
|0114|Gernika
|0115|Valladolid B
|0117|G. Torrelavega
|0118|Mar Menor
|0119|Gandía
|0121|Murcia
|0122|Gavá
|0123|Espanyol B
|0126|Gáldar
|0128|San Pedro
|0129|Málaga
|0130|Sevilla B
|0131|Betis B
|0132|Guadix
|0133|C. At. Madrid »B«
|0135|Valencia B
|0140|Elgoibar
|0141|Burgos
|0142|Leganés B
|0143|Zamora
|0144|Caudal
|0145|Majadahonda
|0146|Racing B
|0147|Endesa Andorra
|0148|Lorca
|0149|Novelda
|0150|Sóller
|0151|Ontinyent
|0152|Motril
|0153|Plasencia
|0154|Moralo
|0155|Isla Cristina
|0156|Playas Jandía
|0201|Milan
|0202|Juventus
|0203|Sampdoria
|0204|Lazio
|0205|Parma
|0206|Nápoles
|0207|Roma
|0213|Inter
|0215|Fiorentina
|0216|Bari
|0217|Brescia
|0219|Piacenza
|0220|Udinese
|0221|Vicenza
|0222|Atalanta
|0236|Bolonia
|0246|Lecce
|0250|Empoli
|0301|Blackburn R.
|0302|Everton
|0303|Manchester Utd.
|0305|Liverpool
|0306|Leeds Utd
|0307|Newcastle Utd
|0308|Aston Villa
|0309|Arsenal
|0310|Tottenham H
|0311|West Ham
|0312|Chelsea
|0314|Wimbledon
|0315|Sheffield W.
|0316|Coventry
|0317|Southampton
|0319|Derby County
|0320|Leicester
|0322|Bolton W
|0326|Crystal Pal.
|0331|Barnsley
|0366|Wrexham
|0372|Cardiff C.
|0390|Swansea City
|0401|Borussia D.
|0402|Borussia M.
|0403|W. Bremen
|0404|Kaiserslautern
|0406|Bayern M.
|0407|Karlsruher
|0408|Schalke 04
|0409|Hamburgo
|0410|Hansa Rostock
|0411|Munich 1860
|0412|Stuttgart
|0413|Colonia
|0415|B. Leverkusen
|0417|Bochum
|0418|Arminia Biel.
|0419|Duisburgo
|0420|Wolfsburg
|0421|Hertha Berlín
|0501|Nantes
|0502|P.S.G.
|0503|Auxerre
|0504|Lens
|0505|Mónaco
|0506|Lyon
|0507|Estrasburgo
|0508|Girondins
|0509|Montpellier
|0510|Metz
|0512|Guingamp
|0513|Rennes
|0514|Niza
|0515|Le Havre
|0516|Cannes
|0517|Bastia
|0520|Olympique M.
|0522|Châteauroux
|0523|Toulouse
|0601|Oporto
|0602|Sporting Port.
|0603|Benfica
|0604|V. Guimaraes
|0605|Farense
|0606|Boavista
|0607|Belenenses
|0609|SC Braga
|0610|Marítimo
|0612|Salgueiros
|0613|Estrela Amad.
|0614|Leça
|0615|Chaves
|0616|Río Ave
|0617|Vitoria Setub.
|0619|Varzim
|0620|Coimbra
|0621|Campomaiorense
|0701|Estrella Roja
|0703|Partizán
|0704|Vojvodina
|0706|Becej
|0751|Spartak Moscú
|0752|Dinamo Moscú
|0753|Rotor
|0754|A. Vladikavkaz
|0755|Lokomotiv M.
|0756|CSKA Moscú
|0801|Ajax
|0802|Feyenoord
|0803|Roda
|0804|PSV
|0805|Vitesse
|0806|Sparta
|0807|Heerenveen
|0808|NAC
|0809|Groningen
|0810|Twente
|0811|Waalwijk
|0812|Willem II
|0813|Fortuna Sitt.
|0814|De Graafschap
|0815|Utrecht
|0816|Volendam
|0817|NEC
|0820|Maastricht
|0851|Anderlecht
|0852|Brujas
|0853|Standard Lieja
|0854|Lierse
|0856|G. Ekeren
|0862|Harelbeke
|0863|R. Antwerp
|0865|Sint Truidense
|0866|Mouscron
|0901|Hajduk Split
|0902|Croatia Zag.
|0903|Osijek
|0904|Varteks
|0905|Zagreb
|0951|Göteborg
|0953|Orebro
|0954|Malmoe
|0955|AIK Estoc.
|0956|Helsingborgs
|1001|Besiktas
|1002|Trabzonspor
|1003|Galatasaray
|1004|Fenerbahce
|1005|Kocaelispor
|1051|Legia V.
|1052|Katowice
|1053|W.Lodz
|1054|Zaglebie L.
|1055|R.Chorzow
|1057|LKS Lodz
|1058|Amika Wronki
|1059|Lech Poznan
|1060|Gornik Zabrze
|1061|Stomil Olsztyn
|1101|Grasshopper
|1102|Sion
|1104|Neuchatel
|1106|Luzern
|1109|St. Gallen
|1110|Lausanne S.
|1151|C. Salzburgo
|1152|Rapid Viena
|1153|Sturm Graz
|1154|Austria M.
|1155|Tirol
|1156|Casino Graz
|1157|LASK Linz
|1158|SV Ried
|1159|Admira Wacker
|1201|Aalborg
|1202|Copenhague
|1203|Brondby
|1204|Silkeborg
|1205|Aarhus
|1206|Odense
|1209|Vejle BK
|1251|G. Rangers
|1252|Celtic G.
|1253|Motherwell
|1254|Hibernian
|1255|Aberdeen
|1256|Heart of M.
|1258|Kilmarnock
|1259|Dundee U.
|1260|Dunfermline At.
|1261|St. Johnstone
|1301|Panathinaikos
|1302|AEK Atenas
|1303|Olympiakos
|1304|Apollon
|1306|OFI
|1312|PAOK
|1351|Rosenborg
|1352|Molde FK
|1353|Lillestrom
|1354|Viking
|1355|Bodo Glimt
|1356|Brann
|1357|Tromso
|1401|Steaua B.
|1404|D.Bucarest
|1406|R. Bucarest
|1407|Nat.Bucarest
|1452|Sparta Praga
|1453|Slavia Praga
|1454|Sigma Olomouc
|1455|Jablonec N.
|1456|Petra Drnovice
|1457|Boby Brno
|1458|Banik Ostrava
|1501|Lok. Sofia
|1502|Levski Sofia
|1503|Botev Plovdiv
|1504|Slavia Sofia
|1505|CSKA Sofía
|1551|Sl.Bratislava
|1552|Inter Bratislava
|1554|Kosice
|1555|Trnava
|1603|Maribor B.
|1604|Primorje
|1605|Gorica
|1651|Anorthosis
|1652|Apoel Nic.
|1653|Omonia
|1654|AEK Larnaca
|1655|Apollon L.
|1701|Ferencvaros
|1703|Ujpesti Dozsa
|1705|Vasutas
|1707|MTK
|1708|Vasas CV
|1751|S.Donetsk
|1752|Dinamo Kiev
|1753|Chernomorets
|1801|Grevenmacher
|1802|Jeunesse
|1804|Avenir Beggen
|1805|Vaduz
|1851|Derry City
|1852|Dundalk
|1853|Shelbourne
|1854|St. Patrick's At.
|1855|Bohemians
|1856|Sligo Rovers
|1857|Shamrock Rov.
|1858|Univ. C. Dublin
|1859|Cork City
|1901|Linfield
|1902|Crusaders
|1903|Glenavon
|1904|Portadown
|1905|Glentoran
|1906|Cliftonville
|1907|Ards
|1951|KR Reykjavik
|1952|Akranes
|2003|MyPa
|2004|Haka
|2005|Jazz
|2052|Bangor City
|2054|Barry Town
|2055|Newton
|2059|Cwmbran
|2060|Inter Cardiff
|2063|Ebbw Vale
|2064|Llasantffraid
|2101|M.Tel Aviv
|2102|Maccabi Haifa
|2103|Beitar
|2104|H.Tel Aviv
|2105|Hap. Petach
|2106|Hap. Beer Sheva
|2151|Valletta
|2152|Hibernians
|2153|Sliema
|2202|KI
|2203|Gotu
|2251|Zalguiris
|2252|Inkaras Kaunas
|2253|Kareda S.
|2401|Teuta
|2402|P. Tirana
|2502|Dinamo Tbilisi
|2503|Dinamo Batumi
|2553|Constructorul
|2601|Bolívar
|2602|The Strongest
|2603|Oriente Petrol.
|2651|El Nacional
|2652|Emelec
|2653|Barc. Guayaquil
|2701|Dinamo Minsk
|2751|Universitario
|2752|Alianza Lima
|2753|Sporting Cristal
|2800|Flamengo
|2801|Vasco da Gama
|2802|Palmeiras
|2803|Botafogo
|2804|Sao Paulo
|2805|Gremio
|2806|Corinthians
|2807|Santos
|2808|Fluminense
|2809|Cruzeiro
|2900|Peñarol
|2901|Nacional
|2902|Wanderers
|2951|Caracas F.C
|2952|Atlético Zulia
|2953|Minervén
|3000|U. de Chile
|3001|U. Católica
|3002|Colo Colo
|3100|A. de Cali
|3101|N. Medellín
|3102|Millonarios
|3200|O. Asunción
|3201|Cerro Porteño
|3202|Guaraní
|4000|Skonto
|4025|Pyunic
|4050|Sileks
|4075|Mozyr
|4100|Lantana
|4125|Neftchi
|4200|Principat
|8800|Sel. Europa
|8801|Sel. América
|9001|River
|9002|San Lorenzo
|9003|Vélez
|9004|Argentinos Jrs.
|9005|Newell's
|9007|Lanús
|9009|Rosario Central
|9010|Gimnasia (LP)
|9011|Independiente
|9012|Racing
|9013|Boca
|9014|Huracán
|9015|Platense
|9016|Gimnasia (J)
|9017|Ferro
|9018|Dep. Español
|9019|Colón
|9020|Estudiantes (LP)
|9029|Gimnasia y Tiro
|9030|Unión
|9900|Estrellas ESPAÑOLAS
|9950|Jugadores Libres
|9955|Juveniles Españoles
[nix-shell:~/…/pcx-utils]$ 
```


## Usage flows

In this section, we will show the two main flows in order to update data.

### EQ022022.PKF → PTR and TMS files → BMP and DBC files

1. Ensure that there exists the following two folders:
   - `../dat/pcf0060/DBDAT/`
   - `../dat/pcf0061/DBDAT/`

with the following content from **PC Fútbol 6.x**:

```
[nix-shell:~/…/pcx-utils]$ tree ../dat/pcf0060
../dat/pcf0060/
├── DAT.PKF
├── DBDAT
│   └── ESTADIO.PKF
└── MUSICAS.PKF

1 directory, 4 files
[nix-shell:~/…/pcx-utils]$ tree ../dat/pcf0061
../dat/pcf0061
├── DBDAT
│   ├── BANDERAS.PKF
│   ├── EQ022022.PKF
│   ├── MINIBAND.PKF
│   ├── MINIENTR.PKF
│   ├── MINIESC.PKF
│   ├── MINIFOTO.PKF
│   ├── NANOESC.PKF
│   ├── RIDIESC.PKF
│   └── TEXTOS.PKF
├── IMG.PKF
├── MANAGER.EXE
└── RECURSOS.PKF
1 directory, 12 files
```

2. Parse the pointers:

```
[nix-shell:~/…/pcx-utils]$ ./pcf-v61-parse-pointers.bash 
# Parse pointers
## Teams:
* ../dat/pcf0061/DBDAT/EQ022022.PKF
* pcx/pcf/v61/eq022022.ptr
## Images:
### Flags:
* ../dat/pcf0061/DBDAT/BANDERAS.PKF
* pcx/pcf/v61/banderas.ptr
* ../dat/pcf0061/DBDAT/MINIBAND.PKF
* pcx/pcf/v61/miniband.ptr
### Stadiums:
* ../dat/pcf0060/DBDAT/ESTADIO.PKF
* pcx/pcf/v60/estadio.ptr
### Logos:
* ../dat/pcf0061/DBDAT/MINIESC.PKF
* pcx/pcf/v61/miniesc.ptr
* ../dat/pcf0061/DBDAT/NANOESC.PKF
* pcx/pcf/v61/nanoesc.ptr
* ../dat/pcf0061/DBDAT/RIDIESC.PKF
* pcx/pcf/v61/ridiesc.ptr
### Players:
* ../dat/pcf0061/DBDAT/MINIFOTO.PKF
* pcx/pcf/v61/minifoto.ptr
## Texts:
* ../dat/pcf0061/DBDAT/TEXTOS.PKF
* pcx/pcf/v61/textos.ptr
## Other:
* ../dat/pcf0060/DAT.PKF
* pcx/pcf/v60/dat.ptr
* ../dat/pcf0060/MUSICAS.PKF
* pcx/pcf/v60/musicas.ptr
[nix-shell:~/…/pcx-utils]$ 
```

3. Parse the teams:

```
[nix-shell:~/…/pcx-utils]$ ./pcf-v61-parse-teams.bash 
# Parse teams:
* ../dat/pcf0061/DBDAT/EQ022022.PKF
* pcx/pcf/v61/eq022022.tms
[nix-shell:~/…/pcx-utils]$ 
```

4. Edit the teams AST file with a text editor:
* From:
```
Teams
  [ E C { tid = 1
        , tunknown00 = 1964
        , name = "F.C. Barcelona"
        , stadium = "Camp Nou"
        , country = ESP
        , tunknown01 = 41
        , fullname = "Fútbol Club Barcelona"
        , capacity = 108428
        , standing = 0
        , width = 72
        , length = 107
        , founded = 1899
…
  ]
```
* To:
```
Teams
  [ E C { tid = 1
        , tunknown00 = 1964
        , name = "F.C. Barcelona"
        , stadium = "Camp Nou"
        , country = ESP
        , tunknown01 = 41
        , fullname = "Fútbol Club Barcelona"
        , capacity = 99354
        , standing = 0
        , width = 68
        , length = 105
        , founded = 1899
…
  ]
```

5. Convert the AST team file to bytes and store as DBC files:

```
[nix-shell:~/…/pcx-utils]$ ./pcf-v61-bytes-teams.bash 
# Bytes teams:
* pcx/pcf/v61/eq022022.tms
* pcx/pcf/v61/eq022022/
[nix-shell:~/…/pcx-utils]$ 
```

6. Extract images

```
[nix-shell:~/…/pcx-utils]$ ./pcf-v61-bytes-images.bash 
# Extract images
## Flags:
* ../dat/pcf0061/DBDAT/BANDERAS.PKF
* pcx/pcf/v61/banderas/
* ../dat/pcf0061/DBDAT/MINIBAND.PKF
* pcx/pcf/v61/miniband/
## Teams:
* ../dat/pcf0060/DBDAT/ESTADIO.PKF
* pcx/pcf/v60/estadio/
* ../dat/pcf0061/DBDAT/MINIESC.PKF
* pcx/pcf/v61/miniesc/
* ../dat/pcf0061/DBDAT/NANOESC.PKF
* pcx/pcf/v61/nanoesc/
* ../dat/pcf0061/DBDAT/RIDIESC.PKF
* pcx/pcf/v61/ridiesc/
## Players:
* ../dat/pcf0061/DBDAT/MINIFOTO.PKF
* pcx/pcf/v61/minifoto/
[nix-shell:~/…/pcx-utils]$ 
```

7. Edit the teams and players images with a tool that preserves the 256 DM
   index-table of colours, as for example [GIMP][gimpwebsite].

8. Copy DBC and BMP files to the games DBDAT folder:

```
[nix-shell:~/.wine/drive_c/FUTBOL6]$ cp -R ~/…/pcx-utils/pcx/pcf/v61/eq022022/ DBDAT/
[nix-shell:~/.wine/drive_c/FUTBOL6]$ cp -R ~/…/pcx-utils/pcx/pcf/v61/banderas/ DBDAT/
[nix-shell:~/.wine/drive_c/FUTBOL6]$ cp -R ~/…/pcx-utils/pcx/pcf/v61/miniband/ DBDAT/
[nix-shell:~/.wine/drive_c/FUTBOL6]$ cp -R ~/…/pcx-utils/pcx/pcf/v60/estadio/  DBDAT/
[nix-shell:~/.wine/drive_c/FUTBOL6]$ cp -R ~/…/pcx-utils/pcx/pcf/v61/miniesc/  DBDAT/
[nix-shell:~/.wine/drive_c/FUTBOL6]$ cp -R ~/…/pcx-utils/pcx/pcf/v61/minifoto/ DBDAT/
[nix-shell:~/.wine/drive_c/FUTBOL6]$ cp -R ~/…/pcx-utils/pcx/pcf/v61/nanoesc/  DBDAT/
[nix-shell:~/.wine/drive_c/FUTBOL6]$ cp -R ~/…/pcx-utils/pcx/pcf/v61/ridiesc/  DBDAT/
```

[gimpwebsite]: https://www.gimp.org/

### EA FC/FIFA API → PTR and TMS files → BMP and DBC files

1. Retrieve the JSON data from the EA FC/FIFA API (+200 JSON files)

```
[nix-shell:~/…/pcx-utils]$ ./eaf-get-json.sh      # TODO:
[nix-shell:~/…/pcx-utils]$ ./eaf-get-countries.sh # TODO:
[nix-shell:~/…/pcx-utils]$ ./eaf-get-teams.sh     # TODO:
[nix-shell:~/…/pcx-utils]$ ./eaf-get-players.sh   # TODO:

…

[nix-shell:~/…/pcx-utils]$ 
```

2. Retrieve the PNG images from the EA FC/FIFA API (+16.000 image files)

```
[nix-shell:~/…/pcx-utils]$ ./eaf-get-imgs.sh # TODO:

…

[nix-shell:~/…/pcx-utils]$ 
```

3. Transforms data and images from the EA FC/FIFA API to DBC and BMP files. It
   takes around half an hour.

```
[nix-shell:~/…/pcx-utils]$ ./eaf-all-leagues.sh
# Extended

## Primera (Spain)
### Extracting from JSON
#### Teams
* eaf/temp/esp-primera.fbt
#### Players
* eaf/temp/esp-primera.fbp
#### Merge
mkdir: created directory 'eaf/data/'
* eaf/data/esp-primera.tms
#### Convert to DBCs
mkdir: created directory 'eaf/data/eq022022'
* eaf/data/eq022022/
### Transforming images
#### Teams images
mkdir: created directory 'eaf/data/estadio'
mkdir: created directory 'eaf/data/miniesc'
mkdir: created directory 'eaf/data/nanoesc'
mkdir: created directory 'eaf/data/ridiesc'
* eaf/temp/esp-primera.ftb
#### Players images
mkdir: created directory 'eaf/data/minifoto'
* eaf/temp/esp-primera.fpb

## Segunda (Spain)
### Extracting from JSON
#### Teams
* eaf/temp/esp-segunda.fbt
#### Players
* eaf/temp/esp-segunda.fbp
#### Merge
* eaf/data/esp-segunda.tms
#### Convert to DBCs
* eaf/data/eq022022/
### Transforming images
#### Teams images
* eaf/temp/esp-segunda.ftb
#### Players images
* eaf/temp/esp-segunda.fpb

# Standard

## Scudetto (Italy)
### Extracting from JSON
#### Teams
* eaf/temp/ita-scudetto.fbt
#### Players
* eaf/temp/ita-scudetto.fbp
#### Merge
* eaf/data/ita-scudetto.tms
#### Convert to DBCs
* eaf/data/eq022022/
### Transforming images
#### Teams images
* eaf/temp/ita-scudetto.ftb
#### Players images
* eaf/temp/ita-scudetto.fpb

## Ligue One (France)
### Extracting from JSON
#### Teams
* eaf/temp/fra-ligueone.fbt
#### Players
* eaf/temp/fra-ligueone.fbp
#### Merge
* eaf/data/fra-ligueone.tms
#### Convert to DBCs
* eaf/data/eq022022/
### Transforming images
#### Teams images
* eaf/temp/fra-ligueone.ftb
#### Players images
* eaf/temp/fra-ligueone.fpb

## Premier (England/Wales)
### Extracting from JSON
#### Teams
* eaf/temp/eng-premier.fbt
#### Players
* eaf/temp/eng-premier.fbp
#### Merge
* eaf/data/eng-premier.tms
#### Convert to DBCs
* eaf/data/eq022022/
### Transforming images
#### Teams images
* eaf/temp/eng-premier.ftb
#### Players images
* eaf/temp/eng-premier.fpb

## Bundesliga (Germany)
### Extracting from JSON
#### Teams
* eaf/temp/deu-budesliga.fbt
#### Players
* eaf/temp/deu-budesliga.fbp
#### Merge
* eaf/data/deu-budesliga.tms
#### Convert to DBCs
* eaf/data/eq022022/
### Transforming images
#### Teams images
* eaf/temp/deu-budesliga.ftb
#### Players images
* eaf/temp/deu-budesliga.fpb

## Primeira (Portugal)
### Extracting from JSON
#### Teams
* eaf/temp/prt-primeira.fbt
#### Players
* eaf/temp/prt-primeira.fbp
#### Merge
* eaf/data/prt-primeira.tms
#### Convert to DBCs
* eaf/data/eq022022/
### Transforming images
#### Teams images
* eaf/temp/prt-primeira.ftb
#### Players images
* eaf/temp/prt-primeira.fpb

## Eredivisie (The Netherlands)
### Extracting from JSON
#### Teams
* eaf/temp/nld-eredivisie.fbt
#### Players
* eaf/temp/nld-eredivisie.fbp
#### Merge
* eaf/data/nld-eredivisie.tms
#### Convert to DBCs
* eaf/data/eq022022/
### Transforming images
#### Teams images
* eaf/temp/nld-eredivisie.ftb
#### Players images
* eaf/temp/nld-eredivisie.fpb

## Süper Lig (Turkey)
### Extracting from JSON
#### Teams
* eaf/temp/tur-superlig.fbt
#### Players
* eaf/temp/tur-superlig.fbp
#### Merge
* eaf/data/tur-superlig.tms
#### Convert to DBCs
* eaf/data/eq022022/
### Transforming images
#### Teams images
* eaf/temp/tur-superlig.ftb
#### Players images
* eaf/temp/tur-superlig.fpb

## First Division A (Belgium)
### Extracting from JSON
#### Teams
* eaf/temp/bel-proleague.fbt
#### Players
* eaf/temp/bel-proleague.fbp
#### Merge
* eaf/data/bel-proleague.tms
#### Convert to DBCs
* eaf/data/eq022022/
### Transforming images
#### Teams images
* eaf/temp/bel-proleague.ftb
#### Players images
* eaf/temp/bel-proleague.fpb

## Primera (Argentina)
### Extracting from JSON
#### Teams
* eaf/temp/arg-primera.fbt
#### Players
* eaf/temp/arg-primera.fbp
#### Merge
* eaf/data/arg-primera.tms
#### Convert to DBCs
* eaf/data/eq022022/
### Transforming images
#### Teams images
* eaf/temp/arg-primera.ftb
#### Players images
* eaf/temp/arg-primera.fpb

## Superliga (Denmark)
### Extracting from JSON
#### Teams
* eaf/temp/dnk-superliga.fbt
#### Players
* eaf/temp/dnk-superliga.fbp
#### Merge
* eaf/data/dnk-superliga.tms
#### Convert to DBCs
* eaf/data/eq022022/
### Transforming images
#### Teams images
* eaf/temp/dnk-superliga.ftb
#### Players images
* eaf/temp/dnk-superliga.fpb

## Super League (Switzerland)
### Extracting from JSON
#### Teams
* eaf/temp/che-superleague.fbt
#### Players
* eaf/temp/che-superleague.fbp
#### Merge
* eaf/data/che-superleague.tms
#### Convert to DBCs
* eaf/data/eq022022/
### Transforming images
#### Teams images
* eaf/temp/che-superleague.ftb
#### Players images
* eaf/temp/che-superleague.fpb

## Allsvenskan (Sweden)
### Extracting from JSON
#### Teams
* eaf/temp/swe-allsvenskan.fbt
#### Players
* eaf/temp/swe-allsvenskan.fbp
#### Merge
* eaf/data/swe-allsvenskan.tms
#### Convert to DBCs
* eaf/data/eq022022/
### Transforming images
#### Teams images
* eaf/temp/swe-allsvenskan.ftb
#### Players images
* eaf/temp/swe-allsvenskan.fpb

## Premiership (Scotland)
### Extracting from JSON
#### Teams
* eaf/temp/sct-premiership.fbt
#### Players
* eaf/temp/sct-premiership.fbp
#### Merge
* eaf/data/sct-premiership.tms
#### Convert to DBCs
* eaf/data/eq022022/
### Transforming images
#### Teams images
* eaf/temp/sct-premiership.ftb
#### Players images
* eaf/temp/sct-premiership.fpb

## Bundesliga (Austria)
### Extracting from JSON
#### Teams
* eaf/temp/aut-budesliga.fbt
#### Players
* eaf/temp/aut-budesliga.fbp
#### Merge
* eaf/data/aut-budesliga.tms
#### Convert to DBCs
* eaf/data/eq022022/
### Transforming images
#### Teams images
* eaf/temp/aut-budesliga.ftb
#### Players images
* eaf/temp/aut-budesliga.fpb

## Ekstraklasa (Poland)
### Extracting from JSON
#### Teams
* eaf/temp/pol-ekstraklasa.fbt
#### Players
* eaf/temp/pol-ekstraklasa.fbp
#### Merge
* eaf/data/pol-ekstraklasa.tms
#### Convert to DBCs
* eaf/data/eq022022/
### Transforming images
#### Teams images
* eaf/temp/pol-ekstraklasa.ftb
#### Players images
* eaf/temp/pol-ekstraklasa.fpb

## Premier Division (Ireland)
### Extracting from JSON
#### Teams
* eaf/temp/irl-premier.fbt
#### Players
* eaf/temp/irl-premier.fbp
#### Merge
* eaf/data/irl-premier.tms
#### Convert to DBCs
* eaf/data/eq022022/
### Transforming images
#### Teams images
* eaf/temp/irl-premier.ftb
#### Players images
* eaf/temp/irl-premier.fpb

## MLS (United States of America)
### Extracting from JSON
#### Teams
* eaf/temp/usa-mls.fbt
#### Players
* eaf/temp/usa-mls.fbp
#### Merge
* eaf/data/usa-mls.tms
#### Convert to DBCs
* eaf/data/eq022022/
### Transforming images
#### Teams images
* eaf/temp/usa-mls.ftb
#### Players images
* eaf/temp/usa-mls.fpb

## Super League (China)
### Extracting from JSON
#### Teams
* eaf/temp/chn-superleague.fbt
#### Players
* eaf/temp/chn-superleague.fbp
#### Merge
* eaf/data/chn-superleague.tms
#### Convert to DBCs
* eaf/data/eq022022/
### Transforming images
#### Teams images
* eaf/temp/chn-superleague.ftb
#### Players images
* eaf/temp/chn-superleague.fpb

## Pro League (Saudi Arabia)
### Extracting from JSON
#### Teams
* eaf/temp/sau-proleague.fbt
#### Players
* eaf/temp/sau-proleague.fbp
#### Merge
* eaf/data/sau-proleague.tms
#### Convert to DBCs
* eaf/data/eq022022/
### Transforming images
#### Teams images
* eaf/temp/sau-proleague.ftb
#### Players images
* eaf/temp/sau-proleague.fpb

[nix-shell:~/…/pcx-utils]$ 
```

4. Copy DBC and BMP files to the games DBDAT folder:

```
[nix-shell:~/.wine/drive_c/FUTBOL6]$ cp -R ~/…/pcx-utils/eaf/data/eq022022/ DBDAT/
[nix-shell:~/.wine/drive_c/FUTBOL6]$ cp -R ~/…/pcx-utils/eaf/data/estadio/  DBDAT/
[nix-shell:~/.wine/drive_c/FUTBOL6]$ cp -R ~/…/pcx-utils/eaf/data/miniesc/  DBDAT/
[nix-shell:~/.wine/drive_c/FUTBOL6]$ cp -R ~/…/pcx-utils/eaf/data/minifoto/ DBDAT/
[nix-shell:~/.wine/drive_c/FUTBOL6]$ cp -R ~/…/pcx-utils/eaf/data/nanoesc/  DBDAT/
[nix-shell:~/.wine/drive_c/FUTBOL6]$ cp -R ~/…/pcx-utils/eaf/data/ridiesc/  DBDAT/
```

5. (**optional**) Patch the calendar from 1997/1998 to 2019/2020 and remove
   birthday constraint 1900/2000:

```
[nix-shell:~/…/pcx-utils]$ ./pcf-v61-patch-all.bash 
# Patch calendar years (1997-1998):
# Patch birthday years (1900-2000):
# Patch unlimited non-EU players:
* ../dat/pcf0061/MANAGER.EXE
* pcx/pcf/v61/manager.exe
000032b0: 4424 188b 11ff 928c 0000 0085 c074 498b  D$........ | 000032b0: 4424 188b 11ff 928c 0000 0085 c0eb 498b  D$........
00007bd0: 6800 cd07 7e0d 8b4d e868 cb03 0000 e85d  h...~..M.h | 00007bd0: 6800 e707 7e0d 8b4d e868 cb03 0000 e85d  h...~..M.h
00008120: 088b 348d b82e 6800 66c7 055c 4768 00cd  ..4...h.f. | 00008120: 088b 348d b82e 6800 66c7 055c 4768 00e7  ..4...h.f.
00008a30: 0866 c705 5c47 6800 cd07 8d45 e8b9 5c47  .f..\Gh... | 00008a30: 0866 c705 5c47 6800 e707 8d45 e8b9 5c47  .f..\Gh...
00008af0: c705 5c47 6800 cd07 e8e3 ef0a 0083 c404  ..\Gh..... | 00008af0: c705 5c47 6800 e707 e8e3 ef0a 0083 c404  ..\Gh.....
0000df30: 0000 5355 8bce e8e5 0900 0084 c00f 8459  ..SU...... | 0000df30: 0000 5355 8bce e8e5 0900 0084 c090 9090  ..SU......
0000df40: 0100 0033 c933 c08a 4b20 8a45 2088 4d20  ...3.3..K  | 0000df40: 9090 9033 c933 c08a 4b20 8a45 2088 4d20  ...3.3..K 
0000ea40: 1800 84db 755d 8a44 2418 84c0 7433 8b0d  ....u].D$. | 0000ea40: 1800 84db eb5d 8a44 2418 84c0 7433 8b0d  .....].D$.
000511f0: 0000 578b 01ff 9094 0000 0085 c074 548b  ..W....... | 000511f0: 0000 578b 01ff 9094 0000 0085 c0eb 548b  ..W.......
0006c2f0: 6800 8bf8 8b01 ff50 703b f876 2a8b 0d00  h......Pp; | 0006c2f0: 6800 8bf8 8b01 ff50 703b f8eb 2a8b 0d00  h......Pp;
000767d0: 4721 8b11 50ff 928c 0000 0085 c074 548b  G!..P..... | 000767d0: 4721 8b11 50ff 928c 0000 0085 c0eb 548b  G!..P.....
0007f700: 0000 85c0 7446 8b8f 8004 0000 e8ef 6802  ....tF.... | 0007f700: 0000 85c0 eb46 8b8f 8004 0000 e8ef 6802  .....F....
000895d0: c966 813d 5c47 6800 cd07 8944 2468 0f9f  .f.=\Gh... | 000895d0: c966 813d 5c47 6800 e707 8944 2468 0f9f  .f.=\Gh...
000a2ea0: ff92 8c00 0000 85c0 7404 ff44 2410 8a4c  ........t. | 000a2ea0: ff92 8c00 0000 85c0 eb04 ff44 2410 8a4c  ..........
000a66b0: 4ce8 6a81 0000 6681 3d5c 4768 00cd 077e  L.j...f.=\ | 000a66b0: 4ce8 6a81 0000 6681 3d5c 4768 00e7 077e  L.j...f.=\
000a6760: 5c47 6800 663d cd07 7e62 663d ce07 0f8e  \Gh.f=..~b | 000a6760: 5c47 6800 663d e707 7e62 663d e807 0f8e  \Gh.f=..~b
000a6d80: cd07 754f 83be 1802 0000 0175 4668 4904  ..uO...... | 000a6d80: e707 754f 83be 1802 0000 0175 4668 4904  ..uO......
000ae090: 4d55 eb16 6681 3d5c 4768 00cd 07bd 3300  MU..f.=\Gh | 000ae090: 4d55 eb16 6681 3d5c 4768 00e7 07bd 3300  MU..f.=\Gh
000ae850: 0000 6681 3d5c 4768 00cd 0775 22dd 85e4  ..f.=\Gh.. | 000ae850: 0000 6681 3d5c 4768 00e7 0775 22dd 85e4  ..f.=\Gh..
000b17a0: cd07 0f8e 7e02 0000 688a 0400 008d 4c24  ....~...h. | 000b17a0: e707 0f8e 7e02 0000 688a 0400 008d 4c24  ....~...h.
000b2470: fd6c 0700 0076 0881 fdd0 0700 0072 1f6a  .l...v.... | 000b2470: fd6c 0700 0076 0881 fdff ff00 0072 1f6a  .l...v....
000b4e70: b8cd 0700 002b c1c3 81c1 e400 0000 51b9  .....+.... | 000b4e70: b8e7 0700 002b c1c3 81c1 e400 0000 51b9  .....+....
000b7370: 0f84 3801 0000 6681 3d5c 4768 00cd 070f  ..8...f.=\ | 000b7370: 0f84 3801 0000 6681 3d5c 4768 00e7 070f  ..8...f.=\
000c1180: 0508 4368 00cd 07c3 9090 9090 9090 9090  ..Ch...... | 000c1180: 0508 4368 00e7 07c3 9090 9090 9090 9090  ..Ch......
000cf6e0: b108 b8cd 0700 00c6 0572 4368 000e 880d  .........r | 000cf6e0: b108 b8e7 0700 00c6 0572 4368 000e 880d  .........r
000d1ee0: 05a0 4368 00cd 07c3 9090 9090 9090 9090  ..Ch...... | 000d1ee0: 05a0 4368 00e7 07c3 9090 9090 9090 9090  ..Ch......
000d1f10: 0594 4368 00cd 07c3 9090 9090 9090 9090  ..Ch...... | 000d1f10: 0594 4368 00e7 07c3 9090 9090 9090 9090  ..Ch......
000d1f40: 05a4 4368 00cd 07c3 9090 9090 9090 9090  ..Ch...... | 000d1f40: 05a4 4368 00e7 07c3 9090 9090 9090 9090  ..Ch......
000d1f70: 0598 4368 00cd 07c3 9090 9090 9090 9090  ..Ch...... | 000d1f70: 0598 4368 00e7 07c3 9090 9090 9090 9090  ..Ch......
000d1fa0: 059c 4368 00cd 07c3 9090 9090 9090 9090  ..Ch...... | 000d1fa0: 059c 4368 00e7 07c3 9090 9090 9090 9090  ..Ch......
000d1fd0: 0590 4368 00ce 07c3 9090 9090 9090 9090  ..Ch...... | 000d1fd0: 0590 4368 00e8 07c3 9090 9090 9090 9090  ..Ch......
000d2000: 05b4 4368 00ce 07c3 9090 9090 9090 9090  ..Ch...... | 000d2000: 05b4 4368 00e8 07c3 9090 9090 9090 9090  ..Ch......
000d2030: 0584 4368 00ce 07c3 9090 9090 9090 9090  ..Ch...... | 000d2030: 0584 4368 00e8 07c3 9090 9090 9090 9090  ..Ch......
000d2060: 05b0 4368 00ce 07c3 9090 9090 9090 9090  ..Ch...... | 000d2060: 05b0 4368 00e8 07c3 9090 9090 9090 9090  ..Ch......
000d2090: 058c 4368 00ce 07c3 9090 9090 9090 9090  ..Ch...... | 000d2090: 058c 4368 00e8 07c3 9090 9090 9090 9090  ..Ch......
000d20c0: 05ac 4368 00ce 07c3 9090 9090 9090 9090  ..Ch...... | 000d20c0: 05ac 4368 00e8 07c3 9090 9090 9090 9090  ..Ch......
000d20f0: 0588 4368 00ce 07c3 9090 9090 9090 9090  ..Ch...... | 000d20f0: 0588 4368 00e8 07c3 9090 9090 9090 9090  ..Ch......
000d2120: 05a8 4368 00ce 07c3 9090 9090 9090 9090  ..Ch...... | 000d2120: 05a8 4368 00e8 07c3 9090 9090 9090 9090  ..Ch......
000d7840: 5356 b30e bece 0700 0088 1dc2 4368 00b1  SV........ | 000d7840: 5356 b30e bee8 0700 0088 1dc2 4368 00b1  SV........
000d7850: 0b88 1df6 4368 00b8 cd07 0000 b209 bb01  ....Ch.... | 000d7850: 0b88 1df6 4368 00b8 e707 0000 b209 bb01  ....Ch....
000db310: 84db 0700 008b 5d08 f643 0c01 0f84 ce07  ......]..C | 000db310: 84db 0700 008b 5d08 f643 0c01 0f84 e807  ......]..C
000dd870: 5356 b8cd 0700 00b1 09b2 15bb 0b00 0000  SV........ | 000dd870: 5356 b8e7 0700 00b1 09b2 15bb 0b00 0000  SV........
000dd880: bece 0700 00c6 0552 4468 001f c605 5344  .......RDh | 000dd880: bee8 0700 00c6 0552 4468 001f c605 5344  .......RDh
000e4660: 5356 b315 bece 0700 0088 1d06 4568 00b1  SV........ | 000e4660: 5356 b315 bee8 0700 0088 1d06 4568 00b1  SV........
000e4670: 0b88 1d3a 4568 00b8 cd07 0000 b209 bb01  ...:Eh.... | 000e4670: 0b88 1d3a 4568 00b8 e707 0000 b209 bb01  ...:Eh....
000ea0b0: 0594 4568 00cd 07c3 9090 9090 9090 9090  ..Eh...... | 000ea0b0: 0594 4568 00e7 07c3 9090 9090 9090 9090  ..Eh......
000ea0e0: 0590 4568 00cd 07c3 9090 9090 9090 9090  ..Eh...... | 000ea0e0: 0590 4568 00e7 07c3 9090 9090 9090 9090  ..Eh......
000fa940: 05c0 4568 00cd 07c3 9090 9090 9090 9090  ..Eh...... | 000fa940: 05c0 4568 00e7 07c3 9090 9090 9090 9090  ..Eh......
000fa970: 05b8 4568 00cd 07c3 9090 9090 9090 9090  ..Eh...... | 000fa970: 05b8 4568 00e7 07c3 9090 9090 9090 9090  ..Eh......
000fa9a0: 05bc 4568 00cd 07c3 9090 9090 9090 9090  ..Eh...... | 000fa9a0: 05bc 4568 00e7 07c3 9090 9090 9090 9090  ..Eh......
000fa9d0: 05a8 4568 00cd 07c3 9090 9090 9090 9090  ..Eh...... | 000fa9d0: 05a8 4568 00e7 07c3 9090 9090 9090 9090  ..Eh......
000fa9f0: b8cd 0700 00b1 0ab2 0bc6 05ca 4568 0011  .......... | 000fa9f0: b8e7 0700 00b1 0ab2 0bc6 05ca 4568 0011  ..........
000faa90: 05ac 4568 00ce 07c3 9090 9090 9090 9090  ..Eh...... | 000faa90: 05ac 4568 00e8 07c3 9090 9090 9090 9090  ..Eh......
000faac0: 05e0 4568 00ce 07c3 9090 9090 9090 9090  ..Eh...... | 000faac0: 05e0 4568 00e8 07c3 9090 9090 9090 9090  ..Eh......
000faaf0: 05e4 4568 00ce 07c3 9090 9090 9090 9090  ..Eh...... | 000faaf0: 05e4 4568 00e8 07c3 9090 9090 9090 9090  ..Eh......
000fab20: 05b4 4568 00ce 07c3 9090 9090 9090 9090  ..Eh...... | 000fab20: 05b4 4568 00e8 07c3 9090 9090 9090 9090  ..Eh......
000fab50: 05b0 4568 00ce 07c3 9090 9090 9090 9090  ..Eh...... | 000fab50: 05b0 4568 00e8 07c3 9090 9090 9090 9090  ..Eh......
00100950: 05f8 4568 00cd 07c3 9090 9090 9090 9090  ..Eh...... | 00100950: 05f8 4568 00e7 07c3 9090 9090 9090 9090  ..Eh......
00100980: 05e8 4568 00cd 07c3 9090 9090 9090 9090  ..Eh...... | 00100980: 05e8 4568 00e7 07c3 9090 9090 9090 9090  ..Eh......
001009b0: 05f0 4568 00cd 07c3 9090 9090 9090 9090  ..Eh...... | 001009b0: 05f0 4568 00e7 07c3 9090 9090 9090 9090  ..Eh......
001009e0: 05fc 4568 00cd 07c3 9090 9090 9090 9090  ..Eh...... | 001009e0: 05fc 4568 00e7 07c3 9090 9090 9090 9090  ..Eh......
00100a10: 0508 4668 00cd 07c3 9090 9090 9090 9090  ..Fh...... | 00100a10: 0508 4668 00e7 07c3 9090 9090 9090 9090  ..Fh......
00100a40: 05f4 4568 00cd 07c3 9090 9090 9090 9090  ..Eh...... | 00100a40: 05f4 4568 00e7 07c3 9090 9090 9090 9090  ..Eh......
00100a60: b003 66c7 0504 4668 00ce 07a2 0646 6800  ..f...Fh.. | 00100a60: b003 66c7 0504 4668 00e8 07a2 0646 6800  ..f...Fh..
00100aa0: 0500 4668 00ce 07c3 9090 9090 9090 9090  ..Fh...... | 00100aa0: 0500 4668 00e8 07c3 9090 9090 9090 9090  ..Fh......
00100ad0: 0510 4668 00ce 07c3 9090 9090 9090 9090  ..Fh...... | 00100ad0: 0510 4668 00e8 07c3 9090 9090 9090 9090  ..Fh......
00100b00: 050c 4668 00ce 07c3 9090 9090 9090 9090  ..Fh...... | 00100b00: 050c 4668 00e8 07c3 9090 9090 9090 9090  ..Fh......
00100b30: 05ec 4568 00ce 07c3 9090 9090 9090 9090  ..Eh...... | 00100b30: 05ec 4568 00e8 07c3 9090 9090 9090 9090  ..Eh......
00106b80: 0534 4668 00cd 07c3 9090 9090 9090 9090  .4Fh...... | 00106b80: 0534 4668 00e7 07c3 9090 9090 9090 9090  .4Fh......
00106bb0: 0520 4668 00cd 07c3 9090 9090 9090 9090  . Fh...... | 00106bb0: 0520 4668 00e7 07c3 9090 9090 9090 9090  . Fh......
00106be0: 0530 4668 00cd 07c3 9090 9090 9090 9090  .0Fh...... | 00106be0: 0530 4668 00e7 07c3 9090 9090 9090 9090  .0Fh......
00106c10: 0514 4668 00cd 07c3 9090 9090 9090 9090  ..Fh...... | 00106c10: 0514 4668 00e7 07c3 9090 9090 9090 9090  ..Fh......
00106c40: 052c 4668 00ce 07c3 9090 9090 9090 9090  .,Fh...... | 00106c40: 052c 4668 00e8 07c3 9090 9090 9090 9090  .,Fh......
00106c70: 051c 4668 00ce 07c3 9090 9090 9090 9090  ..Fh...... | 00106c70: 051c 4668 00e8 07c3 9090 9090 9090 9090  ..Fh......
00106ca0: 0528 4668 00ce 07c3 9090 9090 9090 9090  .(Fh...... | 00106ca0: 0528 4668 00e8 07c3 9090 9090 9090 9090  .(Fh......
00106cd0: 0518 4668 00ce 07c3 9090 9090 9090 9090  ..Fh...... | 00106cd0: 0518 4668 00e8 07c3 9090 9090 9090 9090  ..Fh......
00106d00: 0524 4668 00ce 07c3 9090 9090 9090 9090  .$Fh...... | 00106d00: 0524 4668 00e8 07c3 9090 9090 9090 9090  .$Fh......
0010bfa0: 0540 4668 00ce 07c3 9090 9090 9090 9090  .@Fh...... | 0010bfa0: 0540 4668 00e8 07c3 9090 9090 9090 9090  .@Fh......
0010bfd0: 053c 4668 00ce 07c3 9090 9090 9090 9090  .<Fh...... | 0010bfd0: 053c 4668 00e8 07c3 9090 9090 9090 9090  .<Fh......
00120f00: 897c 2420 e8d7 cd07 008d 5424 3c6a 018d  .|$ ...... | 00120f00: 897c 2420 e8d7 e707 008d 5424 3c6a 018d  .|$ ......
0012e450: 6cce 0700 85c0 750a 5f5e 5d5b 83c4 20c2  l.....u._^ | 0012e450: 6ce8 0700 85c0 750a 5f5e 5d5b 83c4 20c2  l.....u._^
0018a400: b001 66c7 055c 4768 00cd 07a2 5e47 6800  ..f..\Gh.. | 0018a400: b001 66c7 055c 4768 00e7 07a2 5e47 6800  ..f..\Gh..

[nix-shell:~/…/pcx-utils]$ 
```

6. (**optional**) and copy the `manager.exe` file to the games `root` folder:

```
[nix-shell:~/.wine/drive_c/FUTBOL6]$ cp -R ~/…/pcx-utils/pcx/pcf/v61/manager.exe .
```


## PCF Teams v.6.x (AST) schema

### Country

```haskell
data Country -- ISO-3166
  = XXX
  | ALB     -- ALBANIA
  | DEU     -- GERMANY
  | ARG     -- ARGENTINA
  | AUS     -- AUSTRALIA
  | AUT     -- AUSTRIA
  | AZE     -- AZERBAIJAN
  | BLR     -- BELARUS
  | BOL     -- BOLIVIA
  | BIH     -- BOSNIA AND HERZEGOVINA
  | BRA     -- BRAZIL
  | BGR     -- BULGARIA
  | BEL     -- BELGIUM
  | CMR     -- CAMEROON
  | CHL     -- CHILE
  | CYP     -- CYPRUS
  | COL     -- COLOMBIA
  | HRV     -- CROATIA
  | DNK     -- DENMARK
  | GBR_SCT -- SCOTLAND
  | SVK     -- SLOVAKIA
  | SVN     -- SLOVENIA
  | ESP     -- SPAIN
  | FIN     -- FINLAND
  | FRA     -- FRANCE
  | GHA     -- GHANA
  | GRC     -- GREECE
  | NLD     -- NETHERLANDS
  | HND     -- HONDURAS
  | HUN     -- HUNGARY
  | GBR_ENG -- ENGLAND
  | IRL     -- IRELAND
  | GBR_NIR -- NORTH. IRELAND
  | ISL     -- ICELAND
  | FRO     -- FAROE ISLANDS
  | ISR     -- ISRAEL
  | ITA     -- ITALY
  | LTU     -- LITHUANIA
  | LUX     -- LUXEMBOURG
  | MKD     -- MACEDONIA
  | MLT     -- MALTA
  | MAR     -- MOROCCO
  | MDA     -- MOLDOVA
  | NGA     -- NIGERIA
  | NOR     -- NORWAY
  | GBR_WLS -- WALES
  | POL     -- POLAND
  | PRT     -- PORTUGAL
  | CZE     -- CZECHIA
  | ROU     -- ROMANIA
  | RUS     -- RUSSIA
  | SRB     -- SERBIA
  | ZAF     -- SOUTH AFRICA
  | SWE     -- SWEDEN
  | CHE     -- SWITZERLAND
  | TUR     -- TURKEY
  | UKR     -- UKRAINE
  | URY     -- URUGUAY
  | YUG     -- YUGOSLAVIA
  | PER     -- PERU
  | CAN     -- CANADA
  | USA     -- U.S.A.
  | GEO     -- GEORGIA
  | CRI     -- COSTA RICA
  | PRY     -- PARAGUAY
  | JPN     -- JAPAN
  | DZA     -- ALGERIA
  | TTO     -- TRINIDAD AND TOBAGO
  | SEN     -- SENEGAL
  | SUR     -- SURINAME
  | ZMB     -- ZAMBIA
  | CPV     -- CABO VERDE
  | VEN     -- VENEZUELA
  | RHO     -- RHODESIA
  | SGP     -- SINGAPORE
  | AND     -- ANDORRA
  | MOZ     -- MOZAMBIQUE
  | LIE     -- LIECHTENSTEIN
  | LBR     -- LIBERIA
  | PAN     -- PANAMA
  | ZAR     -- ZAIRE
  | TJK     -- TAJIKISTAN
  | UZB     -- UZBEKISTHAN
  | MEX     -- MEXICO
  | GIN     -- GUINEA
  | AGO     -- ANGOLA
  | ZWE     -- ZIMBABWE
  | SLE     -- SIERRA LEONE
  | GLP     -- GUADELOUPE
  | ECU     -- ECUADOR
  | EST     -- ESTONIA
  | GNB     -- GUINEA-BISSAU
  | LBY     -- LIBYA
  | EGY     -- EGYPT
  | JAM     -- JAMAICA
  | NCL     -- NEW CALEDONIA
  | BMU     -- BERMUDA
  | NZL     -- NEW ZEALAND
  | GUF     -- FRENCH GUIANA
  | VCT     -- SAINT VINCENT AND THE GRENADINES
  | TCD     -- CHAD
  | TGO     -- TOGO
  | GIN_C   -- GUINEA CONAKRY
  | TZA     -- TANZANIA
  | BFA     -- BURKINA FASO
  | GMB     -- GAMBIA
  | RWA     -- RWANDA
  | KEN     -- KENYA
  | MRT     -- MAURITANIA
  | MLI     -- MALI
  | UGA     -- UGANDA
  | COG     -- CONGO
  | LVA     -- LATVIA
  | CIV     -- CÔTE D'IVOIRE
  | ARM     -- ARMENIA
  | NIC     -- NICARAGUA
  | ESP_CAT -- CATALUÑA
  | NER     -- NIGER
  | BRB     -- BARBADOS
  | IRN     -- IRAN
  | QAT     -- QATAR
  | TUN     -- TUNISIA
  | GAB     -- GABON
  | PYF     -- FRENCH POLYNESIA
  | MUS     -- MAURITIUS
  | MDG     -- MADAGASCAR
  | MTQ     -- MARTINIQUE
  | VNM     -- VIETNAM
```

### Formation

```haskell
type Point2D = (Word16, Word16)

data Position = P
  { area    :: (Point2D, Point2D)
  , defence ::  Point2D
  , offence ::  Point2D
  }

data Formation
  = F343
  | F352
  | F433
  | F442
  | F532
  | F541
  | F424
  | F523
  | F451
  | F3331
  | Custom [(Index, Position)]
```

### Manager

```haskell
data Core = C
  { mid  :: Word16
  , name :: String
  }

data Database = D
  { fullname      :: String
  , mdunknown00   :: String
  , tactics       :: String
  , honours       :: String
  , miscellaneous :: String
  , lastseason    :: String
  , managercareer :: String
  , mdunknown01   :: Word8
  , playercareer  :: String
  , statements    :: String
  }

data Manager
  = S Core
  | E Core Database
```

### Player

```haskell
data Status
  = Veteran
  | Signing
  | Academy
  | OnLeave

data Role
  = GK
  | RB  -- Right Back
  | LB  -- Left Back
  | SW  -- Sweeper
  | LCB -- Left Center Back
  | RCB -- Right Center Back
  | RM  -- Right Midfielder
  | RCM -- Right Centre Midfielder
  | ST  -- Striker
  | CAM -- Centre Attacking Midfielder
  | LM  -- Left Midfielder
  | RW  -- Right Wing
  | CF  -- Centre Forward
  | LW  -- Left Wing
  | CDM -- Centre Defensive Midfielder
  | LF  -- Left Forward
  | RF  -- Right Forward
  | LCM -- Left Centre Midfielder

data Skin
  = Skin
  | Light
  | Dark
  | Medium

data Hair
  = Hair
  | Blond
  | Bald
  | Black
  | Grey
  | Redhead
  | Brown

data Position
  = KEP -- Keeper
  | DEF -- Defender
  | MID -- Midfielder
  | FOR -- Forward

data Core = C
  { pid         :: Word16
  , number      :: Word8
  , name        :: String
  , fullname    :: String
  , index       :: Word8
  , status      :: Status
  , roles       :: [Role]
  , citizenship :: Country
  , skin        :: Skin
  , hair        :: Hair
  , position    :: Position
  , birthday    :: Date
  , height      :: Word8
  , weight      :: Word8
  , pace        :: Word8
  , stamina     :: Word8
  , aggression  :: Word8
  , skill       :: Word8
  , finishing   :: Word8
  , dribbling   :: Word8
  , passing     :: Word8
  , shooting    :: Word8
  , tackling    :: Word8
  , goalkeeping :: Word8
  }

data Database = D
  { country       :: Word8
  , birthplace    :: String
  , fromteam      :: String
  , nationalteam  :: String
  , pdunknown00   :: String
  , features      :: String
  , honours       :: String
  , intcaps       :: String
  , miscellaneous :: String
  , lastseason    :: String
  , career        :: String
  }

data Player
  = S Core
  | E Core Database
```

### Staff

```haskell
data Staff
  = M Manager
  | P Player
```

### Stats

```haskell
type LastDecade = [Word16]
type LastSeason = [Word8]

type Participations = Word8
type Won            = Word8
type Competition    = (Participations, Won)

data Stats = T
  { unknown00        :: Word16
  , unknown01        :: Word8
  , lastdecade       :: LastDecade
  , seasons          :: Word8
  , matches          :: Word16
  , win              :: Word16
  , draw             :: Word16
  , gf               :: Word16
  , ga               :: Word16
  , points           :: Word16
  , champion         :: Word8
  , runnersup        :: Word8
  , lastseason       :: LastSeason
  , uefa             :: Competition
  , cup              :: Competition
  , championsleague  :: Competition
  , cupwinnerscup    :: Competition
  , supercup         :: Competition
  , intercontinental :: Competition
  , eufasupercup     :: Competition
  }
```

### Tactics

```haskell
data PlayStyle
  = Attacking
  | Speculative
  | Mixed

data Tackling
  = Soft
  | Medium
  | Aggresive

data Coverage
  = Zone
  | ManToMan

data Clearance
  = Short
  | Long

data Preasure
  = Own
  | Midfield
  | Opponent

data Tactics = T
  -- ATTACK
  { possession :: Word8 -- Passing vs Long Ball in percentage
  , counter    :: Word8 -- in percentage
  , playstyle  :: PlayStyle
  -- DEFENCE
  , tackling   :: Tackling
  , coverage   :: Coverage
  , clearance  :: Clearance
  , preasure   :: Preasure
  }
```

### Team(s)

```haskell
data Core = C
  { tid        :: Word16
  , tunknown00 :: Word16
  , name       :: String
  , stadium    :: String
  , country    :: Country
  , tunknown01 :: Word8
  , fullname   :: String
  , capacity   :: Word32
  , standing   :: Word32
  , width      :: Word16
  , length     :: Word16
  , founded    :: Word16
  --
  , formation  :: Formation
  , tactics    :: Tactics
  , staff      :: [Staff]
  }

data Database = D
  { built     :: Word16
  , members   :: Word32
  , president :: String
  , budget    :: Word32
  , budgetpro :: Word32
  , sponsor   :: String
  , supplier  :: String
  , reserve   :: Word16
  , stats     :: Stats
  }

data Team
  = S Core
  | E Core Database

data Teams = Teams [PKF.Team]
```


## Limitations

Since the focus has only been on **PC Fútbol 6.x**, it's very unlikely that the
tools will work with any of the other versions, even though it's possible to
expand the tools to support them.

> **Note**: It's possible to extract pointers from **PC Basket 6.0** and **PC
> Fútbol 5.0**, but it seems that the other tools don't work.


## More information (in Spanish and English)

* YouTube:
  - [Spanish - Hack Madrid %27: Hacking on PKF files][ythackmadrid]
  - [English - BornHack 2020 - Ramón Soto Mathiesen - Hacking on PKF files][ytbornhack]
  
* PCFutbolManía:
  - [Conversor de formato PC Fútbol a entendible y de formato entendible a PC
Fútbol][pcfmconversor]
  - [Patrones en EQ022022.PKF][pcfmpatrones]
  - [PC Fútbol 6.1: Actualización de equipos con datos FUT/FIFA2020][pcfmdescargas]

[ythackmadrid]:  https://www.youtube.com/watch?v=PcA9o88xH_4
[ytbornhack]:    https://www.youtube.com/watch?v=T_3JeI5R8gA
[pcfmconversor]: https://www.pcfutbolmania.com/thread-10700.html
[pcfmpatrones]:  https://www.pcfutbolmania.com/thread-10702.html
[pcfmdescargas]: https://www.pcfutbolmania.com/thread-11641.html
