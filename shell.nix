################################################################################
##
## PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

with import <nixpkgs> { };
stdenv.mkDerivation rec {
  name = "env";
  env = buildEnv { name = name; paths = buildInputs; };
  buildInputs = [
    # visualize folder structure: tree -d
    tree
    # sed alike for JSON data 
    jq
    # Image Magick
    imagemagick
    # web clients 
    curl
    wget
    # hexdump of binary
    xxd
  ];
}
  
# Isolated Development Environment using Nix  
# 
# Source: https://ariya.io/2016/06/isolated-development-environment-using-nix
