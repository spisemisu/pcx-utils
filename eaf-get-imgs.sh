#!/bin/sh

################################################################################
##
## PCx-Utils, (c) 2023 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

clear

mkdir -v -p eaf/imgs/countries
mkdir -v -p eaf/imgs/teams
mkdir -v -p eaf/imgs/players

echo "# countries:"
cat eaf/json/countries.json | \
    jq -r '.[] | "wget -O eaf/imgs/countries/\(.id).png \(.imageUrl)"' \
       > eaf/temp/countries.urls 
sh eaf/temp/countries.urls

echo "# teams:"
cat eaf/json/teams.json | \
    jq -r '.[] | "wget -O eaf/imgs/teams/\(.id).png \(.imageUrl)"' \
       > eaf/temp/teams.urls 
sh eaf/temp/teams.urls

echo "# players:"
#cat eaf/json/players.json | \
#    jq -r '.[] | "wget -O eaf/imgs/players/\(.uid).png \(.avatarUrl)"' \
#       > eaf/temp/players.urls 
#sh eaf/temp/players.urls

#echo "# remove all url files"
#rm eaf/temp/*.urls
