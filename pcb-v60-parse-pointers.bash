#!/usr/bin/env bash

################################################################################
##
## PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

clear

mkdir -v -p pcx/pcb/v60

echo "# Parse pointers:"

echo "## Teams:"

echo "* ../dat/pcb0060/DBDAT/EQ022022.PKF"
./bin/pcx-parsepointers \
    pkf="../dat/pcb0060/DBDAT/EQ022022.PKF" \
    > pcx/pcb/v60/eq022022.ptr
echo "* pcx/pcb/v60/eq022022.ptr"
