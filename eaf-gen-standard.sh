#!/bin/sh

################################################################################
##
## PCx-Utils, (c) 2023 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

iso=$1
nid=$2
ids=$3

echo "### Extracting from JSON"

echo "#### Teams"
./eaf-ast-teamstd.sh \
    $ids $iso eaf/meta/$nid.json \
    > eaf/temp/$nid.fbt
echo "* eaf/temp/$nid.fbt"

echo "#### Players"
./eaf-ast-playstd.sh \
    $ids      eaf/meta/$nid.json \
    > eaf/temp/$nid.fbp
echo "* eaf/temp/$nid.fbp"

echo "#### Merge"

mkdir -v -p eaf/data/

./bin/eaf-v6-parseteams \
    aby="2003" \
    fbt="eaf/temp/$nid.fbt" \
    fbp="eaf/temp/$nid.fbp" \
    > eaf/data/$nid.tms
echo "* eaf/data/$nid.tms"

echo "#### Convert to DBCs"

mkdir -v -p eaf/data/eq022022

./bin/pcf-v6-bytesteams \
    tms="eaf/data/$nid.tms" \
    dbc="eaf/data/eq022022/"
echo "* eaf/data/eq022022/"

#echo "#### Remove temporary files"
#rm -fr eaf/temp/*

echo "### Transforming images"

mkdir -v -p eaf/temp/

#echo "#### Remove temporary files"
#rm -fr eaf/temp/*

echo "#### Teams images"
mkdir -v -p eaf/data/estadio
mkdir -v -p eaf/data/miniesc
mkdir -v -p eaf/data/nanoesc
mkdir -v -p eaf/data/ridiesc

./eaf-img-teamstd.sh \
    $ids      eaf/meta/$nid.json \
    > eaf/temp/$nid.ftb
echo "* eaf/temp/$nid.ftb"
sh eaf/temp/$nid.ftb

#echo "#### Remove temporary files"
#rm -fr eaf/temp/*

echo "#### Players images"
mkdir -v -p eaf/data/minifoto

./eaf-img-players.sh \
    $ids \
    > eaf/temp/$nid.fpb
echo "* eaf/temp/$nid.fpb"
sh eaf/temp/$nid.fpb

#echo "#### Remove temporary files"
#rm -fr eaf/temp/* 
