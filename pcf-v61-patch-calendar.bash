#!/usr/bin/env bash

################################################################################
##
## PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

clear

mkdir -v -p pcx/pcf/v61

echo "# Patch calendar years (1997-1998):"

echo "* ../dat/pcf0061/MANAGER.EXE"
cat "../dat/pcf0061/MANAGER.EXE" \
    | ./bin/pcf-v6-patchcalendar \
	  ytd="2023" \
	  > pcx/pcf/v61/manager.exe
echo "* pcx/pcf/v61/manager.exe"

diff \
    --side-by-side \
    --suppress-common-lines \
    <(xxd ../dat/pcf0061/MANAGER.EXE) \
    <(xxd pcx/pcf/v61/manager.exe)


## Reference
#
# How do I compare binary files in Linux?
#
# - https://superuser.com/a/968863
#
# unix diff side-to-side results?
#
# - https://stackoverflow.com/a/49630142
