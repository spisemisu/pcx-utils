#!/bin/sh

################################################################################
##
## PCx-Utils, (c) 2023 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

cat eaf/json/players.json | \
    jq -c --arg x $1 \
       '.[] | 
       select (
       	 .team.id == ($x | split(",")[] | tonumber)
       )' | \
    jq -c -s 'sort_by(.team.id)[]' | \
    jq -c -r \
    --argjson iso3166 "$(jq -s 'add' eaf/meta/iso3166.json eaf/meta/add3166.json)" \
    --argjson pcf3166 "$(cat eaf/meta/pcf3166.json)" \
    --argjson pcfposi "$(cat eaf/meta/pcfposi.json)" \
    --argjson pcfrole "$(cat eaf/meta/pcfrole.json)" \
    --argjson lidacid "$(
      jq -c --arg x $1 \
      '[.[] | select ( .id == ($x | split(",")[] | tonumber))]
      | [sort_by(.id)[]]
      | map (.id) 
      | to_entries
      | map( { (.value | tostring): .key } ) 
      | add' \
      eaf/json/teams.json
    )" \
    --argjson pcfteam "$(cat $2)" \
    ' . 
      | .cid = ($pcfteam[($lidacid[(.team.id | tostring)])])
      | .bir = ( .birthdate | (split("-")) )
      | .nam = ( "\(.firstName) \(.lastName)" )
      | .noc = ( if .commonName == null
                 then
                    .lastName
                 else
                    .commonName
                 end
               )
      | .hgt = (if .height                 == null then 140 else .height                 end)
      | .wgt = (if .weight                 == null then 000 else .weight                 end)
      | .rat = (if .overallRating          == null then 050 else .overallRating          end)
      | .pac = (if .stats.pac.value        == null then 050 else .stats.pac.value        end)
      | .sta = (if .stats.stamina.value    == null then 050 else .stats.stamina.value    end)
      | .agg = (if .stats.aggression.value == null then 050 else .stats.aggression.value end)
      | .fin = (if .stats.finishing.value  == null then 050 else .stats.finishing.value  end)
      | .dri = (if .stats.dri.value        == null then 050 else .stats.dri.value        end)
      | .pas = (if .stats.pas.value        == null then 050 else .stats.pas.value        end)
      | .sho = (if .stats.sho.value        == null then 050 else .stats.sho.value        end)
      | .tac = (if .stats.def.value        == null then 050 else .stats.def.value        end)
      | .a3c = 
               ( if   $iso3166[(.nationality.label)] 
                 then 
               	      if   $pcf3166[$iso3166[(.nationality.label)]]
                      then $iso3166[(.nationality.label)] 
                      else "XXX"
                      end 
                 else "XXX" 
                 end
               )
      | .pos  = $pcfposi[(.position.shortLabel)]
      | .rol  = $pcfrole[(.position.shortLabel)]
      | "(\(.cid), E C { pid = \(.uid), number = 0, name = \"\(.noc)\", fullname = \"\(.nam)\", index = 0, status = Veteran, roles = \(.rol), citizenship = \(.a3c), skin = Skin, hair = Hair, position = \(.pos), birthday = ( \(.bir[0]) , \(.bir[1]) , \(.bir[2]) ), height = \(.hgt), weight = \(.wgt), pace = \(.pac), stamina = \(.sta), aggression = \(.agg), skill = \(.rat), finishing = \(.fin), dribbling = \(.dri), passing = \(.pas), shooting = \(.sho), tackling = \(.tac), goalkeeping = \(.rat) } D { country = 0, birthplace = [], fromteam = [], nationalteam = [], pdunknown00 = [], features = [], honours = [], intcaps = [], miscellaneous = [], lastseason = [], career = [] })"
    '
