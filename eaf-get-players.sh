#!/bin/sh

################################################################################
##
## PCx-Utils, (c) 2023 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

clear

mkdir -v -p eaf/json/
mkdir -v -p eaf/temp/

echo "# players: eaf/temp/ratings-offset-*.json > eaf/json/players.json (+fix birthdays)"
jq -s '
   .[] |
   .items |
   map(select(.gender.id == 0)) |
   map(.uid = .rank) |
   map(.birthdate |= (gsub(" 00:00";""))) |
   map(.birthdate |= (gsub(" 12:00:00 AM";""))) |
   map(.birthdate |= (gsub("\\.";"/"))) |
   map(.birthdate |= (split("/") | "\(.[2])-\(.[0])-\(.[1])"))
   ' eaf/temp/ratings-offset-*.json \
       > eaf/temp/players.json
jq -s '
   [ .[] ] |
   flatten |
   to_entries | map(.value.uid = .key + 1) |
   map(.value)
   ' eaf/temp/players.json \
       > eaf/json/players.json
