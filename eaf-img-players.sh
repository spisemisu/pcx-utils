#!/bin/sh

################################################################################
##
## PCx-Utils, (c) 2023 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

cat eaf/json/players.json | \
    jq -c --arg x $1 \
       '.[] | 
       select (
       	 .team.id == ($x | split(",")[] | tonumber)
       )' | \
    jq -c -s 'sort_by(.team.id)[]' | \
    jq -c -r \
    --argjson lidacid "$(
      jq -c --arg x $1 \
      '[.[] | select ( .id == ($x | split(",")[] | tonumber))]
      | [sort_by(.id)[]]
      | map (.id) 
      | to_entries
      | map( { (.value | tostring): .key } ) 
      | add' \
      eaf/json/teams.json
    )" \
    ' . 
      | "
convert eaf/imgs/players/\(.uid).png \\
	-alpha remove \\
	BMP2:eaf/temp/\(.uid).bmp
convert eaf/temp/\(.uid).bmp \\
	-resize 32x32 \\
	BMP2:eaf/temp/\(.uid)_mini.bmp
convert eaf/temp/\(.uid)_mini.bmp \\
	-shave 1x1 \\
	-bordercolor Black \\
	-border 1x1 \\
	-type palette \\
	-compress none \\
	-remap eaf/meta/palette.bmp \\
	BMP2:eaf/temp/\(.uid)_mini_.bmp
./bin/pcx-colourpalette \\
	bmp=eaf/temp/\(.uid)_mini_.bmp \\
	> eaf/data/minifoto/j96$(printf %05d \(.uid)).bmp"
    ' 

# ImageMagick > Special crop and resize
#
# https://www.imagemagick.org/discourse-server/viewtopic.php?t=13793

