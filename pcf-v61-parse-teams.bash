#!/usr/bin/env bash

################################################################################
##
## PCx-Utils, (c) 2020 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

clear

mkdir -v -p pcx/pcf/v61

echo "# Parse teams:"

echo "* ../dat/pcf0061/DBDAT/EQ022022.PKF"
./bin/pcf-v6-parseteams \
    pkf="../dat/pcf0061/DBDAT/EQ022022.PKF" \
    ptr="pcx/pcf/v61/eq022022.ptr" \
    > pcx/pcf/v61/eq022022.tms
echo "* pcx/pcf/v61/eq022022.tms"
